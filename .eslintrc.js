module.exports = {
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint',
    'jsdoc',
    'import',
    'promise',
    'optimize-regex',
    'security',
    'cypress',
    'unicorn'
  ],
  extends: [
    'standard-with-typescript',
    'plugin:jsdoc/recommended',
    "plugin:node/recommended",
    'plugin:promise/recommended',
    'plugin:security/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
    'plugin:cypress/recommended',
    'plugin:unicorn/recommended'
  ],
  parserOptions: {
    project: './tsconfig.json'
  },
  rules: {
    'no-dupe-class-members': 'off',
    'optimize-regex/optimize-regex': 'warn',
    'node/no-unsupported-features/es-syntax': 'off',
    'lines-between-class-members': 'off',
    'unicorn/filename-case': 'off',
    'unicorn/prevent-abbreviations': [
      'error',
      {
        'replacements': {
          'mod': false,
          'args': false
        }
      }
    ]
  },
  env: {
    es6: true,
    node: true,
    jest: true
  }
}
