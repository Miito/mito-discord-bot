FROM node:12.4-alpine

ARG DISCORD_TOKEN
ARG OSU_TOKEN
ARG YOUTUBE_TOKEN
ARG PIXIV_USERNAME
ARG PIXIV_PASSWORD

ENV DISCORD_TOKEN = $DISCORD_TOKEN \
    OSU_TOKEN = $OSU_TOKEN \
    YOUTUBE_TOKEN = $YOUTUBE_TOKEN \
    PIXIV_USERNAME = $PIXIV_USERNAME \
    PIXIV_PASSWORD = $PIXIV_PASSWORD

WORKDIR /user/src/app

COPY package.json ./
COPY package-lock.json ./
RUN npm ci --only=production

COPY . .

CMD ["npm", "run", "serve"]
