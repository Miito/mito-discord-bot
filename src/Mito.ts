import Terminal from './Utils/Terminal'
// import Keyv from 'keyv'
// import KeyvProvider from 'commando-provider-keyv'
import mongoose from 'mongoose'
import fs, { readdirSync } from 'fs'
import path from 'path'
import { CommandoClient, FriendlyError, Command } from 'discord.js-commando'
import { GuildMember } from 'discord.js'
import AudioController from './Audio/AudioController'
import config = require('../bot.config.json')

// const KeyvSettings = { serialize: data => data, deserialize: data => data }

/**
 * Bot base client
 *
 * @class Mito
 */
class Mito {
  private readonly token: string | undefined
  private readonly client: CommandoClient
  protected readonly audio: AudioController

  /**
   * Creates an instance of Mito.
   *
   * @param {string} token Discord API Token
   * @memberof Mito
   */
  public constructor (token: string) {
    this.token = token
    this.client = new CommandoClient({
      commandPrefix: config.defaultPrefix,
      owner: config.discordOwners,
      disabledEvents: ['TYPING_START'],
      presence: {
        status: 'online',
        activity: {
          name: 'lolis~',
          type: 'WATCHING'
        }
      }
    })

    this.audio = new AudioController(this.client)

    Terminal.log(`Mito Bot v.${require(path.resolve(process.cwd(), 'package.json')).version}`)
  }

  /**
   * Initialize client
   *
   * @returns {Promise<void>}
   * @memberof Mito
   */
  public async init (): Promise<void> {
    this.client.registry
      .registerDefaultTypes()
      .registerGroups([
        ['misc', 'Misc'],
        ['fun', 'Fun'],
        ['roles', 'Roles'],
        ['nsfw', 'NSFW'],
        ['sfw', 'SFW'],
        ['music', 'Music'],
        ['weeb', 'Weeb'],
        ['osu', 'osu!'],
        ['bot', 'bot'],
        ['utility', 'utility'],
        ['pokemon', 'pokemon'],
        ['account', 'account']
      ])
      .registerDefaultGroups()
      .registerDefaultCommands()
      .registerCommandsIn(path.join(__dirname, 'Commands'))

    // this.client.setProvider(new KeyvProvider(new Keyv(process.env.MONGODB_URL, KeyvSettings)))

    await mongoose.connect(typeof process.env.MONGODB_URL !== 'undefined' ? process.env.MONGODB_URL : 'mongodb://localhost:27017/test', { useNewUrlParser: true });

    this.client
      .on('error', Terminal.error)
      .on('warn', Terminal.warn)
      .on('ready', (): void => {
        Terminal.log(`${this.client.user ? this.client.user.tag : 'Bot'} Logged in`)
        Terminal.log('Mito Bot is up and running')
      })
      .on('disconnect', (): void => {
        Terminal.warn('Disconnected')
      })
      .on('reconnect', (): void => {
        Terminal.warn('Reconnecting...')
      })
      .on('commandError', (command: Command, error: Error): void => {
        if (error instanceof FriendlyError) {
          return
        }
        Terminal.error(`Error in command ${command.groupID}:${command.memberName} ${error}`)
      })
      .on('guildMemberAdd', (client: CommandoClient, member: GuildMember): void => {
        Terminal.log(`client: ${client}`)
        Terminal.log(`member: ${member}`)
      })
      .on('warn', (error: string): void => {
        Terminal.error(error)
      })
      .on('error', (error: Error): void => {
        Terminal.error(error)
      })

    // Events Loader
    fs.readdir(`${__dirname}/Events/`, (error, files) => {
      if (error) { Terminal.error(error) }

      files.forEach(async (file: string) => {
        if (/(\.js)$/.exec(file)) {
          const eventFunction = await import(`${__dirname}/Events/${file}`)
          const eventName = file.split('.')[0]
          this.client.on(eventName, (...args: any) => eventFunction.default(this.client, ...args))
        }
      })
    })

    if (process.env.NODE_ENV === 'development') {
      this.client.on('debug', (error: string): void => {
        Terminal.log(error)
      })
    }

    await this.client.login(this.token)
  }
}

process.on('unhandledRejection', (reason, promise): void => {
  Terminal.log(`${reason} Unhandled Rejection at ${promise}`)
})

process.on('uncaughtException', (error): void => {
  Terminal.error(`Uncaught Exception thrown at ${error.stack}`)
})

export default Mito
