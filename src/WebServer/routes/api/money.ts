import express, { Request, Response } from 'express'
import Money from '../../../Models/money'
import Terminal from '../../../Utils/Terminal'
const router = express.Router()

// @route GET api/money
// @description Get all money
// @access Private
router.get('/', (_request: Request, response: Response) => {
  Money.find()
    .sort({ date: -1 })
    .then((items) => response.json(items))
    .catch((error) => Terminal.error(error.message))
})

export default router
