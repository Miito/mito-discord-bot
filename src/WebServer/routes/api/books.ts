import express, { Request, Response } from 'express'
// import Book from '../../../Models/book'
const router = express.Router()

// @route GET api/books
// @description Get all books
// @access Public
router.get('/', (_request: Request, response: Response) => {
  response.send('book route index!')
})

// // @route GET api/books/test
// // @description tests books route
// // @access Public
// router.get('/test', (request: Request, response: Response) => {
//   response.send('book route testing!')
// })

// // @route GET api/books
// // @description Get all books
// // @access Public
// router.get('/', (request: Request, response: Response) => {
//   Book.find()
//     .then(books => response.json(books))
//     .catch(() => response.status(404).json({ nobooksfound: 'No Books found' }))
// })

// // @route GET api/books/:id
// // @description Get single book by id
// // @access Public
// router.get('/:id', (request: Request, response: Response) => {
//   Book.findById(request.params.id)
//     .then(book => response.json(book))
//     .catch(() => response.status(404).json({ nobookfound: 'No Book found' }))
// })

// // @route GET api/books
// // @description add/save book
// // @access Public
// router.post('/', (request: Request, response: Response) => {
//   Book.create(request.body)
//     .then(book => response.json({ msg: 'Book added successfully' }))
//     .catch(() => response.status(400).json({ error: 'Unable to add this book' }))
// })

// // @route GET api/books/:id
// // @description Update book
// // @access Public
// router.put('/:id', (request: Request, response: Response) => {
//   Book.findByIdAndUpdate(request.params.id, request.body)
//     .then(book => response.json({ msg: 'Updated successfully' }))
//     .catch(() =>
//       response.status(400).json({ error: 'Unable to update the Database' })
//     )
// })

// // @route GET api/books/:id
// // @description Delete book by id
// // @access Public
// router.delete('/:id', (request: Request, response: Response) => {
//   Book.findByIdAndRemove(request.params.id, request.body)
//     .then(book => response.json({ mgs: 'Book entry deleted successfully' }))
//     .catch(() => response.status(404).json({ error: 'No such a book' }))
// })

export default router
