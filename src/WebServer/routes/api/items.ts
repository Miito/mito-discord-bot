import express, { Request, Response } from 'express'
import Item from '../../../Models/Item'
import Terminal from '../../../Utils/Terminal'
const router = express.Router()

// @route GET api/items
// @description Get all Items
// @access Public
router.get('/', (_request: Request, response: Response) => {
  Item.find()
    .sort({ date: -1 })
    .then((items) => response.json(items))
    .catch((error) => Terminal.error(error.message))
})

// @route POST api/items
// @description Create a Item
// @access Public
router.post('/', (request: Request, response: Response) => {
  const newItem = new Item({
    name: request.body.name
  })

  newItem
    .save()
    .then(item => response.json(item))
    .catch((error) => Terminal.error(error.message))
})

// @route DELETE api/items/:id
// @description Delete a Item
// @access Public
router.delete('/:id', (request: Request, response: Response) => {
  Item
    .findById(request.params.id)
    .then(async (item) => item.remove().then(() => response.json({ success: true })))
    .catch(() => response.status(404).json({ success: false }))
})

export default router
