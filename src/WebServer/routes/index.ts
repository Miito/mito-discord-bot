import express, { Request, Response } from 'express'
const router = express.Router()

// @route GET /
// @description index
// @access Public
// router.get('/', (_request: Request, response: Response) => {
//   response.sendFile('../../WebClient/build/index.html')
// })
router.get('/*', function (_request: Request, response: Response) {
  response.sendFile('../../WebClient/build/index.html')
})

export default router
