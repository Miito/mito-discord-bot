import Terminal from '../Utils/Terminal'
import express, { Request, Response } from 'express'
import cors from 'cors'
import path from 'path'
import cookieParser from 'cookie-parser'
import compression from 'compression'
import { ConnectToDatabase } from '../Database/DatabaseHelper'
import helmet from 'helmet'

// Route
import ItemRoute from './routes/api/items'
import HomeRoute from './routes'

// import passport from 'passport'
// import io from 'socket.io'
// import i18n from 'i18n-express'

/**
 *
 * Express web server
 *
 * @class WebServer
 */
class WebServer {
  private readonly app: express.Application
  private readonly PORT: number | string

  public constructor () {
    this.app = express()
    this.PORT = typeof process.env.PORT !== 'undefined' ? process.env.PORT : 5000

    Terminal.log(`Mito Bot Web Server v.${require(path.resolve(process.cwd(), 'package.json')).version}`)
  }

  public async init (): Promise<void> {
    this.app.use(compression())
    this.app.use(helmet())
    this.app.use(cors())
    this.app.use(cookieParser())
    this.app.use(express.urlencoded({ extended: true }))
    this.app.use(express.json())
    this.app.use(express.static('../WebClient/build'))

    const DatabaseContext = await ConnectToDatabase()
    DatabaseContext.connection
      .once('disconnected', () => {
        Terminal.log('MongoDB database connection disconnected')
      })
      .once('open', () => {
        Terminal.log('MongoDB database connection open successfully')
      })
      .once('connected', () => {
        Terminal.log('MongoDB database connection connected successfully')
      })
      .once('err', (error) => {
        Terminal.log(`MongoDB Error: ${error.stack}`)
      })

    this.app.listen(this.PORT, (): void => {
      Terminal.log(`WebServer listening on port ${this.PORT}`)
    })

    this.app.get('/ping', (_request: Request, response: Response) => {
      response.send('pong')
    })

    this.app.use('/', HomeRoute)
    this.app.use('/api/items', ItemRoute)
  }
}

export default WebServer
