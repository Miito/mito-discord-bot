// @ts-nocheck
declare class Kitsu {
  public constructor(...args: any[])

  public delete(...args: any[]): any[]

  public get(...args: any[]): any[]

  public patch(...args: any[]): any[]

  public post(...args: any[]): any[]

  public self(...args: any[]): any[]
}
