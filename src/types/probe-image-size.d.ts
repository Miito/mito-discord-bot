declare module 'probe-image-size'

interface Result {
  width: number
  height: number
  type: string
  mime: string
  wUnits: string
  hUnits: string
  url: string
}

declare function Probe(
  source: any,
  options?: {},
  callback?: Function
): Promise<Result>;

declare namespace ProbeResult {
  interface Result {
    width: number
    height: number
    type?: string
    mime?: string
    wUnits?: string
    hUnits?: string
    url?: string
  }

  namespace parsers {
    function bmp(): Result
    function gif(): Result
    function jpeg(): Result
    function png(): Result
    function psd(): Result
    function svg(): Result
    function tiff(): Result
    function webp(): Result
  }

  function sync(source: any): Result
  namespace sync {
    namespace parsers {
      function bmp(data: Buffer): Result
      function gif(data: Buffer): Result
      function jpeg(data: Buffer): Result
      function png(data: Buffer): Result
      function psd(data: Buffer): Result
      function svg(data: Buffer): Result
      function tiff(data: Buffer): Result
      function webp(data: Buffer): Result
    }
  }
}
export = Probe;
