import * as dotenv from 'dotenv'
import Terminal from './Utils/Terminal'
import path from 'path'
import Mito from './Mito'
import Sentry from '@sentry/node'
import WebServer from './WebServer/Server'
import config = require('../bot.config.json')

if (process.env.NODE_ENV === 'production' && config.sentry.enabled) {
  try {
    Sentry.init({
      dsn: config.sentry.dsn,
      environment: typeof process.env.NODE_ENV !== 'undefined' ? process.env.NODE_ENV : 'development'
    })
  } catch (error) {
    Terminal.error(error)
  }
}

process.title = `${require(path.join(process.cwd(), 'package.json')).name} | gitlab.com/Miito/mito-discord-bot`

dotenv.config({
  path: path.resolve(process.cwd(), '.env'),
  encoding: 'utf8',
  debug: false
})

try {
  new WebServer().init().catch((error) => Terminal.error(error))
} catch (error) {
  Terminal.error(error.message)
}

try {
  new Mito(typeof process.env.DISCORD_TOKEN !== 'undefined' ? process.env.DISCORD_TOKEN : '').init().catch((error) => Terminal.error(error))

} catch (error) {
  Terminal.error(error.message)
}
