
/**
 *
 *
 * @param {number} milliseconds
 * @returns {Promise<number>}
 */
async function sleep (milliseconds: number): Promise<number> {
  return new Promise<number>((resolve: Function): number => setTimeout(resolve, milliseconds))
}

export default sleep
