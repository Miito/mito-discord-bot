// import Timer from './Timer'

// describe('Timer', (): void => {
//   let classUnderTest: Timer

//   beforeEach((): void => {
//     classUnderTest = new Timer()
//   })

//   it('should construct', (): void => {
//     expect(classUnderTest).toBeDefined()
//     expect(new Timer(10)).toBeDefined()
//   })

//   it('should fire at given intervals', async (): Promise<any> => {
//     classUnderTest.Interval = 1
//     // @ts-ignore
//     classUnderTest.Elapsed.push((): unknown => { })
//     await classUnderTest.Start()
//     expect(classUnderTest.Enabled).toBeFalsy()
//   })

//   it('should stop an enabled timer', (): void => {
//     classUnderTest.Interval = 1
//     // @ts-ignore
//     classUnderTest.Elapsed.push((): unknown => { })
//     classUnderTest.AutoReset = true
//     classUnderTest.Start()
//     classUnderTest.Stop()
//     expect(classUnderTest.Enabled).toBeFalsy()
//   })
// })
