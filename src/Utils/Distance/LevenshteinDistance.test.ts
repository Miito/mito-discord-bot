import LevenshteinDistance from './LevenshteinDistance'

test('LevenshteinDistance is a Function', (): void => {
  expect(LevenshteinDistance).toBeInstanceOf(Function)
})

test('LevenshteinDistance returns the correct results', (): void => {
  expect(LevenshteinDistance('30-seconds-of-code', '30-seconds-of-python-code')).toBe(7)
})

test('LevenshteinDistance returns the correct result for 0-length string as first argument', (): void => {
  expect(LevenshteinDistance('', 'foo')).toBe(3)
})

test('LevenshteinDistance returns the correct result for 0-length string as second argument', (): void => {
  expect(LevenshteinDistance('bar', '')).toBe(3)
})
