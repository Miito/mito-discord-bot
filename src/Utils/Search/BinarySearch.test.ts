import BinarySearch from './BinarySearch'

test('binarySearch is a Function', (): void => {
  expect(BinarySearch).toBeInstanceOf(Function)
})

test('Finds item in array', (): void => {
  expect(BinarySearch([1, 4, 6, 7, 12, 13, 15, 18, 19, 20, 22, 24], 6)).toBe(2)
})

test('Returns -1 when not found', (): void => {
  expect(BinarySearch([1, 4, 6, 7, 12, 13, 15, 18, 19, 20, 22, 24], 21)).toBe(-1)
})

test('Works with empty arrays', (): void => {
  expect(BinarySearch([], 21)).toBe(-1)
})

test('Works for one element arrays', (): void => {
  expect(BinarySearch([1], 1)).toBe(0)
})
