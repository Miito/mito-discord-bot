const BinarySearch = (array: number[], value: number, start: number = 0, end: number = array.length - 1): number => {
  if (start > end) return -1
  const mid = Math.floor((start + end) / 2)
  if (array[mid] > value) return BinarySearch(array, value, start, mid - 1)
  if (array[mid] < value) return BinarySearch(array, value, mid + 1, end)
  return mid
}

export default BinarySearch
