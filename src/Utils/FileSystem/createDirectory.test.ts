import createDirectory from './createDirectory'

test('createDirectory is a Function', (): void => {
  expect(createDirectory).toBeInstanceOf(Function)
})
