import { readFile } from 'fs'

/**
 *
 *
 * @param {string} path
 * @returns {Promise<Buffer>}
 */
async function ReadFile (path: string): Promise<Buffer> {
  return new Promise((resolve, reject): void => {
    readFile(path, (error, data): void => {
      if (error) { return reject(error) }

      resolve(data)
    })
  })
}

export default ReadFile
