import fs from 'fs'

const JSONToFile = (object: any[], filename: string): void => fs.writeFileSync(`${filename}.json`, JSON.stringify(object, null, 2))

export default JSONToFile
