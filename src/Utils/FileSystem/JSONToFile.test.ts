import JSONToFile from './JSONToFile'

jest.mock('fs')

test('JSONToFile is a Function', (): void => {
  expect(JSONToFile).toBeInstanceOf(Function)
})
