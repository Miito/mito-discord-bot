import fs from 'fs'

const createDirectory = (directory: string): void => (!fs.existsSync(directory) ? fs.mkdirSync(directory) : undefined)

export default createDirectory
