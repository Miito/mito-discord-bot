import ReadFile from './ReadFile'

jest.mock('fs')

test('ReadFile is a Function', (): void => {
  expect(ReadFile).toBeInstanceOf(Function)
})
