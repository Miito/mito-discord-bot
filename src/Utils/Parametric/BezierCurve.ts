import { PATH } from './CurveType'
import Vector2 from '../Numerics/Vector2'

class BezierCurve {
  public type: number

  public start: Vector2

  public startControl: Vector2

  public endControl: Vector2

  public end: Vector2

  public constructor (start: Vector2, firstControl: Vector2, secondControl: Vector2, end: Vector2) {
    this.type = PATH.BEZIER_CURVE
    this.start = start
    this.startControl = firstControl
    this.endControl = secondControl
    this.end = end
  }

  public subdivide (t: number, firstHalf: boolean): BezierCurve {
    const ab = Vector2.lerp(this.start, this.startControl, t)
    const bc = Vector2.lerp(this.startControl, this.endControl, t)
    const cd = Vector2.lerp(this.endControl, this.end, t)
    const abbc = Vector2.lerp(ab, bc, t)
    const bccd = Vector2.lerp(bc, cd, t)
    const destination = Vector2.lerp(abbc, bccd, t)
    return firstHalf
      ? new BezierCurve(this.start, ab, abbc, destination)
      : new BezierCurve(destination, bccd, cd, this.end)
  }

  public reverse (): BezierCurve {
    return new BezierCurve(this.end, this.endControl, this.startControl, this.start)
  }
}

export default BezierCurve
