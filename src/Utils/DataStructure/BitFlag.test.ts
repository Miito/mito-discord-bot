import { parseBitFlags } from './BitFlag'

enum TestData {
  KeyZero = 0,
  KeyOne = 1,
  KeyTwo = 1 << 1,
  KeyThree = 1 << 2,
  KeyFour = 1 << 3,
  KeyFive = 1 << 4,
  KeySix = 1 << 5,
  KeySeven = 1 << 6,
  KeyEight = 1 << 7,
  KeyNine = 1 << 8,
}

test('parseBitFlags is a Function', (): void => {
  expect(parseBitFlags).toBeInstanceOf(Function)
})

it('Pass 6 and TestData and return KeyTwo and KeyThree in a Array<String>', (): void => {
  expect(parseBitFlags(6, TestData)).toEqual(['KeyTwo', 'KeyThree'])
})

it('Pass keys higher than available in EnumData', (): void => {
  expect(parseBitFlags(512, TestData)).toEqual([])
})

it('Pass 0 and return key with 0', (): void => {
  expect(parseBitFlags(0, TestData)).toEqual(['KeyZero'])
})
