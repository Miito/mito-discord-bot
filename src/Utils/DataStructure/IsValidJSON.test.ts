import IsValidJSON from './IsValidJSON'

test('IsValidJSON is a Function', (): void => {
  expect(IsValidJSON).toBeInstanceOf(Function)
})

test('{"name":"Adam","age":20} is a valid JSON', (): void => {
  expect(IsValidJSON('{"name":"Adam","age":20}')).toBeTruthy()
})

test('{"name":"Adam",age:"20"} is not a valid JSON', (): void => {
  expect(IsValidJSON('{"name":"Adam",age:"20"}')).toBeFalsy()
})
