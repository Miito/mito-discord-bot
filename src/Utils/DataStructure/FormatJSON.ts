
/**
 *
 *
 * @param {JSON} value
 * @param {number} [tabSize=2]
 * @returns {string}
 */
function FormatJSON (value: JSON, tabSize: number = 2): string {
  return JSON.stringify(value, undefined, tabSize)
}

export default FormatJSON
