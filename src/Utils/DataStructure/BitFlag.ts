/**
 *
 * Find bits in enum data
 *
 * @param {number} bits bitwise flag
 * @param {object} Enum Enum object
 * @returns {string[]} List of match key
 */
function parseBitFlags (bits: number, Enum: object): any[] {
  const FoundFlags = []

  if (bits === 0) {
    FoundFlags.push(Object.keys(Enum).find((key: string): boolean => (Enum as $TsFixMe)[key] === bits))
    return FoundFlags
  }

  for (const i in (Enum)) {
    if (Object.prototype.hasOwnProperty.call(Enum, i)) {
      const bitFlag = (Enum as $TsFixMe)[i]
      if (bitFlag !== Object.values(Enum).find((key: string): boolean => (Enum as $TsFixMe)[0] === (Enum as $TsFixMe)[key]) && (bits & bitFlag) === bitFlag) {
        FoundFlags.push(Object.keys(Enum).find((key: string): boolean => (Enum as $TsFixMe)[key] === (Enum as $TsFixMe)[i]))
      }
    }
  }

  return FoundFlags
}

export { parseBitFlags }
