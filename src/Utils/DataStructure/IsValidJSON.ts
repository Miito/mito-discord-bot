const IsValidJSON = (string: string): boolean => {
  try {
    JSON.parse(string)
    return true
  } catch (error) {
    return false
  }
}

export default IsValidJSON
