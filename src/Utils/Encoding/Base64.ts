const atob = (string: string): string => Buffer.from(string, 'base64').toString('binary')
const btoa = (string: string): string => Buffer.from(string, 'binary').toString('base64')

export { atob, btoa }
