import Hash from './Hash'
import { atob, btoa } from './Base64'

export default { Hash, atob, btoa }
