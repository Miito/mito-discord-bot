import { atob, btoa } from './Base64'

test('atob is a Function', (): void => {
  expect(atob).toBeInstanceOf(Function)
})

test('atob("Zm9vYmFy") equals "foobar"', (): void => {
  expect(atob('Zm9vYmFy')).toBe('foobar')
})

test('atob("Z") returns ""', (): void => {
  expect(atob('Z')).toBe('')
})

test('btoa is a Function', (): void => {
  expect(btoa).toBeInstanceOf(Function)
})

test('btoa("foobar") equals "Zm9vYmFy"', (): void => {
  expect(btoa('foobar')).toBe('Zm9vYmFy')
})
