import crypto from 'crypto'

/**
 * Create hash by string
 *
 * @class Hash
 */
class Hash {
  public async HashCreator (type: string, value: string, digest: $TsFixMe): Promise<string> {
    return new Promise<string>((resolve, reject): void => {
      if (!type) { return reject(new Error('Type not specified.')) }

      const hash = crypto
        .createHash(type)
        .update(value)
        .digest(digest)

      resolve(hash)
    })
  }

  public async SHA256 (value: string, digest: $TsFixMe): Promise<string> {
    return this.HashCreator('sha256', value, digest)
  }

  public async SHA512 (value: string, digest: $TsFixMe): Promise<string> {
    return this.HashCreator('sha512', value, digest)
  }
}

export default Hash
