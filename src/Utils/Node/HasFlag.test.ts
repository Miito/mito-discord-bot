import HasFlags from './HasFlag'

process.argv.push('--arg1', 'value1')

test('hasFlags is a Function', (): void => {
  expect(HasFlags).toBeInstanceOf(Function)
})

it('find flags in process.argv and return a boolean', (): void => {
  expect(HasFlags('--arg1')).toBeTruthy()
  expect(HasFlags('--arg2')).toBeFalsy()
})

it('find flags in process.argv and return a boolean', (): void => {
  process.argv.push('--arg2', 'value2')
  expect(HasFlags('--arg1')).toBeTruthy()
  expect(HasFlags('--arg2')).toBeTruthy()
})
