const HasFlags = (...flags: string[]): boolean => flags.every((flag: string): boolean => process.argv.includes(/^-{1,2}/.test(flag) ? flag : '--' + flag))

export default HasFlags
