const Colour = (): string => { return `#${('000000' + Math.random().toString(16).slice(2, 8).toUpperCase()).slice(-6)}` }
const Number = (min: number, max: number): number => Math.floor(Math.random() * (max - min + 1) + min)

export { Colour, Number }
