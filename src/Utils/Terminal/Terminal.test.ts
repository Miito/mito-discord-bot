import Terminal from './Terminal'

test('Terminal is a Object', (): void => {
  expect(Terminal).toBeInstanceOf(Object)
})

test('Terminal has Function', (): void => {
  expect(Terminal.debug).toBeInstanceOf(Function)
  expect(Terminal.info).toBeInstanceOf(Function)
  expect(Terminal.error).toBeInstanceOf(Function)
  expect(Terminal.log).toBeInstanceOf(Function)
  expect(Terminal.warn).toBeInstanceOf(Function)
})
