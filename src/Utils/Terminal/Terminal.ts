import chalk from 'chalk'
import format from 'date-fns/format'

const Terminal = {
  debug (...params: unknown[]): void {
    console.log(`${chalk.bgBlue(`[ ${format(new Date(), 'YYYY-MM-DD hh:mm:ss')} ] ${chalk.bgBlue.black('DEBUG')} -`)}`, ...params)
  },
  info (...params: unknown[]): void {
    console.log(`${chalk.bgBlue(`[ ${format(new Date(), 'YYYY-MM-DD hh:mm:ss')} ] ${chalk.bgGreen.black('INFO')} -`)}`, ...params)
  },
  error (...params: unknown[]): void {
    console.log(`${chalk.red(`[ ${format(new Date(), 'YYYY-MM-DD hh:mm:ss')} ] ${chalk.bgRed.black('ERROR')} -`)}`, ...params)
  },
  log (...params: unknown[]): void {
    console.log(`${chalk.cyan(`[ ${format(new Date(), 'YYYY-MM-DD hh:mm:ss')} ] ${chalk.bgCyan.black('INFO')} -`)}`, ...params)
  },
  warn (...params: unknown[]): void {
    console.log(`${chalk.yellow(`[ ${format(new Date(), 'YYYY-MM-DD hh:mm:ss')} ] ${chalk.bgYellow.black('WARNING')} -`)}`, ...params)
  }
}

export default Terminal
