/**
 *
 */
function BinomialCoefficient (n: number, k: number): number {
  if (Number.isNaN(n) || Number.isNaN(k)) { return NaN }
  if (k < 0 || k > n) { return 0 }
  if (k === 0 || k === n) { return 1 }
  if (k === 1 || k === n - 1) { return n }
  if (n - k < k) { k = n - k }

  let response = n
  for (let i = 2; i <= k; i++) { response *= (n - i + 1) / i }
  return Math.round(response)
}

export default BinomialCoefficient
