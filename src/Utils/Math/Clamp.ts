/**
 *
 */
function Clamp (minimum: number, maximum: number, value: number): number {
  return Math.min(maximum, Math.max(minimum, value))
}

export default Clamp
