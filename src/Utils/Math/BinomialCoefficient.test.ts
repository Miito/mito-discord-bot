import BinomialCoefficient from './BinomialCoefficient'

describe('BinomialCoefficient', (): void => {
  test('BinomialCoefficient is a Function', (): void => {
    expect(BinomialCoefficient).toBeInstanceOf(Function)
  })

  it('returns the Binomial Coefficient value', (): void => {
    expect(BinomialCoefficient(8, 3)).toBe(56)
  })

  it('returns NaN', (): void => {
    expect(BinomialCoefficient(NaN, 3)).toBeNaN()
  })

  it('Returns NaN', (): void => {
    expect(BinomialCoefficient(5, NaN)).toBeNaN()
  })
})
