import Clamp from './Clamp'

describe('Clamp', (): void => {
  test('clampNumber is a Function', (): void => {
    expect(Clamp).toBeInstanceOf(Function)
  })

  it('Clamps num within the inclusive range specified by the boundary values a and b', (): void => {
    expect(Clamp(2, 3, 5)).toBe(3)
  })
})
