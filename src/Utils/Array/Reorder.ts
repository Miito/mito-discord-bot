/**
 *
 */
function Reorder (array: any[], indexes: {from: number, to: number}): any[] {
  const element = array[indexes.from]

  array.splice(indexes.from, 1)
  array.splice(indexes.to, 0, element)

  return array
}

export default Reorder
