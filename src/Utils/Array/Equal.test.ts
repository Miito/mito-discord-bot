import Equal from './Equal'

test('equals is a Function', (): void => {
  expect(Equal).toBeInstanceOf(Function)
})

test("{ a: [2, {e: 3}], b: [4], c: 'foo' } is equal to { a: [2, {e: 3}], b: [4], c: 'foo' }", (): void => {
  expect(
    Equal({ a: [2, { d: 3 }], b: [4], c: 'foo' }, { a: [2, { d: 3 }], b: [4], c: 'foo' })
  ).toBeTruthy()
})

test('[1,2,3] is equal to [1,2,3]', (): void => {
  expect(Equal([1, 2, 3], [1, 2, 3])).toBeTruthy()
})

test('{ a: [2, 3], b: [4] } is not equal to { a: [2, 3], b: [6] }', (): void => {
  expect(Equal({ a: [2, 3], b: [4] }, { a: [2, 3], b: [6] })).toBeFalsy()
})

test('[1,2,3] is not equal to [1,2,4]', (): void => {
  expect(Equal([1, 2, 3], [1, 2, 4])).toBeFalsy()
})

test('[1, 2, 3] should be equal to { 0: 1, 1: 2, 2: 3 }) - type is different, but their enumerable properties match.', (): void => {
  expect(Equal([1, 2, 3], { 0: 1, 1: 2, 2: 3 })).toBeTruthy()
})

const date = new Date()

test('Two of the same date are equal', (): void => {
  expect(Equal(date, new Date(0))).toBeFalsy()
})

test('null should not be equal to anything', (): void => {
  expect(Equal(null, { a: 'test' })).toBeFalsy()
})

test('undefined should not be equal to anything', (): void => {
  expect(Equal(undefined, { a: 'test' })).toBeFalsy()
})

test('{a: ""} should not be equal to {a: "", b: ""}', (): void => {
  expect(Equal({ a: '' }, { a: '', b: '' })).toBeFalsy()
})

test('Two of the same null, undefined', (): void => {
  expect(Equal([null], [null])).toBeTruthy()
  expect(Equal([undefined], [undefined])).toBeTruthy()
})

test('Two of the same null, undefined', (): void => {
  expect(Equal(null, null)).toBeTruthy()
  expect(Equal(undefined, undefined)).toBeTruthy()
  expect(Equal(null, undefined)).toBeFalsy()
  expect(Equal(undefined, null)).toBeFalsy()
})
