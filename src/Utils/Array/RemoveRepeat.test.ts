import RemoveRepeat from './RemoveRepeat'

test('RemoveRepeat is a Function', (): void => {
  expect(RemoveRepeat).toBeInstanceOf(Function)
})

test('RemoveRepeat on a array', (): void => {
  const ArrayA = [1, 2, 2, 3, 3, 3, 4, 5, 6]
  expect(RemoveRepeat(ArrayA)).toEqual([1, 2, 3, 4, 5, 6])
})
