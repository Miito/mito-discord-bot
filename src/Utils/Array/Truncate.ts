/**
 *
 */
function Truncate (array: any[], length = 0): any[] {
  if (!Array.isArray(array)) {
    throw new SyntaxError(`${array} is not an array`)
  }

  if (array.length > length) {
    array.slice(0, length)
  }

  return array
}

export default Truncate
