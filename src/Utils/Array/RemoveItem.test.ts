import RemoveItem from './RemoveItem'

test('RemoveItem is a Function', (): void => {
  expect(RemoveItem).toBeInstanceOf(Function)
})

test('RemoveItem on a array', (): void => {
  const ArrayA = [244, 244, 2440, 442, 'hello world']
  expect(RemoveItem(ArrayA, 244))
})
