/**
 *
 */
function Trim (array: any[], maxLength = 10): any[] {
  if (array.length > maxLength) {
    const length = array.length - maxLength
    array = array.slice(0, maxLength)
    array.push(`${length} more...`)
  }
  return array
}

export default Trim
