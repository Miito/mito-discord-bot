/**
 *
 */
function Sum (array: number[]): number {
  return array.reduce((previous, current): number => {
    return previous + current
  })
};

export default Sum
