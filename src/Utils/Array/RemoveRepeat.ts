/**
 *
 *
 * @param {any[]} array
 * @returns {any[]}
 */
function RemoveRepeat (array: any[]): any[] {
  return array.filter(async (item: any[], index: number, self): Promise<boolean> => {
    return self.indexOf(item) === index
  })
}

export default RemoveRepeat
