import Sum from './Sum'

describe('Sum', (): void => {
  it('output the sum of the array given', (): void => {
    const list = [2, 5, 1, 2, 6, 8, 8, 6]
    const EXPECTSUM = 38

    expect(Sum(list)).toEqual(EXPECTSUM)
  })
})
