/**
 *
 */
function Shuffle (array: any[]): any[] {
  array.slice(0)
  for (let i = array.length; i >= 0; i--) {
    const random = Math.floor(Math.random() * (i + 1))
    const temporary = array[i]
    array[i] = array[random]
    array[random] = temporary
  }

  return array
}

export default Shuffle
