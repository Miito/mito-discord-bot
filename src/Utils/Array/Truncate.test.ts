import Truncate from './Truncate'

const TestData = [
  'test',
  'test2',
  'test3',
  1,
  2,
  3
]

const ResultData = [
  'test',
  'test2',
  'test3',
  1
]

test('Truncate is a Function', (): void => {
  expect(Truncate).toBeInstanceOf(Function)
})

it('Truncate the last two items in a array', (): void => {
  expect(Truncate(TestData, 2)).toEqual(expect.arrayContaining(ResultData))
})

it('return a SyntaxError when giving value is not a array', (): void => {
  expect((): void => {
    // @ts-ignore
    Truncate(2)
  }).toThrowError(SyntaxError)
  expect((): void => {
    // @ts-ignore
    Truncate('error')
  }).toThrowError(SyntaxError)
})
