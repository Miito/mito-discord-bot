/**
 *
 *
 * @param {any[]} array
 * @param {*} item
 * @returns {boolean}
 */
function RemoveItem (array: any[], item: any): boolean {
  const index = array.indexOf(item)
  return !!~index && !!array.splice(index, 1)
}

export default RemoveItem
