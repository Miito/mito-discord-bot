
/**
 *
 *
 * @template T
 * @template K
 * @param {T[]} a
 * @param {K[]} b
 * @returns {boolean}
 */
function Equal <T, K>(a: T[], b: K[]): boolean {
  if (a === b) { return true }
  if (a instanceof Date && b instanceof Date) { return a.getTime() === b.getTime() }
  if (!a || !b || (typeof a !== 'object' && typeof b !== 'object')) { return a === b }
  if (a === null || a === undefined || b === null || b === undefined) { return false }
  if (a.prototype !== b.prototype) { return false }
  const keys = Object.keys(a)
  if (keys.length !== Object.keys(b).length) { return false }
  return keys.every((k): boolean => Equal(a[k], b[k]))
}

export default Equal
