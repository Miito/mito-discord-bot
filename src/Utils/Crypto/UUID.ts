/* eslint-disable @typescript-eslint/restrict-plus-operands */

import crypto from 'crypto'

const UUIDGeneratorNode = (): string => {
  // @ts-ignore
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c: number): string =>
    (c ^ (crypto.randomBytes(1)[0] & (15 >> (c / 4)))).toString(16)
  )
}

const isUUID = (id: string): boolean => {
  return /^([\da-f]{8}(-[\da-f]{4}){3}-[\da-f]{12}?)$/i.test(id)
}

export { UUIDGeneratorNode, isUUID }
