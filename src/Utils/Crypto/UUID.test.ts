import { UUIDGeneratorNode, isUUID } from './UUID'

test('UUIDGeneratorNode is a Function', (): void => {
  expect(UUIDGeneratorNode).toBeInstanceOf(Function)
})

const uuid = UUIDGeneratorNode()

test('Contains dashes in the proper places', (): void => {
  expect([uuid[8], uuid[13], uuid[18], uuid[23]]).toEqual(['-', '-', '-', '-'])
})

test('Only contains hexadecimal digits', (): void => {
  expect(/^[\d-A-Fa-f]+$/.test(uuid)).toBeTruthy()
})

test('Only contains hexadecimal digits', (): void => {
  expect(isUUID(uuid)).toBeTruthy()
})
