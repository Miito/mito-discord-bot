import IsAbsoluteURL from './IsAbsoluteURL'

test('IsAbsoluteURL is a Function', (): void => {
  expect(IsAbsoluteURL).toBeInstanceOf(Function)
})

test('Given string is an absolute URL', (): void => {
  expect(IsAbsoluteURL('https://google.com')).toBeTruthy()
})

test('Given string is an absolute URL', (): void => {
  expect(IsAbsoluteURL('ftp://www.myserver.net')).toBeTruthy()
})

test('Given string is not an absolute URL', (): void => {
  expect(IsAbsoluteURL('/foo/bar')).toBeFalsy()
})
