/**
 *
 */
function IsAbsoluteURL (string: string): boolean {
  return /^[a-z][\d+-.a-z]*:/.test(string)
}

export default IsAbsoluteURL
