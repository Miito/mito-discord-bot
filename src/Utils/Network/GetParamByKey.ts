/**
 *
 *
 * @param {string} key
 * @param {string} url
 * @returns {string}
 */
function GetParamByKey (key: string, url: string): string {
  key = key.replace(/[[\]]/g, '\\$&')
  const regex: RegExp = new RegExp('[?&]' + key + '(=([^&#]*)|&|#|$)')
  const results: RegExpExecArray | null = regex.exec(url)
  if (!results) return ''
  if (!results[2]) return ''
  return decodeURIComponent(results[2].replace(/\+/g, ' '))
}

export default GetParamByKey
