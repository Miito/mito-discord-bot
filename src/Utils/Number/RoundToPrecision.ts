/**
 *
 */
function RoundToPrecision (x: number, precision: number): number {
  var y = +x + (precision === undefined ? 0.5 : precision / 2)
  return y - (y % (precision === undefined ? 1 : +precision))
}

export default RoundToPrecision
