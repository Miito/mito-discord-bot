/**
 *
 *
 * @param {number} number
 * @param {number} [scale=0]
 * @returns {number}
 */
function Round (number: number, scale: number = 0): number {
  if (!number.toString().includes('e')) {
    return Number(`${Math.round(Number(`${number}e+${scale}`))}e-${scale}`)
  }
  const array = `${number}`.split('e')
  let sig = ''

  if (Number(array[1]) + scale > 0) {
    sig = '+'
  }

  return Number(`${Math.round(Number(`${Number(array[0])}e${sig}${Number(array[1]) + scale}`))}e-${scale}`)
}

export default Round
