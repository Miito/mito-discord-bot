import AbstractError from './AbstractError'

class UnreachableCaseError extends AbstractError {
  public constructor (public message: string) {
    super(message)
  }

  public get name (): string {
    return 'UnreachableCaseError'
  }
}

export default UnreachableCaseError
