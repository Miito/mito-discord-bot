import AbstractError from './AbstractError'

class NotImplementedError extends AbstractError {
  public constructor (public message: string) {
    super(message)
  }

  public get name (): string {
    return 'NotImplementedError'
  }
}

export default NotImplementedError
