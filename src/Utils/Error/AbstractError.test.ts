import AbstractError from './AbstractError'

describe('AbstractError', (): void => {
  it('AbstractError is a Function', (): void => {
    expect(AbstractError).toBeDefined()
    expect(AbstractError).toBeInstanceOf(Function)
  })

  class DummyError extends AbstractError {}

  it('should instantiate', (): void => {
    const error: AbstractError = new DummyError('This is the message')
    expect(error).toEqual(jasmine.any(AbstractError))
  })

  it('should extend from Error', (): void => {
    const error: AbstractError = new DummyError('This is the message')
    expect(error).toEqual(jasmine.any(Error))
  })

  it('should have AbstractError as name', (): void => {
    const error: AbstractError = new DummyError('This is the message')
    expect(error.name).toEqual('AbstractError')
  })

  it('should initialize the message', (): void => {
    const error: AbstractError = new DummyError('This is the message')
    expect(error.message).toEqual('This is the message')
  })
})
