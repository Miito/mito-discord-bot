abstract class AbstractError extends Error {
  public constructor (public message: string) {
    super(message)
    Object.setPrototypeOf(this, new.target.prototype)

    if ('captureStackTrace' in Error) {
      Error.captureStackTrace(this, this.constructor)
    }
  }

  public get name (): string {
    return 'AbstractError'
  }
}

export default AbstractError
