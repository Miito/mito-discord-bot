import AbstractError from './AbstractError'
import NotImplementedError from './NotImplementedError'

describe('NotImplementedError', (): void => {
  it('NotImplementedError is a Function', (): void => {
    expect(NotImplementedError).toBeInstanceOf(Function)
  })

  it('should be instantiable', (): void => {
    const error: NotImplementedError = new NotImplementedError('Message of the error')
    expect(error).toEqual(jasmine.any(NotImplementedError))
  })

  it('should extends from AbstractError', (): void => {
    const error: NotImplementedError = new NotImplementedError('Message of the error')
    expect(error).toEqual(jasmine.any(AbstractError))
  })

  it('should have NotImplementedError as name', (): void => {
    const error: NotImplementedError = new NotImplementedError('This is the message')
    expect(error.name).toEqual('NotImplementedError')
  })
})
