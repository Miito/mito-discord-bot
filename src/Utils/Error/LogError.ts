class logError extends Error {
  public name: string

  public constructor (body: string) {
    super(body)

    this.name = new.target.name || 'logError'

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, logError.prototype)

    fixStack(this)
  }
}

/**
 * Capture and fix the error stack when available
 *
 * Use Error.captureStackTrace
 * Support v8 environments
 */
function fixStack (target: Error, fn: Function = target.constructor): Error {
  const captureStackTrace: Function = (Error as any).captureStackTrace
  return captureStackTrace && captureStackTrace(target, fn)
}

export default logError
