import AbstractError from './AbstractError'
import UnreachableCaseError from './UnreachableCaseError'

describe('UnreachableCaseError', (): void => {
  it('UnreachableCaseError is a Function', (): void => {
    expect(UnreachableCaseError).toBeInstanceOf(Function)
  })

  it('should be instantiable', (): void => {
    const error: UnreachableCaseError = new UnreachableCaseError('Message of the error')
    expect(error).toEqual(jasmine.any(UnreachableCaseError))
  })

  it('should extends from AbstractError', (): void => {
    const error: UnreachableCaseError = new UnreachableCaseError('Message of the error')
    expect(error).toEqual(jasmine.any(AbstractError))
  })

  it('should have UnreachableCaseError as name', (): void => {
    const error: UnreachableCaseError = new UnreachableCaseError('This is the message')
    expect(error.name).toEqual('UnreachableCaseError')
  })

  it('should initialize empty message when no provided', (): void => {
    const error: UnreachableCaseError = new UnreachableCaseError('test')
    expect(error.message).toBe('test')
  })
})
