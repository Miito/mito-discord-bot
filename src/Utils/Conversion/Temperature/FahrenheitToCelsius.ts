const FahrenheitToCelsius = (degrees: number): number => (degrees - 32) * 5 / 9

export default FahrenheitToCelsius
