const CelsiusToFahrenheit = (degrees: number): number => 1.8 * degrees + 32

export default CelsiusToFahrenheit
