import FahrenheitToCelsius from './FahrenheitToCelsius'

test('FahrenheitToCelsius is a Function', (): void => {
  expect(FahrenheitToCelsius).toBeInstanceOf(Function)
})

test('32 Fahrenheit is 0 Celsius', (): void => {
  expect(FahrenheitToCelsius(32)).toBe(0)
})

test('212 Fahrenheit is 100 Celsius', (): void => {
  expect(FahrenheitToCelsius(212)).toBe(100)
})

test('150 Fahrenheit is 65.55555555555556 Celsius', (): void => {
  expect(FahrenheitToCelsius(150)).toBe(65.55555555555556)
})

test('1000 Fahrenheit is 537.7777777777778', (): void => {
  expect(FahrenheitToCelsius(1000)).toBe(537.7777777777778)
})
