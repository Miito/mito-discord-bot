import CelsiusToFahrenheit from './CelsiusToFahrenheit'

test('CelsiusToFahrenheit is a Function', (): void => {
  expect(CelsiusToFahrenheit).toBeInstanceOf(Function)
})

test('0 Celsius is 32 Fahrenheit', (): void => {
  expect(CelsiusToFahrenheit(0)).toBe(32)
})

test('100 Celsius is 212 Fahrenheit', (): void => {
  expect(CelsiusToFahrenheit(100)).toBe(212)
})

test('-50 Celsius is -58 Fahrenheit', (): void => {
  expect(CelsiusToFahrenheit(-50)).toBe(-58)
})

test('1000 Celsius is 1832 Fahrenheit', (): void => {
  expect(CelsiusToFahrenheit(1000)).toBe(1832)
})
