import DegreesToRadians from '../../Number/DegreesToRadians'

test('GetRadian is a Function', (): void => {
  expect(DegreesToRadians).toBeInstanceOf(Function)
})

test('should convert degree to radian', (): void => {
  expect(DegreesToRadians(290)).toBe(5.061454830783556)
  expect(DegreesToRadians(225)).toBe(3.9269908169872414)
  expect(DegreesToRadians(120)).toBe(2.0943951023931953)
})
