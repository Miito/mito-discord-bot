/**
 * Converts an angle from radians to degrees.
 *
 * @param {number} angle in radians.
 * @returns {number} Angle in degrees.
 */
function RadiansToDegrees (angle: number): number {
  return (angle * 180) / Math.PI
}

export default RadiansToDegrees
