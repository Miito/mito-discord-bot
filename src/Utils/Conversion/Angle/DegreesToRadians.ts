/**
 *
 *
 * @param {number} angle
 * @returns {number}
 */
function GetRadian (angle: number): number {
  return (Math.PI / 180) * angle
}

export default GetRadian
