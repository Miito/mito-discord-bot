import RadiansToDegrees from './RadiansToDegrees'

test('GetRadian is a Function', (): void => {
  expect(RadiansToDegrees).toBeInstanceOf(Function)
})

test('should convert degree to radian', (): void => {
  expect(RadiansToDegrees(Math.PI)).toBe(180)
})
