const MphToKmph = (mph: number): number => 1.6093440006146922 * mph

export default MphToKmph
