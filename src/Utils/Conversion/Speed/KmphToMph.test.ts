import KmphToMph from './KmphToMph'

test('KmphToMph is a Function', (): void => {
  expect(KmphToMph).toBeInstanceOf(Function)
})

test('Returns mph from kph.', (): void => {
  expect(KmphToMph(10)).toBe(6.21371192)
})
