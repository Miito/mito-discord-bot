import MphToKmph from './MphToKmph'

test('MphToKmph is a Function', (): void => {
  expect(MphToKmph).toBeInstanceOf(Function)
})

test('Returns kph from mph.', (): void => {
  expect(MphToKmph(10)).toBe(16.09344000614692)
})
