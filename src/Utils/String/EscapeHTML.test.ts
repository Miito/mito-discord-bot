import EscapeHTML from './EscapeHTML'

test('EscapeHTML is a Function', (): void => {
  expect(EscapeHTML).toBeInstanceOf(Function)
})

test('Escapes a string for use in HTML', (): void => {
  expect(EscapeHTML('<a href="#">Me & you</a>')).toBe(
    '&lt;a href=&quot;#&quot;&gt;Me &amp; you&lt;/a&gt;'
  )
})
