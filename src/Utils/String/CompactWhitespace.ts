const CompactWhitespace = (string: string): string => string.replace(/\s{2,}/g, ' ')

export default CompactWhitespace
