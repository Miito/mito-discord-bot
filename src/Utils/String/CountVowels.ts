const CountVowels = (string: string): number => (string.match(/[aeiou]/gi) || []).length

export default CountVowels
