import CountCaps from './CountCaps'

describe('CountCaps', (): void => {
  test('CountCaps is a Function', (): void => {
    expect(CountCaps).toBeInstanceOf(Function)
  })

  it('CountCaps the first letter of a string', (): void => {
    expect(CountCaps('fooBar')).toEqual(1)
  })

  it('Capitalizes the first letter of a string', (): void => {
    expect(CountCaps('FooBar')).toEqual(2)
  })

  it('Works with characters', (): void => {
    expect(CountCaps('FOOBAR')).toEqual(6)
  })

  it('Works with characters', (): void => {
    expect(CountCaps('FoObAr')).toEqual(3)
  })
})
