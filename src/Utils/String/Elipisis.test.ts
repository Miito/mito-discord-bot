import Elipisis from './Elipisis'

describe('Elipisis', (): void => {
  test('Elipisis is a Function', (): void => {
    expect(Elipisis).toBeInstanceOf(Function)
  })

  it('Elipisis some text', (): void => {
    const text = 'FoobarFoobarFoobarFoobarFoobar'
    expect(Elipisis(text, 6)).toBe('Foo...')
  })

  it('Elipisis some text', (): void => {
    const text = 'Foob4rFoob4rFoob4rFoob4rFoob4r'
    expect(Elipisis(text, 9)).toBe('Foob4r...')
  })

  it('Elipisis some text', (): void => {
    const text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tempus imperdiet mauris sit amet lacinia. Donec nisl ipsum, consectetur at accumsan et, ultrices molestie diam. Nam dapibus lectus a risus faucibus, id laoreet odio porta. Etiam mollis ullamcorper orci, sed porttitor nunc cursus ac. Mauris tempor sed odio eget elementum. Praesent bibendum quam lacus, sit amet finibus arcu eleifend nec.'
    expect(Elipisis(text)).toBe('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tempus imperdiet mauris sit amet ...')
  })
})
