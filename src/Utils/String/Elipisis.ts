/**
 *
 *
 * @param {string} string
 * @param {number} [length=100]
 * @returns {string}
 */
function Elipisis (string: string, length: number = 100): string {
  return (
    string.length > length ? `${string.substr(0, length - 3)}...` : string
  )
}

export default Elipisis
