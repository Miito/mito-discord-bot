/**
 * **split** splits a string by a given separator
 *
 * @param {string} separator deliminator/separator
 * @param {string} string string to be split
 * @returns {string[]} an array of strings
 */
function Split (separator: string, string: string): string[] {
  return string.split(separator)
}

export default Split
