// @ts-ignore
const EscapeHTML = (string: string): string => string.replace(/["&'<>]/g, (tag: string): string => ({ '&': '&amp;', '<': '&lt;', '>': '&gt;', "'": '&#39;', '"': '&quot;' }[tag] || tag))

export default EscapeHTML
