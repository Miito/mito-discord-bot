import CountVowels from './CountVowels'

describe('CountVowels', (): void => {
  test('CountVowels is a Function', (): void => {
    expect(CountVowels).toBeInstanceOf(Function)
  })

  it('CountVowels returns the correct count', (): void => {
    expect(CountVowels('foobar')).toBe(3)
  })

  it('CountVowels returns the correct count', (): void => {
    expect(CountVowels('ggg')).toBe(0)
  })
})
