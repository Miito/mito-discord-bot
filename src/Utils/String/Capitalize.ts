const Capitalize = ([first, ...rest]: string | string[], lowerRest = false): string => first.toUpperCase() + (lowerRest ? rest.join('').toLowerCase() : rest.join(''))

export default Capitalize
