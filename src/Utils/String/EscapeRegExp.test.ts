import EscapeRegExp from './EscapeRegExp'

test('EscapeRegExp is a Function', (): void => {
  expect(EscapeRegExp).toBeInstanceOf(Function)
})

test('Escapes a string to use in a regular expression', (): void => {
  expect(EscapeRegExp('(test)')).toBe('\\(test\\)')
})
