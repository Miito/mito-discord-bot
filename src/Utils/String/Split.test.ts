import Split from './Split'

test('Split is a Function', (): void => {
  expect(Split).toBeInstanceOf(Function)
})

it('Split a sting by a separator', (): void => {
  expect(Split('/', '/usr/local/')).toBe(['', 'usr', 'local'])
  expect(Split('', 'hello world!')).toBe([''])
})
