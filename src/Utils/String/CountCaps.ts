/**
 *
 */
function CountCaps (capcount: string): number {
  return (capcount.replace(/[^A-Z]/g, '').length)
}

export default CountCaps
