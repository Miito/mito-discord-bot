import CompactWhitespace from './CompactWhitespace'

test('CompactWhitespace is a Function', (): void => {
  expect(CompactWhitespace).toBeInstanceOf(Function)
})

test('CompactWhitespace returns a string with compacted whitespaces', (): void => {
  expect(CompactWhitespace('Lorem Ipsum')).toBe('Lorem Ipsum')
})

test('CompactWhitespace returns a string with compacted whitespaces', (): void => {
  expect(CompactWhitespace('Lorem    Ipsum')).toBe('Lorem Ipsum')
})

test('CompactWhitespace returns a string with compacted whitespaces', (): void => {
  expect(CompactWhitespace('Lorem \t Ipsum')).toBe('Lorem Ipsum')
})

test('CompactWhitespace returns a string with compacted whitespaces', (): void => {
  expect(CompactWhitespace('\t   Lorem \n\n  \n Ipsum  ')).toBe(' Lorem Ipsum ')
})
