const CapitalizeEveryWord = (string: string): string => string.replace(/\b[a-z]/g, (character: string): string => character.toUpperCase())

export default CapitalizeEveryWord
