const EscapeRegExp = (string: string): string => string.replace(/[$()*+.?[\\\]^{|}]/g, '\\$&')

export default EscapeRegExp
