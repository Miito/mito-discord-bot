import CapitalizeEveryWord from './CapitalizeEveryWord'

test('CapitalizeEveryWord is a Function', (): void => {
  expect(CapitalizeEveryWord).toBeInstanceOf(Function)
})

test('Capitalizes the first letter of every word in a string', (): void => {
  expect(CapitalizeEveryWord('hello world!')).toBe('Hello World!')
})
test('Works with characters', (): void => {
  expect(CapitalizeEveryWord('$# @!')).toBe('$# @!')
})

test('Works with one word string', (): void => {
  expect(CapitalizeEveryWord('a')).toBe('A')
})
