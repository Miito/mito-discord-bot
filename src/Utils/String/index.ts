import Capitalize from './Capitalize'
import CountCaps from './CountCaps'
import Elipisis from './Elipisis'

export { Capitalize, CountCaps, Elipisis }
