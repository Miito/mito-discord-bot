import Capitalize from './Capitalize'

describe('Capitalize', (): void => {
  test('Capitalize is a Function', (): void => {
    expect(Capitalize).toBeInstanceOf(Function)
  })

  it('Capitalizes the first letter of a string', (): void => {
    expect(Capitalize('fooBar')).toBe('FooBar')
  })

  it('Capitalizes the first letter of a string', (): void => {
    expect(Capitalize('fooBar', true)).toBe('Foobar')
  })

  it('Works with characters', (): void => {
    expect(Capitalize('#!#', true)).toBe('#!#')
  })

  it('"Works with single character words', (): void => {
    expect(Capitalize('a', true)).toBe('A')
  })
})
