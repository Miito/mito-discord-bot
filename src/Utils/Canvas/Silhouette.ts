import { CanvasRenderingContext2D } from 'canvas'

/**
 *
 */
function Silhouette (context: CanvasRenderingContext2D, x: number, y: number, width: number, height: number): CanvasRenderingContext2D {
  const data = context.getImageData(x, y, width, height)

  for (let i = 0; i < data.data.length; i += 4) {
    data.data[i] = 0
    data.data[i + 1] = 0
    data.data[i + 2] = 0
  }

  context.putImageData(data, x, y)
  return context
}

export default Silhouette
