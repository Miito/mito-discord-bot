import { CanvasRenderingContext2D } from 'canvas'

/**
 *
 */
function DrawImageWithTint (context: CanvasRenderingContext2D, image: CanvasRenderingContext2D, color: string, x: number, y: number, width: number, height: number): void {
  const { fillStyle, globalAlpha } = context
  context.fillStyle = color
  context.drawImage(image, x, y, width, height)
  context.globalAlpha = 0.5
  context.fillRect(x, y, width, height)
  context.fillStyle = fillStyle
  context.globalAlpha = globalAlpha
}

export default DrawImageWithTint
