import { CanvasRenderingContext2D } from 'canvas'

/**
 *
 */
function ShortenText (context: CanvasRenderingContext2D, text: string, maxWidth: number): string {
  let shorten
  if (context.measureText(text).width > maxWidth) shorten = true
  while (context.measureText(text).width > maxWidth) text = text.substr(0, text.length - 1)
  return shorten ? `${text}...` : text
}

export default ShortenText
