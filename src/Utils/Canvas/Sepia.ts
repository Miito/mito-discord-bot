import { CanvasRenderingContext2D } from 'canvas'

/**
 *
 */
function Sepia (context: CanvasRenderingContext2D, x: number, y: number, width: number, height: number): CanvasRenderingContext2D {
  const data = context.getImageData(x, y, width, height)

  for (let i = 0; i < data.data.length; i += 4) {
    const brightness = (0.34 * data.data[i]) + (0.5 * data.data[i + 1]) + (0.16 * data.data[i + 2])
    data.data[i] = brightness + 100
    data.data[i + 1] = brightness + 50
    data.data[i + 2] = brightness
  }

  context.putImageData(data, x, y)
  return context
}

export default Sepia
