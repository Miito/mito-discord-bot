import { CanvasRenderingContext2D } from 'canvas'

/**
 *
 */
function Invert (context: CanvasRenderingContext2D, x: number, y: number, width: number, height: number): CanvasRenderingContext2D {
  const data = context.getImageData(x, y, width, height)

  for (let i = 0; i < data.data.length; i += 4) {
    data.data[i] = 255 - data.data[i]
    data.data[i + 1] = 255 - data.data[i + 1]
    data.data[i + 2] = 255 - data.data[i + 2]
  }

  context.putImageData(data, x, y)
  return context
}

export default Invert
