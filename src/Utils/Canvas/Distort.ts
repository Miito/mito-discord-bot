import { CanvasRenderingContext2D } from 'canvas'

/**
 *
 */
function Distort (context: CanvasRenderingContext2D, amplitude: number, x: number, y: number, width: number, height: number, strideLevel: number = 4): CanvasRenderingContext2D {
  const data = context.getImageData(x, y, width, height)
  const temporary = context.getImageData(x, y, width, height)
  const stride = width * strideLevel

  for (let i = 0; i < width; i++) {
    for (let j = 0; j < height; j++) {
      const xs = Math.round(amplitude * Math.sin(2 * Math.PI * 3 * (j / height)))
      const ys = Math.round(amplitude * Math.cos(2 * Math.PI * 3 * (i / width)))
      const destination = (j * stride) + (i * strideLevel)
      const source = ((j + ys) * stride) + ((i + xs) * strideLevel)
      data.data[destination] = temporary.data[source]
      data.data[destination + 1] = temporary.data[source + 1]
      data.data[destination + 2] = temporary.data[source + 2]
    }
  }
  context.putImageData(data, x, y)
  return context
}

export default Distort
