const QuickSort = ([n, ...nums]: number[], desc = false): number[] =>
  isNaN(n)
    ? []
    : [
      ...QuickSort(nums.filter((v: number): boolean => (desc ? v > n : v <= n)), desc),
      n,
      ...QuickSort(nums.filter((v: number): boolean => (!desc ? v > n : v <= n)), desc)
    ]

export default QuickSort
