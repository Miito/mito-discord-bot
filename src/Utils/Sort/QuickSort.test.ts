import QuickSort from './QuickSort'

test('QuickSort is a Function', (): void => {
  expect(QuickSort).toBeInstanceOf(Function)
})

test('QuickSort([5, 6, 4, 3, 1, 2]) returns [1, 2, 3, 4, 5, 6]', (): void => {
  expect(QuickSort([5, 6, 4, 3, 1, 2])).toEqual([1, 2, 3, 4, 5, 6])
})

test('QuickSort([-1, 0, -2]) returns [-2, -1, 0]', (): void => {
  expect(QuickSort([-1, 0, -2])).toEqual([-2, -1, 0])
})

const start = new Date().getTime()
QuickSort([11, 1, 324, 23232, -1, 53, 2, 524, 32, 13, 156, 133, 62, 12, 4])
const end = new Date().getTime()

test('QuickSort([11, 1, 324, 23232, -1, 53, 2, 524, 32, 13, 156, 133, 62, 12, 4]) takes less than 2s to run', (): void => {
  expect(end - start < 2000).toBeTruthy()
})
