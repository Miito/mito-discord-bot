import { Is } from './Is'

test('Is is a Function', (): void => {
  expect(Is).toBeInstanceOf(Function)
})

test('Works for arrays with data', (): void => {
  expect(Is(Array, [1])).toBeTruthy()
})

test('Works for empty arrays', (): void => {
  expect(Is(Array, [])).toBeTruthy()
})

test('Works for arrays, not objects', (): void => {
  expect(Is(Array, {})).toBeFalsy()
})

test('Works for objects', (): void => {
  expect(Is(Object, {})).toBeTruthy()
})

test('Works for maps', (): void => {
  expect(Is(Map, new Map())).toBeTruthy()
})

test('Works for regular expressions', (): void => {
  expect(Is(RegExp, /./g)).toBeTruthy()
})

test('Works for sets', (): void => {
  expect(Is(Set, new Set())).toBeTruthy()
})

test('Works for weak maps', (): void => {
  expect(Is(WeakMap, new WeakMap())).toBeTruthy()
})

test('Works for weak sets', (): void => {
  expect(Is(WeakSet, new WeakSet())).toBeTruthy()
})

test('Works for strings - returns true for primitive', (): void => {
  expect(Is(String, '')).toBeTruthy()
})

test('Works for strings - returns true when using constructor', (): void => {
  expect(Is(String, String(''))).toBeTruthy()
})

test('Works for numbers - returns true for primitive', (): void => {
  expect(Is(Number, 1)).toBeTruthy()
})

test('Works for numbers - returns true when using constructor', (): void => {
  expect(Is(Number, Number('10'))).toBeTruthy()
})

test('Works for booleans - returns true for primitive', (): void => {
  expect(Is(Boolean, false)).toBeTruthy()
})

test('Works for booleans - returns true when using constructor', (): void => {
  expect(Is(Boolean, Boolean(false))).toBeTruthy()
})

test('Works for functions', (): void => {
  expect(Is(Function, (): null => null)).toBeTruthy()
})
