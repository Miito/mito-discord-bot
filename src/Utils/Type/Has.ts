/**
 *
 *
 * @param {*} given
 * @param {*} required
 * @returns {boolean}
 */
function Has (given: any, required: any): boolean {
  return (given & required) > 0
}

/**
 *
 *
 * @param {object} object
 * @param {string} functionName
 * @returns {boolean}
 */
function HasFunction (object: Object, functionName: string): boolean {
  return typeof object[ functionName ] === 'function'
}

export { Has, HasFunction }
