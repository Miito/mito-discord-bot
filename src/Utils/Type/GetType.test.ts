import GetType from './GetType'

test('GetType is a Function', (): void => {
  expect(GetType).toBeInstanceOf(Function)
})

test('Returns the native type of a value', (): void => {
  expect(GetType(new Set([1, 2, 3]))).toBe('set')
})

test('Returns null for null', (): void => {
  expect(GetType(null)).toBe('null')
})

test('Returns undefined for undefined', (): void => {
  expect(GetType(undefined)).toBe('undefined')
})
