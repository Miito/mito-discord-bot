/**
 *
 *
 * @param {*} type the type
 * @param {*} value the value
 * @returns {boolean}
 */
function Is (type: any, value: any): boolean {
  return ![null].includes(value) && value.constructor === type
}

/**
 *
 *
 * @param {*} value
 * @returns {boolean}
 */
function isDefined (value: any): boolean {
  return void 0 !== value
}

/**
 *
 *
 * @param {*} value
 * @returns {boolean}
 */
function isNull (value: any): boolean {
  return value === null
}

/**
 *
 *
 * @param {*} object
 * @returns {object is Array<any>}
 */
function isArray (object: any): object is any[] {
  return Array.isArray(object)
}

/**
 *
 *
 * @param {*} value
 * @returns {value is string}
 */
function isString (value: any): value is string {
  return typeof value === 'string' || value instanceof String
}

/**
 *
 *
 * @param {*} value
 * @returns {value is boolean}
 */
function isBoolean (value: any): value is boolean {
  return typeof value === 'boolean'
}

/**
 *
 *
 * @param {*} value
 * @returns {value is number}
 */
function isNumber (value: any): value is number {
  return typeof value === 'number' || value instanceof Number
}

/**
 *
 *
 * @param {*} value
 * @returns {boolean}
 */
function isInteger (value: any): boolean {
  if (!isNumber(value)) return false
  return value % 1 === 0
}

/**
 *
 *
 * @param {*} value
 * @returns {boolean}
 */
function isDouble (value: any): boolean {
  if (!isNumber(value)) return false
  return value % 1 !== 0
}

/**
 *
 *
 * @param {*} date
 * @returns {date is Date}
 */
function isDate (date: any): date is Date {
  return date instanceof Date || (typeof date === 'object' && Object.prototype.toString.call(date) === '[object Date]')
}

/**
 *
 *
 * @param {*} object
 * @returns {object is object}
 */
function isObject (object: any): object is object {
  return typeof object === 'object' && (!!object)
}

/**
 *
 *
 * @param {*} value
 * @returns {value is Function}
 */
function isFunction (value: any): value is Function {
  return typeof value === 'function'
}

export { Is, isDefined, isNull, isArray, isBoolean, isDate, isInteger, isDouble, isString, isFunction, isObject }
