import { Has, HasFunction } from './Has'

test('Has is a Function', (): void => {
  expect(Has).toBeInstanceOf(Function)
})

test('HasFunction is a Function', (): void => {
  expect(HasFunction).toBeInstanceOf(Function)
})

test('Has function', (): void => {
  expect(Has(1, 0)).toBeFalsy()
  expect(Has(1, 1)).toBeTruthy()
  expect(Has([1], [0])).toBeFalsy()
  expect(Has([1], [1])).toBeTruthy()
  expect(Has(null, null)).toBeTruthy()
  expect(Has(undefined, undefined)).toBeTruthy()
})
