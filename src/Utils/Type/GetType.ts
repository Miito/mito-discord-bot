const GetType = (v: any): string => v === undefined ? 'undefined' : v === null ? 'null' : v.constructor.name.toLowerCase()

export default GetType
