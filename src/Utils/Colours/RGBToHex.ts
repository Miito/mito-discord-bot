/**
 *
 *
 * @param {number[]} array
 * @returns {string}
 */
function RGBToHex (array: number[]): string {
  return ((array[2] | array[1] << 8 | array[0] << 16) | 1 << 24).toString(16).slice(1)
}

export default RGBToHex
