import RGBToHex from './RGBToHex'

test('RGBToHex is a Function', (): void => {
  expect(RGBToHex).toBeInstanceOf(Function)
})
it('Converts the values of RGB components to a color code.', (): void => {
  expect(RGBToHex([255, 165, 1])).toBe('ffa501')
})
