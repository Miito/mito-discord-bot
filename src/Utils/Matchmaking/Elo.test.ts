import Elo from './Elo'

describe('Elo', (): void => {
  test('elo is a Function', (): void => {
    expect(Elo).toBeInstanceOf(Function)
  })

  it('Standard 1v1s', (): void => {
    expect(Elo([1200, 1200])).toEqual([1216, 1184])
  })

  it('Standard 1v1s', (): void => {
    expect(Elo([1200, 1200], 64)).toEqual([1232, 1168])
  })

  it('4 player FFA, all same rank', (): void => {
    expect(Elo([1200, 1200, 1200, 1200]).map(Math.round)).toEqual([1246, 1215, 1185, 1154])
  })
})
