const Elo = ([...ratings], kFactor = 32, selfRating?: number): number[] => {
  const [a, b] = ratings
  const expectedScore = (self: number, opponent: number): number => 1 / (1 + 10 ** ((opponent - self) / 400))
  const newRating = (rating: number, i: number): number => (selfRating || rating) + kFactor * (i - expectedScore(i ? a : b, i ? b : a))

  if (ratings.length === 2) return [newRating(a, 1), newRating(b, 0)]

  for (let i = 0, length = ratings.length; i < length; i++) {
    let j = i
    while (j < length - 1) {
      j++;
      [ratings[i], ratings[j]] = Elo([ratings[i], ratings[j]], kFactor)
    }
  }
  return ratings
}

export default Elo
