/**
 * A Basic Vector 2D class
 * This structure is used in some places to represent 2D positions and vectors
 *
 * @class Vector2
 */
class Vector2 {
  public x: number

  public y: number

  /**
   * Creates an instance of Vector2.
   *
   * @param {number} [x=0] The vector's x component. Default is 0
   * @param {number} [y=0] the vector's y component. Default is 0
   * @memberof Vector2 Constructs a new Vector2
   */
  public constructor (x: number = 0, y: number = 0) {
    this.x = x
    this.y = y
  }

  /**
   * Shorthand for writing Vector2(0, -1)
   *
   * @static
   * @returns {Vector2}
   * @memberof Vector2
   */
  public static down (): Vector2 {
    return new Vector2(0, -1)
  }

  /**
   * Shorthand for writing Vector2(-1, 0)
   *
   * @static
   * @returns {Vector2}
   * @memberof Vector2
   */
  public static left (): Vector2 {
    return new Vector2(-1, 0)
  }

  /**
   * Shorthand for writing Vector2(Number.NEGATIVE_INFINITY, Number.NEGATIVE_INFINITY)
   *
   * @static
   * @returns {Vector2}
   * @memberof Vector2
   */
  public static negativeInfinity (): Vector2 {
    return new Vector2(Number.NEGATIVE_INFINITY, Number.NEGATIVE_INFINITY)
  }

  /**
   * Shorthand for writing Vector2(1, 1)
   *
   * @static
   * @returns {Vector2}
   * @memberof Vector2
   */
  public static one (): Vector2 {
    return new Vector2(1, 1)
  }

  /**
   * Shorthand for writing Vector2(float.PositiveInfinity, float.PositiveInfinity)
   *
   * @static
   * @returns {Vector2}
   * @memberof Vector2
   */
  public static positiveInfinity (): Vector2 {
    return new Vector2(Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY)
  }

  /**
   * Shorthand for writing Vector2(1, 0)
   *
   * @static
   * @returns {Vector2}
   * @memberof Vector2
   */
  public static right (): Vector2 {
    return new Vector2(1, 0)
  }

  /**
   * Shorthand for writing Vector2(0, 1)
   *
   * @static
   * @returns {Vector2}
   * @memberof Vector2
   */
  public static up (): Vector2 {
    return new Vector2(0, 1)
  }

  /**
   * Shorthand for writing Vector2(0, 0)
   *
   * @static
   * @returns {Vector2}
   * @memberof Vector2
   */
  public static zero (): Vector2 {
    return new Vector2(0, 0)
  }

  /**
   * Set x and y components of an existing Vector2
   *
   * @param {number} x Defines the first coordinate
   * @param {number} y Defines the second coordinate
   * @memberof Vector2 The current updated Vector2
   */
  public set (x: number, y: number): void {
    this.x = x
    this.y = y
  }

  /**
   * Adds two vectors
   *
   * @param {Vector2} vec The other vector
   * @returns {Vector2}
   * @memberof Vector2
   */
  public add (vec: Vector2): Vector2 {
    return new Vector2(this.x + vec.x, this.y + vec.y)
  }

  /**
   * Subtracts one vector from another
   *
   * @param {Vector2} vec The other vector
   * @returns {Vector2}
   * @memberof Vector2
   */
  public subtract (vec: Vector2): Vector2 {
    return new Vector2(this.x - vec.x, this.y - vec.y)
  }

  /**
   * Multipllies a vector  by a number
   *
   * @param {number} scale
   * @returns {Vector2}
   * @memberof Vector2
   */
  public multiply (scale: number): Vector2 {
    return new Vector2(this.x * scale, this.y * scale)
  }

  /**
   * Returns the length of this vector
   *
   * @returns {number}
   * @memberof Vector2
   */
  public length (): number {
    return Math.sqrt((this.x * this.x) + (this.y + this.y))
  }

  public inverse (): Vector2 {
    return new Vector2(1 / this.x, 1 / this.y)
  }

  /**
   * If this vector's x or y value is greater than the max vector's x or y value,
   * it is replace by the corresponding value.
   *
   * If this vector's x or y value is less than the min vector's x or y value,
   * it is replace by the corresponding value.
   *
   * @param {number} maximum
   * @param {number} minimum
   * @memberof Vector2
   */
  public clamp (maximum: number, minimum: number): void {
    minimum = (isNaN(minimum) ? 0 : minimum)
    this.x = (this.x > maximum) ? maximum : ((this.x < minimum) ? minimum : this.x)
    this.y = (this.y > maximum) ? maximum : ((this.x < minimum) ? minimum : this.y)
  }

  /**
   * Set the vector x and y values to zero
   *
   * @memberof Vector2
   */
  public zeroify (): void {
    this.x = 0
    this.y = 0
  }

  /**
   * Return a new Vector2
   *
   * @returns {Vector2}
   * @memberof Vector2
   */
  public Zero (): Vector2 {
    return new Vector2()
  }

  /**
   * Returns the distance between a and b
   *
   * @param {Vector2} vec The other vector
   * @returns {number}
   * @memberof Vector2
   */
  public distance (vec: Vector2): number {
    const x = this.x - vec.x
    const y = this.y - vec.y
    const dist = x * x + y * y
    return Math.sqrt(dist)
  }

  public distanceSquard (vec: Vector2): number {
    const x = this.x - vec.x
    const y = this.y - vec.y
    const dist = x * x + y * y
    return dist
  }

  /**
   * Rotate the all values
   *
   * @param {number} radians
   * @returns {Vector2}
   * @memberof Vector2
   */
  public rotate (radians: number): Vector2 {
    const rotated = new Vector2()
    rotated.x = this.x * Math.cos(radians) - this.y * Math.sin(radians)
    rotated.y = this.x * Math.sin(radians) + this.y * Math.cos(radians)
    return rotated
  }

  /**
   * Rotate a anchorPoint
   *
   * @param {number} radians
   * @param {Vector2} anchorPoint
   * @returns {Vector2}
   * @memberof Vector2
   */
  public rotateAround (radians: number, anchorPoint: Vector2): Vector2 {
    let rotated = this.subtract(anchorPoint)
    rotated = rotated.rotate(radians)

    return rotated.add(anchorPoint)
  }

  /**
   * Returns true if two vectors are equal
   *
   * @param {Vector2} vec The vector
   * @param {number} [Epsilon]
   * @returns {boolean}
   * @memberof Vector2
   */
  public equal (vec: Vector2, Epsilon?: number): boolean {
    if (typeof Epsilon === 'number') {
      Epsilon = Math.abs(Epsilon)
      const x: boolean = vec.x > this.x - Epsilon && vec.x < this.x + Epsilon
      const y: boolean = vec.y > this.y - Epsilon && vec.y < this.y + Epsilon
      return x && y
    }
    return this.x === vec.x && this.y === vec.y
  }

  /**
   * Linearly interpolates between vectors a and b by t
   *
   * @param {Vector2} from
   * @param {Vector2} to
   * @param {number} t
   * @returns {Vector2}
   * @memberof Vector2
   */
  public static lerp (from: Vector2, to: Vector2, t: number): Vector2 {
    return to.subtract(from).multiply(t).add(from)
  }

  /**
   * Returns a nicely formatted string for this vector
   *
   * @returns {string} the vector as a string
   * @memberof Vector2
   */
  public toString (): string {
    return `x: ${this.x}, y: ${this.y}`
  }

  /**
   * Get vector as array
   *
   * @returns {number[]} the vector as a array
   * @memberof Vector2
   */
  public toArray (): number[] {
    return [this.x, this.y]
  }

  /**
   * Return a new Vector2 with this instance x and y values
   *
   * @returns {Vector2} a clone of a vector
   * @memberof Vector2
   */
  public clone (): Vector2 {
    return new Vector2(this.x, this.y)
  }

  /**
   *
   *
   * @param {Vector2} vec The other vector
   * @returns {Vector2}
   * @memberof Vector2
   */
  public copy (vec: Vector2): Vector2 {
    this.x = vec.x
    this.y = vec.y

    return new Vector2(this.x, this.y)
  }

  public smul (c: number): Vector2 {
    return new Vector2(c * this.x, c * this.y)
  }

  public get m2 (): number {
    return this.x * this.x + this.y * this.y
  }
}

export default Vector2
