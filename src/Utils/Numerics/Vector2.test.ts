import Vector2 from './Vector2'

test('Vector2 is a Function', (): void => {
  expect(Vector2).toBeInstanceOf(Function)
})

test('should construct a instance of Vector2', (): void => {
  const VectorA = new Vector2()
  expect(VectorA.x).toBe(0)
  expect(VectorA.x).toBe(0)
})

test('should copy Vector2 value to another Vector2', (): void => {
  const VectorA = new Vector2(2, 4)
  const VectorB = new Vector2().copy(VectorA)

  expect(VectorB.x).toBe(2)
  expect(VectorB.y).toBe(4)

  VectorA.x = 0
  VectorA.y = -1

  expect(VectorA.x).toBe(0)
  expect(VectorA.y).toBe(-1)
})

test('should set Vector2 values', (): void => {
  const VectorA = new Vector2()
  expect(VectorA).toEqual(Vector2.zero())

  VectorA.set(3, 6)
  expect(VectorA).toEqual(new Vector2(3, 6))
})

test('should subtract a Vector2 from another Vector2', (): void => {
  const VectorA = Vector2.one()
  const VectorB = Vector2.zero()
  const VectorC = new Vector2(10, -45)
  const VectorD = new Vector2(-1, -12)
  const Vector1 = new Vector2(1.1, -2.4)

  expect(VectorA.subtract(VectorB)).toEqual(VectorA)
  expect(VectorC.subtract(VectorD)).toEqual(new Vector2(11, -33))
  expect(VectorD.subtract(Vector1)).toEqual(new Vector2(-2.1, -9.6))
})

test('should return boolean when vector are equal or not', (): void => {
  const VectorA = Vector2.zero()
  const VectorB = Vector2.one()
  const VectorC = new Vector2(-2, 3)
  const VectorD = new Vector2(-2.1, 2.9)
  const Vector1 = Vector2.one()

  expect(VectorA.equal(VectorA)).toBeTruthy()
  expect(VectorA.equal(VectorB)).toBeFalsy()
  expect(VectorB.equal(Vector1)).toBeTruthy()
  expect(VectorC.equal(VectorD)).toBeFalsy()
  expect(VectorA.equal(VectorA, 0.1)).toBeTruthy()
  expect(VectorC.equal(VectorD, 0.1)).toBeFalsy()
  expect(VectorC.equal(VectorD, -0.11)).toBeTruthy()
  expect(VectorC.equal(VectorD, 0.11)).toBeTruthy()
})

test('should calculate Vector2 distance', (): void => {
  const VectorA = new Vector2(3, 4)
  const VectorB = new Vector2(6, 8)
  const VectorC = new Vector2(-24.67, 44.89)

  expect(VectorA.distance(VectorB)).toEqual(5)
  expect(Vector2.zero().distance(VectorB)).toEqual(10)
  expect(Vector2.one().distance(Vector2.one())).toEqual(0)
  expect(VectorC.distance(VectorC)).toBeCloseTo(0)
  expect(VectorC.distance(VectorB)).toBeCloseTo(47.97417)
})

test('should run Vector2.toString', (): void => {
  const VectorA = new Vector2(-1, 15)
  const VectorB = new Vector2(-1.876, -4)

  expect(Vector2.up().toString()).toBe('x: 0, y: 1')
  expect(Vector2.zero().toString()).toBe('x: 0, y: 0')
  expect(VectorA.toString()).toBe('x: -1, y: 15')
  expect(VectorB.toString()).toBe('x: -1.876, y: -4')
})

test('should run Vector2.toArray', (): void => {
  const VectorA = new Vector2(-1, 15)
  const VectorB = new Vector2(-1.876, -4)

  expect(Vector2.up().toArray()).toEqual([0, 1])
  expect(Vector2.zero().toArray()).toEqual([0, 0])
  expect(VectorA.toArray()).toEqual([-1, 15])
  expect(VectorB.toArray()).toEqual([-1.876, -4])
})

test('should add two Vector2 together', (): void => {
  const VectorA = new Vector2(2, 3)
  const VectorB = new Vector2(1, 4)

  expect(VectorA.add(VectorB)).toEqual(new Vector2(3, 7))
})

test('should multiply two Vector2 together', (): void => {
  const VectorA = new Vector2(2, 3)

  expect(VectorA.multiply(2)).toEqual(new Vector2(4, 6))
})

test('should return Vector2 length', (): void => {
  const VectorA = new Vector2(2, 3)

  expect(VectorA.length()).toEqual(3.1622776601683795)
})

test('should inverse Vector2', (): void => {
  const VectorA = new Vector2(2, 3)

  expect(VectorA.inverse()).toEqual(new Vector2(0.5, 0.3333333333333333))
})

test('should zeroify Vector2', (): void => {
  const VectorA = new Vector2(2, 3)

  expect(VectorA.zeroify()).toEqual(new Vector2(0, 0))
})

test('should clone Vector2', (): void => {
  const VectorA = new Vector2(2, 3)

  expect(VectorA.clone()).toEqual(new Vector2(2, 3))
})

test('should clamp Vector2', (): void => {
  const VectorA = new Vector2(1, 28)

  expect(VectorA.clamp(2, 10)).toEqual(new Vector2())
})

test('should distanceSquard Vector2', (): void => {
  const VectorA = new Vector2(1, 28)
  const VectorB = new Vector2(6, 8)

  expect(VectorA.distanceSquard(VectorB)).toEqual(425)
})

test('should rotate Vector2', (): void => {
  const VectorA = new Vector2(1, 28)
  const RADIANS = 45

  expect(VectorA.rotate(RADIANS)).toEqual(new Vector2(-23.299976698137588, 15.55991921143055))
})

test('should rotate around Vector2 anchor point', (): void => {
  const VectorA = new Vector2(1, 28)
  const VectorB = new Vector2(6, 8)
  const RADIANS = 45

  expect(VectorA.rotateAround(RADIANS, VectorB)).toEqual(new Vector2(-13.644680434771018, 14.251922153684003))
})

test('should lerp Vector2', (): void => {
  const VectorA = new Vector2(1, 28)
  const VectorB = new Vector2(6, 8)

  expect(Vector2.lerp(VectorA, VectorB, 2)).toEqual(new Vector2(11, -12))
})

test('Vector2.zero', (): void => {
  expect(Vector2.zero()).toEqual(new Vector2(0, 0))
})

test('Vector2.one', (): void => {
  expect(Vector2.one()).toEqual(new Vector2(1, 1))
})

test('Vector2 zero', (): void => {
  expect(Vector2.zero()).toEqual(new Vector2(0, 0))
})

test('Vector2 up', (): void => {
  expect(Vector2.up()).toEqual(new Vector2(0, 1))
})

test('Vector2 down', (): void => {
  expect(Vector2.down()).toEqual(new Vector2(0, -1))
})

test('Vector2 left', (): void => {
  expect(Vector2.left()).toEqual(new Vector2(-1, 0))
})

test('Vector2 right', (): void => {
  expect(Vector2.right()).toEqual(new Vector2(1, 0))
})

test('Vector2 negativeInfinity', (): void => {
  expect(Vector2.negativeInfinity()).toEqual(new Vector2(Number.NEGATIVE_INFINITY, Number.NEGATIVE_INFINITY))
})

test('Vector2 positiveInfinity', (): void => {
  expect(Vector2.positiveInfinity()).toEqual(new Vector2(Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY))
})
