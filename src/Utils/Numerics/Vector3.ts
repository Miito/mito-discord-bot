/**
 * A Basic Vector 3D class
 * This structure is used in some places to represent 3D positions and vectors
 *
 * @class Vector3
 */
class Vector3 {
  public x: number

  public y: number

  public z: number

  public constructor (x: number = 0, y: number = 0, z: number = 0) {
    this.x = x
    this.y = y
    this.z = z
  }

  /**
   * Construct a Vector3 from an array
   *
   * @static
   * @param {number[]} array a array with 3 items to generate the vector from
   * @returns {Vector3} A vector containing the values from the array
   * @memberof Vector3
   */
  public static fromArray (array: number[]): Vector3 {
    return new Vector3(array[0], array[1], array[2])
  }

  /**
   *
   *
   * @static
   * @returns {Vector3} A normalized vector in the Y direction
   * @memberof Vector3
   */
  public static up (): Vector3 {
    return new Vector3(0, 1, 0)
  }

  /**
   *
   *
   * @static
   * @returns {Vector3} A normalized vector in the -Y direction
   * @memberof Vector3
   */
  public static down (): Vector3 {
    return new Vector3(0, -1, 0)
  }

  /**
   *
   *
   * @static
   * @returns {Vector3} A normalized vector in the -X direction
   * @memberof Vector3
   */
  public static left (): Vector3 {
    return new Vector3(-1, 0, 0)
  }

  /**
   *
   *
   * @static
   * @returns {Vector3} A normalized vector in the X direction
   * @memberof Vector3
   */
  public static right (): Vector3 {
    return new Vector3(1, 0, 0)
  }

  /**
   *
   *
   * @static
   * @returns {Vector3} A normalized vector in the Z direction
   * @memberof Vector3
   */
  public static forward (): Vector3 {
    return new Vector3(0, 0, 1)
  }

  /**
   *
   *
   * @static
   * @returns {Vector3} A normalized vector in the -Z direction

   * @memberof Vector3
   */
  public static backward (): Vector3 {
    return new Vector3(0, 0, -1)
  }

  /**
   *
   *
   * @static
   * @returns {Vector3} A 3D zero vector
   * @memberof Vector3
   */
  public static zero (): Vector3 {
    return new Vector3(0, 0, 0)
  }

  /**
   * Subtract a vector from this vector
   *
   * @param {Vector3} vec The vector to subtract from this vector
   * @returns {Vector3} A new vector containing the subtraction result
   * @memberof Vector3
   */
  public subtract (vec: Vector3): Vector3 {
    return new Vector3(this.x - vec.x, this.y - vec.y, this.z - vec.z)
  }

  /**
   * Add a vector to this vector
   *
   * @param {Vector3} vec The vector to add
   * @returns {Vector3} A new vector containing the addition result
   * @memberof Vector3
   */
  public add (vec: Vector3): Vector3 {
    return new Vector3(this.x + vec.x, this.y + vec.y, this.z + vec.z)
  }

  /**
   * Calculate the dot product of this and another vector
   *
   * @param {Vector3} vec The vector to dot with this vector
   * @returns {number} The dot product of this and provided vector
   * @memberof Vector3
   */
  public dot (vec: Vector3): number {
    return this.x * vec.x + this.y * vec.y + this.z * vec.z
  }

  /**
   * Scale this vector
   *
   * @param {number} scale The scalar factor
   * @returns {Vector3} A new, scaled vector
   * @memberof Vector3
   */
  public scale (scale: number): Vector3 {
    return new Vector3(this.x * scale, this.y * scale, this.z * scale)
  }

  /**
   * Calculate the cross product between this and another vector
   *
   * @param {Vector3} vec
   * @returns {Vector3} The other vector
   * @memberof Vector3 A new vector containing the cross product
   */
  public cross (vec: Vector3): Vector3 {
    return new Vector3(
      this.y * vec.z - this.z * vec.y,
      this.z * vec.x - this.x * vec.z,
      this.x * vec.y - this.y * vec.x
    )
  }

  /**
   * Determine if this and another vector are perpendicular
   *
   * @param {Vector3} vec The other vector
   * @returns {boolean} True if the two vectors are perpendicular
   * @memberof Vector3
   */
  public orthogonalTo (vec: Vector3): boolean {
    return this.dot(vec) === 0
  }

  /**
   * Determine equality between this and another vector
   *
   * @param {Vector3} vec The other Vector
   * @param {number} [Epsilon] Optional: a tolerance value for comparing non-integer values
   * @returns {boolean} True if the vectors are equal
   * @memberof Vector3
   */
  public equals (vec: Vector3, Epsilon?: number): boolean {
    if (typeof Epsilon === 'number') {
      Epsilon = Math.abs(Epsilon)
      const X: boolean = vec.x > this.x - Epsilon && vec.x < this.x + Epsilon
      const Y: boolean = vec.y > this.y - Epsilon && vec.y < this.y + Epsilon
      const Z: boolean = vec.z > this.z - Epsilon && vec.z < this.z + Epsilon
      return X && Y && Z
    }
    return this.x === vec.x && this.y === vec.y && this.z === vec.z
  }

  /**
   * Determine if this and another vector are parallel
   *
   * @param {Vector3} vec the other vector
   * @returns {boolean} True if the two vectors are parallel
   * @memberof Vector3
   */
  public parallelTo (vec: Vector3): boolean {
    return this.length() === 0 || vec.length() === 0 || new Vector3(0, 0, 0).equals(this.cross(vec))
  }

  /**
   * Calculate the norm (length) of the vector
   *
   * @returns {number} The norm of the vector
   * @memberof Vector3
   */
  public length (): number {
    if (this.equals(Vector3.zero())) {
      return 0
    }

    const Radian: number = this.x * this.x + this.y * this.y + this.z * this.z
    return Math.sqrt(Radian)
  }

  /**
   * Calculate the distance between two vectors
   *
   * @param {Vector3} vec The other vector
   * @returns {number} The distance between the two vectors
   * @memberof Vector3
   */
  public distance (vec: Vector3): number {
    return Math.sqrt(
      (this.x - vec.x) ** 2 + (this.y - vec.y) ** 2 + (this.z - vec.z) ** 2
    )
  }

  /**
   * Returns a nicely formatted string for this vector
   *
   * @returns {string} the vector as a string
   * @memberof Vector3
   */
  public ToString (): string {
    return `x: ${this.x}, y: ${this.y}, z: ${this.z}`
  }

  /**
   * Get vector as array
   *
   * @returns {number[]} the vector as a array
   * @memberof Vector3
   */
  public ToArray (): number[] {
    return [this.x, this.y, this.z]
  }
}

export default Vector3
