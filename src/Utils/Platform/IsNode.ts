/**
 *
 *
 * @returns {boolean}
 */
function IsNode (): boolean {
  return (
    typeof global !== 'undefined' &&
    typeof process !== 'undefined' &&
    typeof process.stdout !== 'undefined'
  )
}

export default IsNode
