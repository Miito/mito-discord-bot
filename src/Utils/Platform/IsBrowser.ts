/**
 * Determine if environment is om a browser
 *
 * @returns {boolean} true if the environment is in a browser
 */
function isBrowser (): boolean {
  return (
    typeof window === 'object' &&
    typeof document === 'object' &&
    document.nodeType === 9
  )
}

export default isBrowser
