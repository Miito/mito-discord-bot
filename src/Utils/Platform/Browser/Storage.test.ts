import Storage from './Storage'

test('Storage is a Function', (): void => {
  expect(Storage).toBeInstanceOf(Function)
})
