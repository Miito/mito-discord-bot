/**
 * localStorage helper
 *
 * @class Storage
 */
class Storage {
  public prefix: string

  /**
   *Creates an instance of Storage.
   * @param {string} [prefix='store']
   * @memberof Storage
   */
  public constructor (prefix: string = 'store') {
    this.prefix = prefix
  }

  public set (key: string, value: string): Storage {
    localStorage.setItem(`${this.prefix}${key}`, value)

    return this
  }

  public get (key: string): string | null | Storage {
    return localStorage.getItem(`${this.prefix}${key}`) && this
  }

  public remove (key: string): Storage {
    localStorage.removeItem(`${this.prefix}${key}`)

    return this
  }

  public clear (): Storage {
    localStorage.clear()

    return this
  }
}

export default Storage
