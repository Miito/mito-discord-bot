/**
 * Determine if environment is iOS
 *
 * @param {string} [userAgent=navigator.userAgent] the environment useragent
 * @returns {boolean} true if the environment is iOS
 */
function IsIOS (userAgent: string = navigator.userAgent): boolean {
  return /ipad|iphone|ipod/i.test(userAgent)
}

export default IsIOS
