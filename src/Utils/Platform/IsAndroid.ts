/**
 * Determine if environment is Android
 *
 * @param {string} [userAgent=navigator.userAgent] the environment useragent
 * @returns {boolean} true if the environment is Android
 */
function IsAndroid (userAgent: string = navigator.userAgent): boolean {
  return /android/i.test(userAgent) || userAgent.indexOf('Adr') > 1
}

export default IsAndroid
