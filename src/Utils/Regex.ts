export const youtubeVideoRegex: RegExp = /((https?):\/\/(www\.)?)?youtu\.?be(\.com)?\/(watch\?v=)?\w{11}/
export const discordInviteRegex: RegExp = /(discord|discordapp).(gg|me|io|com\/invite)\/(\S+)/g
export const telegramInviteRegex: RegExp = /(https?):\/\/t.me\/joinchat\/[ -z]+/
export const hexRegex: RegExp = /(#|0x)([\dA-Fa-f]{6})([\s!&()*+\-.=_]|$)/
export const urlRegex: RegExp = /https?:\/\/(www\.)?[\w#%+-.:=@~]{2,256}\.[a-z]{2,11}\b([\w#%&+-./:=?@~]*)/
export const emojiRegex: RegExp = /<:\w+:\d{17,21}>/
export const specialCharRegex: RegExp = /[!#$%&()*@[\]^_{}]/
export const color: RegExp = /^(#([\dA-Fa-f]{3}){1,2}|[Rr][Gg][Bb](\((\s*(2[0-4]\d|25[0-5]|[01]?\d{1,2})\s*,){2}\s*(2[0-4]\d|25[0-5]|[01]?\d{1,2})\s*\)|[Aa]\((\s*(2[0-4]\d|25[0-5]|[01]?\d{1,2})\s*,){3}\s*([01]|0\.\d+)\s*\)))$/
export const email: RegExp = /\w+([+-.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/
export const phoneNumber: RegExp = /^(\+?0?86?)?1[3-9]\d{9}$/
export const url: RegExp = /[\w#%-.:=@~]{2,256}\.[a-z]{2,6}\b([\w#%&-./:=?@~]*)/i
