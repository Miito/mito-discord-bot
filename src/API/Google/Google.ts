// import cheerio from 'cheerio'
// import axios, { AxiosResponse } from 'axios'

// class Google {
//   public UserAgent: string
//   protected SetCookie: any

//   public constructor ({ UserAgent }: { UserAgent: string }) {
//     const DefaultUserAgent = 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) Gecko/20100101 Firefox/53.0'
//     this.UserAgent = UserAgent || DefaultUserAgent
//     this.SetCookie = []
//   }

//   public async request (query: string, params: unknown): Promise<void> {
//     return axios.request({
//       method: 'GET',
//       url: 'https://www.google.com/search',
//       params: params,
//       validateStatus: (status: number): boolean => {
//         return status >= 200 && status < 303
//       }
//     }).then((response: AxiosResponse): void => {
//       this.SetCookie = response.headers['set-cookie']

//       return response.data
//     })
//   }

//   public async image (query: string, nsfw = false): Promise<void> {
//     return this.request(query, {
//       ie: 'ISO-8859-1',
//       hl: 'en',
//       source: 'hp',
//       tbm: 'isch',
//       gbv: '1',
//       gs_l: 'img',
//       q: query,
//       safe: nsfw ? 'disabled' : 'active'
//     }).then((body) => cheerio.load(body))
//       .then(function findInBody ($: unknown): unknown {
//         const element = $('td a img').first()
//         if (!element) return false
//         const source = element.attr('src')
//         if (!source) return false
//         if (source.startsWith('/')) {
//           $('td a img').first().remove()
//           return findInBody($)
//         }
//         return decodeURIComponent(source)
//       })
//       .catch(() => {
//         false
//       })
//   }

//   public async search (query: string, nsfw = false): Promise<void> {
//     return this.request(query, {
//       q: query,
//       safe: nsfw ? 'off' : 'on',
//       lr: 'lang_en',
//       hl: 'en'
//     })
//   }
// }

// export default Google
