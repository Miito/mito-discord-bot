enum Ruleset {
  Standard = 0,
  Taiko = 1,
  CTB = 2,
  Mania = 3
}

export default Ruleset
