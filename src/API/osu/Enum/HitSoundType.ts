enum HitSoundType {
  None = 0,
  Normal = 1,
  Whistle = 1 << 1,
  Finish = 1 << 2,
  Clap = 1 << 3
}

export default HitSoundType
