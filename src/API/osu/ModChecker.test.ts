import ModChecker from './ModChecker'

test('ModChecker is a function', (): void => {
  expect(ModChecker).toBeInstanceOf(Function)
})
it('return none', (): void => {
  expect(ModChecker(0)).toEqual('')
})

it('return HDDT', (): void => {
  expect(ModChecker(72)).toEqual('Hidden, DoubleTime')
})

it('return HDHR', (): void => {
  expect(ModChecker(24)).toEqual('Hidden, HardRock')
})

it('return ScoreIncreaseMods', (): void => {
  expect(ModChecker(1049688)).toEqual('Hidden, HardRock, DoubleTime, Flashlight, FadeIn')
})
