const Beatmaps = {
  approved: {
    '-2': 'Graveyard',
    '-1': 'WIP',
    0: 'Pending',
    1: 'Ranked',
    2: 'Approved',
    3: 'Qualified',
    4: 'Loved'
  },
  genre: {
    0: 'Any',
    1: 'Unspecified',
    2: 'Video Game',
    3: 'Anime',
    4: 'Rock',
    5: 'Pop',
    6: 'Other',
    7: 'Novelty',
    9: 'Hip Hop',
    10: 'Electronic'
  },
  language: {
    0: 'Any',
    1: 'Other',
    2: 'English',
    3: 'Japanese',
    4: 'Chinese',
    5: 'Instrumental',
    6: 'Korean',
    7: 'French',
    8: 'German',
    9: 'Swedish',
    10: 'Spanish',
    11: 'Italian'
  },
  mode: {
    0: 'Standard',
    1: 'Taiko',
    2: 'Catch the Beat',
    3: 'Mania'
  }
}

export default Beatmaps
