// import ojsama from 'ojsama'

// /**
//  *
//  *
//  * @param {$TsFixMe} osuFilePath
//  * @param {string} enableMod
//  * @returns {$TsFixMe}
//  */
// function CalculateStrains (osuFilePath: $TsFixMe, enableMod: string): $TsFixMe {
//   try {
//     const parser = new ojsama.parser().feed(fs.readFileSync(osu_file_path, 'utf8'))

//     const mods = ojsama.modbits.from_string(mods_string || '')
//     const mods_array = getMods(mods)

//     let speed_multiplier = 1

//     if (mods_array.includes('DT')) { speed_multiplier *= 1.5 }

//     if (mods_array.includes('HT')) { speed_multiplier *= 0.75 }

//     const map = parser.map

//     const mods_filtered = mods_array.filter(mod => TIME_MODS.includes(mod))

//     if (mods_filtered.length > 0) {
//       map.version += ' +' + mods_filtered.join('')
//     }

//     const bpms = []

//     for (let t = 0; t < map.timing_points.length; t++) {
//       const timing_point = map.timing_points[t]
//       if (!timing_point.change) { continue }

//       const bpm = +(MINUTE / timing_point.ms_per_beat * speed_multiplier).toFixed(2)

//       if (bpms.length > 0) {
//         if (bpms[bpms.length - 1] != bpm) {
//           bpms.push([timing_point.time, bpms[bpms.length - 1][1]])
//           bpms.push([timing_point.time, bpm])
//         }
//       } else {
//         bpms.push([timing_point.time, bpm])
//       }
//     }

//     if (bpms.length == 0) {
//       cb('An error occured getting the Beatmap BPM values')
//       return false
//     }

//     bpms.push([map.objects[map.objects.length - 1].time, bpms[bpms.length - 1][1]])

//     const highcharts_settings = {
//       type: 'png',
//       options: {
//         chart: {
//           type: 'spline'
//         },
//         title: {
//           text: `${map.artist} - ${map.title}`
//         },
//         subtitle: {
//           text: `Version: ${map.version}, Mapped by ${map.creator}`
//         },
//         yAxis: {
//           title: {
//             align: 'high',
//             text: 'BPM',
//             style: {
//               'text-anchor': 'start'
//             },
//             rotation: 0,
//             y: -20,
//             x: 10
//           }
//         },
//         xAxis: {
//           type: 'datetime',
//           dateTimeLabelFormats: {
//             month: '%M:%S',
//             year: '%M:%S',
//             day: '%M:%S',
//             minute: '%M:%S',
//             second: '%M:%S',
//             millisecond: '%M:%S'
//           }
//         },
//         series: [{
//           showInLegend: false,
//           data: bpms
//         }]
//       },
//       themeOptions: CHART_THEME
//     }

//     highcharts.export(highcharts_settings, (error, res) => {
//       if (error) cb('An error occured creating the graph')
//       else cb(null, res.data)
//     })
//   } catch (error) {
//     cb('An error occured creating the graph')
//     helper.error(error)
//   }
// }
