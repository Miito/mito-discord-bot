// import { createCanvas } from 'canvas'

// /**
//  *
//  */
// async function GetStrainsBar (OsuBeatmapPath: string | Buffer, Mods: string, progress: number): Promise<$TsFixMe> {
//   // let { strains, max_strain } = module.exports.get_strains(osu_file_path, mods_string)
//   const { strains, max_strain: maxStrain } = ''

//   if (strains) {
//     const canvas = createCanvas(399, 40)
//     const context = canvas.getContext('2d')

//     context.fillStyle = 'transparent'
//     context.fillRect(0, 0, 399, 40)

//     const points: any[] = []
//     const strainChunks: number[] = []

//     const maxChunks = 100
//     const chunkSize = Math.ceil(strains.length / maxChunks)

//     for (let i = 0; i < strains.length; i += chunkSize) {
//       const _strains = strains.slice(i, i + chunkSize)
//       strainChunks.push(Math.max(..._strains))
//     }

//     strainChunks.forEach((strain, index): void => {
//       const _strain = strain / maxStrain
//       const x = index / strainChunks.length * 399
//       const y = Math.min(30, 5 + 35 - _strain * 35)
//       points.push({ x, y })
//     })

//     context.fillStyle = '#F06292'
//     context.moveTo(0, 40)
//     context.lineTo(0, 30)

//     for (let i = 1; i < points.length - 2; i++) {
//       const xc = (Number(points[i].x) + Number(points[i + 1].x)) / 2
//       const yc = (Number(points[i].y) + Number(points[i + 1].y)) / 2
//       context.quadraticCurveTo(points[i].x, points[i].y, xc, yc)
//     }

//     context.lineTo(399, 30)
//     context.lineTo(399, 40)
//     context.closePath()
//     context.fill()

//     context.clearRect(progress * 399, 0, 399 - progress * 399, 40)

//     context.fillStyle = 'transparent'
//     context.fillRect(progress * 399, 0, 399 - progress * 399, 40)

//     context.fillStyle = 'rgba(244, 143, 177, 0.5)'
//     context.moveTo(0, 40)
//     context.lineTo(0, 30)

//     for (let i = 1; i < points.length - 2; i++) {
//       const xc = (Number(points[i].x) + Number(points[i + 1].x)) / 2
//       const yc = (Number(points[i].y) + Number(points[i + 1].y)) / 2
//       context.quadraticCurveTo(points[i].x, points[i].y, xc, yc)
//     }

//     context.lineTo(399, 30)
//     context.lineTo(399, 40)
//     context.closePath()
//     context.fill()

//     return canvas.toBuffer()
//   } else {
//     return false
//   }
// }

// export default GetStrainsBar
