import BinomialCoefficient from '../../../../../Utils/Math/BinomialCoefficient'

/**
 *
 */
function Bezier (point: any[], t: number): number[] {
  let bx = 0
  let by = 0
  const n = point.length - 1

  switch (n) {
    case 1:
      bx = (1 - t) * point[0][0] + t * point[1][0]
      by = (1 - t) * point[0][1] + t * point[1][1]
      break
    case 2:
      bx = (1 - t) * (1 - t) * point[0][0] + 2 * (1 - t) * t * point[1][0] + t * t * point[2][0]
      by = (1 - t) * (1 - t) * point[0][1] + 2 * (1 - t) * t * point[1][1] + t * t * point[2][1]
      break
    case 3:
      bx = (1 - t) * (1 - t) * (1 - t) * point[0][0] + 3 * (1 - t) * (1 - t) * t * point[1][0] + 3 * (1 - t) * t * t * point[2][0] + t * t * t * point[3][0]
      by = (1 - t) * (1 - t) * (1 - t) * point[0][1] + 3 * (1 - t) * (1 - t) * t * point[1][1] + 3 * (1 - t) * t * t * point[2][1] + t * t * t * point[3][1]
      break
    default:
      for (let i = 0; i <= n; i++) {
        bx += BinomialCoefficient(n, i) * (1 - t) ** (n - i) * t ** i * point[i][0]
        by += BinomialCoefficient(n, i) * (1 - t) ** (n - i) * t ** i * point[i][1]
      }
      break
  }

  return [bx, by]
}

export default Bezier
