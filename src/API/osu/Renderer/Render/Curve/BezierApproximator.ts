import Vector2 from '../../../../../Utils/Numerics/Vector2'

const TOLERANCE: number = 0.25

class BezierApproximator {
  public subdivisionBuffer1: Vector2[] = []

  public subdivisionBuffer2: Vector2[] = []

  public constructor (public control: Vector2[]) {}

  private static IsFlatEnough (controlPoints: Vector2[]): boolean {
    for (let i = 0; i < controlPoints.length - 1; i++) {
      if ((controlPoints[i - 1].subtract(controlPoints[i].smul(2)).add(controlPoints[i + 1])).m2 > TOLERANCE) {
        return false
      }
    }

    return true
  }

  public subdivide (controlPoints: Vector2[], left: Vector2[], right: Vector2[]): void {
    const midpoints = this.subdivisionBuffer1

    for (let i = 0; i < this.control.length; i++) {
      midpoints[i] = controlPoints[i]
    }

    for (let i = 0; i < this.control.length; i++) {
      left[i] = midpoints[0]
      right[this.control.length - i - 1] = midpoints[this.control.length - i - 1]
      for (let j = 0; j < this.control.length; j++) {
        midpoints[j] = midpoints[j].add(midpoints[j + 1]).smul(0.5)
      }
    }
  }

  public approximate (controlPoints: Vector2[], output: Vector2[]): void {
    const left = this.subdivisionBuffer2
    const right = this.subdivisionBuffer1
    this.subdivide(controlPoints, left, right)

    // add right to left
    for (let i = 0; i < this.control.length; i++) {
      left[this.control.length + i] = right[i + 1]
    }

    output.push(controlPoints[0])

    for (let i = 0; i < this.control.length; i++) {
      const index = 2 * i
      const p = left[index - 1].add(left[index].smul(2)).add(left[index + 1]).smul(0.25)
      output.push(p)
    }
  }
}

export default BezierApproximator
