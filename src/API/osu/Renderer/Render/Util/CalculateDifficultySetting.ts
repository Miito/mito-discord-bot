import { parseBitFlags } from '../../../../../Utils/DataStructure/BitFlag'
import Mods from '../../../Enum/Mods'

const AR_MS_STEP1 = 120
const AR_MS_STEP2 = 150
const AR0_MS = 1800
const AR5_MS = 1200
const AR10_MS = 450
const OD_MS_STEP = 6
const OD0_MS = 79.5
const OD10_MS = 19.5

/**
 *
 */
function calculateSpeed (EnabledMod: number): number {
  let speed = 1

  if (parseBitFlags(EnabledMod, Mods) === 'DoubleTime') {
    speed *= 1.5
  }

  if (parseBitFlags(EnabledMod, Mods) === 'HalfTime') {
    speed *= 0.75
  }

  if (parseBitFlags(EnabledMod, Mods) === 'HardRock') {
    speed *= 1.4
  }

  if (parseBitFlags(EnabledMod, Mods) === 'Easy') {
    speed *= 0.5
  }

  return speed
}

/**
 *
 */
function calculateApproachRate (RawAR: number, EnabledMod: number): number {
  let approachRate: number
  let AR_MS: number
  let approachRateMultiplier = 1

  if (parseBitFlags(EnabledMod, Mods) === 'HardRock') {
    approachRateMultiplier *= 1.4
  }

  if (parseBitFlags(EnabledMod, Mods) === 'Easy') {
    approachRateMultiplier *= 0.5
  }

  approachRate = RawAR * approachRateMultiplier

  if (approachRate <= 5) {
    AR_MS = AR0_MS - AR_MS_STEP1 * approachRate
  } else {
    AR_MS = AR5_MS - AR_MS_STEP2 * (approachRate - 5)
  }

  if (AR_MS < AR10_MS) { AR_MS = AR10_MS }
  if (AR_MS > AR0_MS) { AR_MS = AR0_MS }

  AR_MS /= calculateSpeed(EnabledMod)

  if (approachRate <= 5) {
    approachRate = (AR0_MS - AR_MS) / AR_MS_STEP1
  } else {
    approachRate = 5 + (AR5_MS - AR_MS) / AR_MS_STEP2
  }

  return approachRate
}

/**
 *
 */
function calculateCircleSize (RawCS: number, EnabledMod: number): number {
  let circleSize: number
  let circleSizeMultiplier = 1

  if (parseBitFlags(EnabledMod, Mods) === 'HardRock') {
    circleSizeMultiplier *= 1.3
  }

  if (parseBitFlags(EnabledMod, Mods) === 'Easy') {
    circleSizeMultiplier *= 0.5
  }

  circleSize = RawCS * circleSizeMultiplier

  if (circleSize > 10) {
    circleSize = 10
  }

  return circleSize
}

/**
 *
 */
function calculateOverallDifficulty (RawOD: number, EnabledMod: number): number {
  let overallDifficulty: number
  let OVERALLDIFFICULTY_MS: number
  let overallDifficultyMultiplier = 1

  if (parseBitFlags(EnabledMod, Mods) === 'HardRock') {
    overallDifficultyMultiplier *= 1.4
  }

  if (parseBitFlags(EnabledMod, Mods) === 'Easy') {
    overallDifficultyMultiplier *= 0.5
  }

  overallDifficulty = RawOD * overallDifficultyMultiplier
  OVERALLDIFFICULTY_MS = OD0_MS - Math.ceil(OD_MS_STEP * overallDifficulty)
  OVERALLDIFFICULTY_MS = Math.min(OD0_MS, Math.max(OD10_MS, OVERALLDIFFICULTY_MS))

  overallDifficulty /= calculateSpeed(EnabledMod)

  overallDifficulty = (OD0_MS - OVERALLDIFFICULTY_MS) / OD_MS_STEP

  return overallDifficulty
}

export { calculateApproachRate, calculateCircleSize, calculateOverallDifficulty, calculateSpeed }
