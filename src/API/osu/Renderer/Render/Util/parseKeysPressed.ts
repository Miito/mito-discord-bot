enum KeyEnum {
  M1 = 1,
  M2 = 1 << 1,
  K1 = 1 << 2,
  K2 = 1 << 3
}

/**
 *
 */
function parseKeysPressed (number: number): KeyEnum {
  const outputKeys = {
    K1: false,
    K2: false,
    M1: false,
    M2: false
  }

  for (const key in KeyEnum) {
    outputKeys[key] = false
    if (KeyEnum[key] & key) {
      outputKeys[key] = true
    }
  }

  if (outputKeys.K1 && outputKeys.M1) {
    outputKeys.M1 = false
  }

  if (outputKeys.K2 && outputKeys.M2) {
    outputKeys.M2 = false
  }

  return outputKeys
}

export default parseKeysPressed
