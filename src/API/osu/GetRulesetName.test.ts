import GetRulesetName from './GetRulesetName'
import UnreachableCaseError from '../../Utils/Error/UnreachableCaseError'

test('GetRulesetName is a Function', (): void => {
  expect(GetRulesetName).toBeInstanceOf(Function)
})

it('calls GetRulesetName and return ruleset name', (): void => {
  expect(GetRulesetName(0)).toBe('osu!std')
  expect(GetRulesetName(1)).toBe('osu!taiko')
  expect(GetRulesetName(2)).toBe('osu!catch')
  expect(GetRulesetName(3)).toBe('osu!mania')
})

it('will fail when a out of range value is given', (): void => {
  expect((): void => { GetRulesetName(4) }).toThrowError(UnreachableCaseError)
})
