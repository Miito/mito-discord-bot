import Nodesu from 'nodesu'
import { parseBitFlags } from '../../Utils/DataStructure'

/**
 *
 */
function ModChecker (value: number): string {
  if (value === 0) {
    return ''
  } else {
    return parseBitFlags(value, Nodesu.Mods).join(', ')
  }
}

export default ModChecker
