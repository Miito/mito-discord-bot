const Multiplayer = {
  scoringType: {
    0: 'Score',
    1: 'Accuracy',
    2: 'Combo',
    3: 'Score v2'
  },
  teamType: {
    0: 'Head to Head',
    1: 'Tag Co-op',
    2: 'Team vs',
    3: 'Tag Team vs'
  },
  team: {
    0: 'None',
    1: 'Blue',
    2: 'Red'
  }
}

export default Multiplayer
