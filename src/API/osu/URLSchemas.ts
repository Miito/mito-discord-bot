const URLSchemas = {
  multiplayerMatch: (id: number, password: string): string => `osu://mp/${id}${password !== undefined ? '/' + password : ''}`,
  edit: (position: string, objects: string): string => `osu://edit/${position}${objects !== undefined ? ' ' + objects : ''}`,
  channel: (name: string): string => `osu://chan/#${name}`,
  download: (id: number): string => `osu://dl/${id}`,
  spectate: (user: number | string): string => `osu://spectate/${user}`
}

export default URLSchemas
