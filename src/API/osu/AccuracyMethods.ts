interface Count {
  [key: string]: number
}

const AccuracyMethods = {
  Standard: (count: Count): number => {
    const totalHits = count['50'] + count['100'] + count['300'] + count.miss
    // @ts-ignore
    return totalHits === 0 ? 0 : ((count['300'] * 300 + count['100'] * 100 + count['50'] * 50) / (totalHits * 300) * 100).toPrecision(4)
  },
  Taiko: (count: Count): number => {
    const total = count['100'] + count['300'] + count.miss
    // @ts-ignore
    return total === 0 ? 0 : ((count['300'] + (0.5 * count[100])) / total * 100).toPrecision(4)
  },
  CatchTheBeat: (count: Count): number => {
    const total = count['50'] + count['100'] + count['300'] + count.katu + count.miss
    // @ts-ignore
    return total === 0 ? 0 : ((count['50'] + count['100'] + count['300']) / total * 100).toPrecision(4)
  },
  Mania: (count: Count): number => {
    const total = count['50'] + count['100'] + count['300'] + count.katu + count.geki + count.miss
    // @ts-ignore
    return total === 0 ? 0 : (((count['50'] * 50) + (count['100'] * 100) + (count.katu * 200) + ((count['300'] + count.geki) * 300)) / (total * 300) * 100).toPrecision(4)
  }
}

export default AccuracyMethods
