import fs from 'fs'

/**
 *
 *
 * @param {string} beatmapPath
 * @returns {boolean}
 */
function ValidateBeatmap (beatmapPath: string): boolean {
  const file = fs.readFileSync(beatmapPath, 'utf8')
  const line = file.split('\n')

  if (line.length > 0 && !line[0].startsWith('osu file format')) {
    return false
  }

  return true
}

export default ValidateBeatmap
