import UnreachableCaseError from '../../Utils/Error/UnreachableCaseError'

/**
 * Get ruleset name from number values
 *
 * @param {number} ruleset
 * @returns {string}
 */
function GetRulesetName (ruleset: number): string {
  switch (ruleset) {
    case 0:
      return 'osu!std'
    case 1:
      return 'osu!taiko'
    case 2:
      return 'osu!catch'
    case 3:
      return 'osu!mania'
    default:
      throw new UnreachableCaseError(`GetRulesetName receive out of range value: ${ruleset}`)
  }
}

export default GetRulesetName
