import AccuracyMethods from './AccuracyMethods'

test('AccuracyMethods.Standard is a Function', (): void => {
  expect(AccuracyMethods.Standard).toBeInstanceOf(Function)
})

test('AccuracyMethods.CatchTheBeat is a Function', (): void => {
  expect(AccuracyMethods.CatchTheBeat).toBeInstanceOf(Function)
})

test('AccuracyMethods.Mania is a Function', (): void => {
  expect(AccuracyMethods.Mania).toBeInstanceOf(Function)
})

test('AccuracyMethods.Taiko is a Function', (): void => {
  expect(AccuracyMethods.Taiko).toBeInstanceOf(Function)
})

it('calls AccuracyMethods.Standard and return Standard calculated accuracy value', (): void => {
  const RankSData = {
    50: 0,
    100: 2,
    300: 379,
    miss: 0
  }

  const RankAData = {
    50: 0,
    100: 4,
    300: 717,
    miss: 11
  }

  const RankBData = {
    50: 11,
    100: 73,
    300: 530,
    miss: 10
  }

  const RankCData = {
    50: 16,
    100: 120,
    300: 450,
    miss: 85
  }

  const RankDData = {
    50: 29,
    100: 104,
    300: 215,
    miss: 73
  }

  expect(AccuracyMethods.Standard(RankSData)).toBe('99.65')
  expect(AccuracyMethods.Standard(RankAData)).toBe('98.13')
  expect(AccuracyMethods.Standard(RankBData)).toBe('89.13')
  expect(AccuracyMethods.Standard(RankCData)).toBe('73.42')
  expect(AccuracyMethods.Standard(RankDData)).toBe('60.45')
})

it('calls AccuracyMethods.Taiko and return Taiko calculated accuracy value', (): void => {
  const RankSData = {
    100: 171,
    300: 2478,
    miss: 0
  }

  const RankAData = {
    100: 23,
    300: 922,
    miss: 23
  }

  const RankBData = {
    100: 74,
    300: 868,
    miss: 33
  }

  const RankCData = {
    100: 112,
    300: 753,
    miss: 100
  }

  const RankDData = {
    100: 381,
    300: 951,
    miss: 772
  }

  expect(AccuracyMethods.Taiko(RankSData)).toBe('96.77')
  expect(AccuracyMethods.Taiko(RankAData)).toBe('96.44')
  expect(AccuracyMethods.Taiko(RankBData)).toBe('92.82')
  expect(AccuracyMethods.Taiko(RankCData)).toBe('83.83')
  expect(AccuracyMethods.Taiko(RankDData)).toBe('54.25')
})

// it('calls AccuracyMethods.CatchTheBeat and return Catch The Beat calculated accuracy value', (): void => {
//   const RankSData = {
//     '300': 6909,
//     '100': 260,
//     '50': 757,
//     'katu': 2,
//     'miss': 10
//   }

//   const RankAData = {
//     '300': 2448,
//     '100': 119,
//     '50': 108,
//     'katu': 2,
//     'miss': 52
//   }

//   const RankBData = {
//     '300': 2333,
//     '100': 115,
//     '50': 108,
//     'katu': 2,
//     'miss': 171
//   }

//   const RankCData = {
//     '300': 2225,
//     '100': 122,
//     '50': 106,
//     'katu': 2,
//     'miss': 272
//   }

//   const RankDData = {
//     '300': 1444,
//     '100': 33,
//     '50': 300,
//     'katu': 2,
//     'miss': 628
//   }

//   expect(AccuracyMethods.CatchTheBeat(RankSData)).toBe('99.85')
//   expect(AccuracyMethods.CatchTheBeat(RankAData)).toBe('98.02')
//   expect(AccuracyMethods.CatchTheBeat(RankBData)).toBe('99.66')
//   expect(AccuracyMethods.CatchTheBeat(RankCData)).toBe('83.90')
//   expect(AccuracyMethods.CatchTheBeat(RankDData)).toBe('83.90')
// })

// it('calls AccuracyMethods.Mania and return Mania calculated accuracy value', (): void => {
//   const RankSData = {
//     geki: 2547,
//     '300': 425,
//     katu: 22,
//     '100': 0,
//     '50': 2,
//     miss: 1
//   }

//   const RankAData = {
//     geki: 3435,
//     '300': 2565,
//     katu: 736,
//     '100': 93,
//     '50': 14,
//     miss: 53
//   }

//   const RankBData = {
//     geki: 259,
//     '300': 282,
//     katu: 162,
//     '100': 39,
//     '50': 8,
//     miss: 28
//   }

//   const RankCData = {
//     geki: 113,
//     '300': 153,
//     katu: 143,
//     '100': 53,
//     '50': 14,
//     miss: 65
//   }

//   const RankDData = {
//     geki: 93,
//     '300': 146,
//     katu: 137,
//     '100': 71,
//     '50': 18,
//     miss: 76
//   }

//   expect(AccuracyMethods.CatchTheBeat(RankSData)).toBe('94.89')
//   expect(AccuracyMethods.CatchTheBeat(RankAData)).toBe('77.20')
//   expect(AccuracyMethods.CatchTheBeat(RankBData)).toBe('87.46')
//   expect(AccuracyMethods.CatchTheBeat(RankCData)).toBe('83.90')
//   expect(AccuracyMethods.CatchTheBeat(RankDData)).toBe('83.90')
// })
