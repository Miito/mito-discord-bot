export interface Anime {
  title: {
    userPreferred: string
  }
  siteUrl: string
  description: string
  format: string
  status: string
  meanScore: number
  rankings: Array<{
    rank: number
    allTime: number
  }>
  genres: string[]
  coverImage: {
    large: string
  }
  nextAiringEpisode: {
    timeUntilAiring: number
    episode: number
  }
}

export interface AnimeList {
  name: string
  avatar: {
    large: string
  }
  siteUrl: string
  options: {
    profileColor: string
  }
  stats: {
    watchedTime: number
    animeListScores: {
      meanScore: number
    }
    animeStatusDistribution: Array<{
      status: string
      amount: number
    }>
  }
}

export interface Manga {
  title: {
    userPreferred: string
  }
  siteUrl: string
  description: string
  format: string
  status: string
  meanScore: number
  rankings: Array<{
    rank: number
    allTime: boolean
  }>
  genres: string[]
  coverImage: {
    large: string
  }
}

export interface MangaList {
  name: string
  avatar: {
    large: string
  }
  siteUrl: string
  options: {
    profileColor: string
  }
  stats: {
    chaptersRead: number
    mangaListScores: {
      meanScore: number
    }
    mangaStatusDistribution: Array<{
      status: string
      amount: number
    }>
  }
}

export interface AnimeMusicInfo {
  title: {
    userPreferred: string
  }
  siteUrl: string
  idMal: number
  startDate: {
    year: number
  }
  coverImage: {
    large: string
  }
}
