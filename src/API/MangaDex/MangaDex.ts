// import { CloudscraperOptions } from './interface/cloudscraper-options'
// import { RequestHelper } from './RequestHelper'
// import { merge } from 'lodash'

// export interface MangadexApiOptions {
//   baseURL?: string
//   requestOptions?: CloudscraperOptions
// }

// const DEFAULT_OPTIONS: MangadexApiOptions = {
//   baseURL: 'https://mangadex.org',
//   requestOptions: {}
// }

// export class MangaDex {
//   private options: MangadexApiOptions

//   public constructor (options: MangadexApiOptions = {}) {
//     this.options = merge({}, DEFAULT_OPTIONS, options)
//   }

//   public async GetManga (mangaID: number): Promise<unknown> {
//     const url = `${this.options.baseURL}/manga/${mangaID}`
//     const response = await RequestHelper.getJson(url, this.options.requestOptions)
//     return response
//   }

//   public async getChapter (chapterID: number): Promise<unknown> {
//     const url = `${this.options.baseURL}/chapter/${chapterID}`
//     const response = await RequestHelper.getJson(url, this.options.requestOptions)
//     return response
//   }
// }
// export default MangaDex
