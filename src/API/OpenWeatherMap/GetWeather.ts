import axios, { AxiosResponse } from 'axios'

/**
 *
 *
 * @param {string} city City code
 * @returns {Promise<AxiosResponse>} weather data
 */
async function GetWeather (city: string): Promise<AxiosResponse> {
  return axios({
    method: 'GET',
    baseURL: 'http://api.openweathermap.org/data/2.5/weather',
    params: {
      q: encodeURIComponent(city),
      appid: 'e327affe1a337518322bfb99bb7323ef',
      units: 'metric'
    }
  })
}

export default GetWeather
