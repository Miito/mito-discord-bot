import './App.scss'
import './scss/main.scss'
import 'react-grid-layout/css/styles.css'
import 'react-resizable/css/styles.css'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { I18nextProvider } from 'react-i18next'
import i18n from './i18n'
import Modal from './components/Modal/Modal'
import PrivateRoute, { Home, Contact, Support, Status, Test, Commands, Login, Profile, NotFoundPage, Dashboard, Setting } from './Route'
import React, { useState } from 'react'

const ThemeContext = React.createContext('dark')

const App: React.FC = () => {
  const [toggle, setToggle] = useState(false)

  return (
    <React.StrictMode>
      <ThemeContext.Provider value='dark'>
        <I18nextProvider i18n={ i18n }>
          <div className='mito'>
            <BrowserRouter>
              <Switch>
                <Route path='/' exact component={ Home }/>
                <Route path='/contact/' component={ Contact }/>
                <Route path='/support/' component={ Support }/>
                <Route path='/status/' component={ Status }/>
                <Route path='/support/' component={ Support }/>
                <Route path='/test/' component={ Test }/>
                <Route path='/commands/:id' component={ Commands } />
                <Route path='/login/' component={ Login } />
                <Route path='/dashboard/' component={ Dashboard } />
                <PrivateRoute path='/profile/' component={ Profile } />
                <PrivateRoute path='/setting/' component={ Setting } />
                <Route path='/' component={ NotFoundPage }/>
              </Switch>
              <Modal toggle={ () => setToggle(!toggle) } open={ toggle }>WIP</Modal>
            </BrowserRouter>
          </div>
        </I18nextProvider>
      </ThemeContext.Provider>
    </React.StrictMode>
  )
}

export default App
