import React from 'react'
import NavBar from '../components/Nav/NavBar/NavBar'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faDiscord } from '@fortawesome/free-brands-svg-icons'
import LoginButton from '../components/LoginButton/LoginButton'
import Logo from '../components/Logo/Logo'
import './Login.scss'
import Footer from '../components/Footer/Footer'

const Login: React.FC = () => {
  return (
    <>
      <div className='relative z-10 container mx-auto p-0'>
        <NavBar />
      </div>
      <div className='login layout-container container mx-auto p-0'>
        <div className='layout'>
          <header className='login__header'>
            <h1 className='login__h1' >Login</h1>
          </header>
          <div className='login-card'>
            <div className='login-card__background'>
              <Logo className='login-card__logo'/>
            </div>
            <div className='login-page'>
              <LoginButton>
                <FontAwesomeIcon icon={ faDiscord } size='2x' />
                <span className='login-button__text' >Authorize with Discord</span>
              </LoginButton>
            </div>
          </div>
          <br />
        </div>
      </div>
      <Footer />
    </>
  )
}

export default Login
