import React from 'react'
import NavBar from '../components/Nav/NavBar/NavBar'
import Footer from '../components/Footer/Footer'
import BaseCard from '../components/Base/BaseCard/BaseCard'
import './Profile.scss'
import ProfileTabs from '../components/ProfileTabs/ProfileTabs'

const Profile: React.FC = () => {
  return (
    <>
      <div className='relative z-10 container mx-auto p-0'>
        <NavBar />
      </div>
      <div className='account layout-container container mx-auto p-0'>
        <div className='account-header'>
          <div className='account__outer'>
            <div className='container'>
              <div className='account-header__top'>
                <div className='account-info'>
                  <img src='https://source.unsplash.com/random/100x100' className='rounded-full' alt='User Avatar'/>
                  <h1 className='account-info__name'>
                    Username
                  </h1>
                </div>
                <div className='account-stats'>
                  <div className='account-stats__col'>
                    <div className='account-stats__header'>
                      <h1>Global Rank</h1>
                    </div>
                    <h1 className='account-stats__items'>64</h1>
                  </div>
                  <div className='account-stats__col'>
                    <div className='account-stats__header'>
                      <h1>Cards</h1>
                    </div>
                    <h1 className='account-stats__items'>128</h1>
                  </div>
                </div>
              </div>
              <div className='account-content'>
                <ProfileTabs>

                </ProfileTabs>
                <div>
                  test
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  )
}

export default Profile
