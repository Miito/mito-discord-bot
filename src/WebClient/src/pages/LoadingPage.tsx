import React from 'react'
import LoadingWidget from '../components/LoadingWidget/LoadingWidget';

const LoadingPage: React.FC = (props: any) => {
  if (props.error) {
    return <Error retry={props}/>
  } else if (props.timedOut) {
    return <div>Taking a long time... <button onClick={ props.retry }>Retry</button></div>;
  } else if (props.pastDelay) {
    return <Spinner />
  } else {
    return null;
  }
}

interface ErrorProps {
  retry: any
}

const Error: React.FC<ErrorProps> = (props) => {
  const {
    retry
  } = props
  return (
    <div className='flex m-5 flex-col text-white h-screen' >
      <div className='text-6xl'>Offline</div>
      <button onClick={ retry }>Retry</button>
    </div>
  )
}

const Spinner: React.FC = () => {
  return (
  <div className='flex justify-center items-center text-white h-screen' >
    <LoadingWidget />
  </div>
  )
}

export default LoadingPage
