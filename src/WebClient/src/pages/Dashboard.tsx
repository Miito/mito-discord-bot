import './Dashboard.scss'
import { BrowserRouter } from 'react-router-dom'
import { Switch, Route } from 'react-router'
import DashboardAside from '../components/Dashboard/DashboardAside/DashboardAside'
import DashboardNavBar from '../components/Dashboard/DashboardNavBar/DashboardNavBar'
import DashboardNavBarHeader from '../components/Dashboard/DashboardNavBarHeader/DashboardNavBarHeader'
import React from 'react'
import { DashboardStats, DashboardAdmin, DashboardFlow, DashboardSettings } from '../Route'
import DashboardViewButton from '../components/Dashboard/DashboardViewButton/DashboardViewButton'

const Dashboard: React.FC = () => {
  return (
    <>
      <div className='dashboard'>
        <BrowserRouter>
          <DashboardAside />
          <div className='dashboard-content'>
            <DashboardNavBar header={ <DashboardNavBarHeader title='Dashboard'/> } content={ <DashboardViewButton /> } />
            <div className='dashboard__container'>
              <Switch>
                <Route path='/dashboard' exact component={ DashboardStats }/>
                <Route path='/dashboard/stats' exact component={ DashboardStats }/>
                <Route path='/dashboard/admin' exact component={ DashboardAdmin }/>
                <Route path='/dashboard/flow' exact component={ DashboardFlow }/>
                <Route path='/dashboard/settings' exact component={ DashboardSettings }/>
              </Switch>
            </div>
          </div>
        </BrowserRouter>
      </div>
    </>
  )
}

export default Dashboard
