import React from 'react'
import NavBar from '../components/Nav/NavBar/NavBar'
import Footer from '../components/Footer/Footer'


const NotFound: React.FC = () => {
  return (
    <>
      <div className='relative z-10 container mx-auto p-0'>
        <NavBar />
      </div>
      <div className='notfound layout-container container mx-auto p-0 min-h-screen'>
        <div className='layout'>
          404 Page
          <br />
        </div>
      </div>
      <Footer />
    </>
  )
}

export default NotFound
