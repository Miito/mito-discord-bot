import React from 'react'
import NavBar from '../components/Nav/NavBar/NavBar'
import Footer from '../components/Footer/Footer'

const Commands: React.FC = () => {
  return (
    <>
      <div className='relative z-10 container mx-auto p-0'>
        <NavBar />
      </div>
      <div className='commands layout-container container mx-auto p-0'>
        <div className='layout'>
          Commands Page
        </div>
      </div>
      <Footer />
    </>
  )
}

export default Commands
