import React from 'react'
import NavBar from '../components/Nav/NavBar/NavBar'
import Footer from '../components/Footer/Footer'

const Test: React.FC = () => {
  return (
    <>
      <div className='relative z-10 layout-container container mx-auto p-0'>
        <NavBar />
      </div>
      <div className='div'>test</div>
      <Footer />
    </>
  )
}

export default Test
