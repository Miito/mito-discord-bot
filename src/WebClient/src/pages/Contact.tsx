import React from 'react'
import NavBar from '../components/Nav/NavBar/NavBar'
import Footer from '../components/Footer/Footer'

const Contact: React.FC = () => {
  return (
    <>
      <div className='relative z-10 container mx-auto p-0'>
        <NavBar />
      </div>
      <div className='contact layout-container container mx-auto p-0'>
        <div className='layout'>
          <h1>Contact Page</h1>
        </div>
      </div>
      <Footer />
    </>
  )
}

export default Contact
