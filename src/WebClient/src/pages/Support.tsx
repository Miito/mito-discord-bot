import React from 'react'
import NavBar from '../components/Nav/NavBar/NavBar'
import Footer from '../components/Footer/Footer'

const Support: React.FC = () => {
  return (
    <>
      <div className='relative z-10 container mx-auto p-0'>
        <NavBar />
      </div>
      <div className='support layout-container container mx-auto p-0'>
        <div className='layout'>
          <h1>Support Page</h1>
        </div>
      </div>
      <Footer />
    </>
  )
}

export default Support
