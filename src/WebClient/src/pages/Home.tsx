import React from 'react'
import Hero from '../components/Hero/Hero'
import './Home.scss'
import NavBar from '../components/Nav/NavBar/NavBar'
import Footer from '../components/Footer/Footer'

const Home: React.FC = () => {
  return (
    <>
      <div className='relative z-10 container mx-auto p-0'>
        <NavBar />
      </div>
      <div className='home layout-container p-0'>
        <div className='home__container'>
          <Hero className='container mx-auto' >
            <h1>Home Page</h1>
            <div className="hero__image">
              Text Header
            </div>
          </Hero>
        </div>
      </div>
      <Footer />
    </>
  )
}

export default Home
