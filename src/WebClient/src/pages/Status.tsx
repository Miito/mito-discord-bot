import { faMinus, faPlus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { GraphContext } from '../models/GraphType'
import { observer } from 'mobx-react-lite'
import { ServerStatusContext } from '../models/ServerStatus'
import CollapsibleSection from '../components/Collapsible/CollapsibleSection/CollapsibleSection'
import GraphDate from '../components/Graph/GraphDate/GraphDate'
import GraphSwitcher from '../components/Graph/GraphSwitcher/GraphSwitcher'
import GraphTimeline from '../components/Graph/GraphTimeline/GraphTimeline'
import React, { useContext } from 'react'
import RecentUptime from '../components/RecentUptime/RecentUptime'
import StaticAlert from '../components/StaticAlert/StaticAlert'
import Stats from '../components/Stats/Stats'
import Timeline from '../components/Incident/IncidentsTimeline/IncidentsTimeline'
import NavBar from '../components/Nav/NavBar/NavBar'
import Footer from '../components/Footer/Footer'

const Status: React.FC = observer(() => {
  const ServerStatusModels = useContext(ServerStatusContext)
  const GraphModel = useContext(GraphContext)
  const GraphElement = GraphModel.graphType ? <GraphTimeline /> : <GraphDate />

  return (
    <>
      <div className='relative z-10 container mx-auto p-0'>
        <NavBar />
      </div>
      <div className='status layout-container container mx-auto p-0'>
        <main className='layout'>
          <StaticAlert getVariant={ServerStatusModels.status}>
            <div style={{ backgroundColor: 'var(--bg-alt)' }} className='bg-black p-1 rounded w-32 h-full flex justify-around'>
              <button onClick={ () => ServerStatusModels.status++ } aria-label='Server Status Increase' className='flex flex-col justify-center items-center bg-red-500 w-full text-white mr-2'><FontAwesomeIcon icon={ faPlus } /></button>
              <button onClick={ () => ServerStatusModels.status-- } aria-label='Server Status Decrease' className='flex flex-col justify-center items-center bg-red-500 w-full text-white'><FontAwesomeIcon icon={ faMinus } /></button>
            </div>
          </StaticAlert>
          <CollapsibleSection header='Uptime' menu={ <GraphSwitcher />}>
            { GraphElement }
          </CollapsibleSection>
          <CollapsibleSection header='Stats' menu={ <RecentUptime /> }>
            <Stats />
          </CollapsibleSection>
          <CollapsibleSection header='Incidents Timeline' >
            <Timeline />
          </CollapsibleSection>
        </main>
      </div>
      <Footer />
    </>
  )
})

export default Status
