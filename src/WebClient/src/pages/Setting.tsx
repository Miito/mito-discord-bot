import React from 'react'
import NavBar from '../components/Nav/NavBar/NavBar'
import Footer from '../components/Footer/Footer'
import SettingSection from '../components/Setting/SettingSection/SettingSection'
import SettingGroup from '../components/Setting/SettingGroup/SettingGroup'
import BaseButton from '../components/Base/BaseButton/BaseButton'

const Support: React.FC = () => {
  return (
    <>
      <div className='relative z-10 container mx-auto p-0'>
        <NavBar />
      </div>
      <div className='setting layout-container container mx-auto p-0'>
        <div className='layout'>
          <SettingSection header='Test' >
            <SettingGroup>
              <h1>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Delectus deleniti nemo assumenda aliquid magni ipsum saepe, corrupti ea odio quae dolores quibusdam eius alias iure quod eligendi dolorum id expedita?
              Totam quos fugit suscipit illo ullam quod rem modi, aliquid cumque est quaerat error dolor neque maxime itaque consequuntur quis officia voluptates voluptate, porro molestiae similique dolores. Consectetur, ut expedita?
              Excepturi esse vel temporibus quasi natus, quis eligendi cum nostrum sequi aut dolorum neque tempore ipsa facere eos quo nemo autem, placeat non fugit ducimus fuga necessitatibus? Impedit, tempore numquam?
              Ab ex minus voluptate hic consequuntur soluta asperiores! Porro ex tempore non reprehenderit quaerat. Harum nesciunt cumque dolore vitae ipsum repudiandae dolores, aspernatur sint nostrum natus, incidunt pariatur rerum laboriosam.
              Dolore deserunt quis sunt error veniam sed id, soluta tempore est necessitatibus corrupti modi totam nostrum laudantium iste distinctio animi. Natus temporibus iste, tempore iure distinctio nihil eius quas qui.
              Eaque, placeat obcaecati dolore provident facere soluta amet architecto in aut nobis maxime necessitatibus ad voluptatem harum odit, porro veniam temporibus itaque reprehenderit doloremque accusamus! In nobis quis fugit earum!
              Quisquam dignissimos perferendis ut eveniet dolorem, vero asperiores veritatis earum repellat in hic reiciendis. Dicta aliquam repellendus esse ad, eius quos dolores eum facere exercitationem sequi alias doloremque numquam asperiores?
              Recusandae dicta unde possimus, eos facere excepturi itaque iste ipsa sit eveniet at omnis, necessitatibus totam in! Reiciendis asperiores dolore quaerat ipsum quasi sapiente ut culpa quas, id officiis aliquid.
              Vero velit nam pariatur id distinctio, voluptas repudiandae quisquam voluptatum officia soluta aspernatur itaque saepe architecto! Minus doloribus nesciunt eaque distinctio voluptatem esse, optio iure temporibus, cum, consequuntur recusandae expedita?
              Eos accusamus error corporis repellat magnam, rerum dolorum nemo. Minima, reprehenderit fugiat. Possimus pariatur fugit molestiae voluptatum magni architecto vel eligendi consequatur magnam, explicabo ratione animi in assumenda, quos dolorem.</h1>
            </SettingGroup>
          </SettingSection>
          <SettingSection header='Test 2' >
            <SettingGroup>
              <h1>Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore laudantium ea mollitia tempora quod. Cum est cupiditate repellendus! Expedita possimus voluptate, blanditiis quod ipsum delectus illo error perferendis eveniet repellat.
              Temporibus enim ipsam impedit maxime iusto reprehenderit porro, consequuntur nam, vitae dolores cumque tempora harum delectus inventore quia architecto accusamus laboriosam cum aperiam quasi quod dicta esse aut? Vel, quae.
              Illum quibusdam aliquam quod, earum architecto dicta nesciunt a necessitatibus eveniet mollitia consectetur perspiciatis minus aliquid, repudiandae provident ipsam voluptates esse reiciendis! Quo distinctio eveniet quisquam ipsa provident cum recusandae.
              Dolores sit, fuga distinctio soluta tenetur omnis! Explicabo rerum assumenda dolorem asperiores aliquam quo a accusantium vero ab, earum exercitationem culpa voluptates impedit dolorum voluptate natus? Rem neque architecto consequuntur!
              Consectetur officia ratione cumque ex recusandae omnis harum quibusdam repellat ad, porro impedit sed voluptas nostrum labore culpa vero quas aut officiis ipsum earum eius enim vel voluptate excepturi! Repellendus!
              Ullam veniam ex doloribus vero, expedita dolorum, illum ea pariatur, quae temporibus deserunt provident. Voluptatem suscipit repellendus provident, consequatur dolorum ab dolor hic esse porro nam, omnis dolores similique deleniti.
              Cupiditate, iure id repellat beatae nemo dolor! Veritatis, eum suscipit consectetur aliquid minima illum. Omnis laudantium consequatur consequuntur maxime rem aliquid dignissimos voluptatibus illo blanditiis! Nostrum a sapiente eaque quos.
              Blanditiis quas iusto ipsa eum voluptate, culpa doloremque accusantium omnis, minima consectetur aperiam nihil rerum assumenda, nobis ut aliquid deleniti! Expedita quo, et non obcaecati laboriosam ex facilis commodi. Quos?
              Amet numquam beatae, suscipit nesciunt quod vitae, ullam, adipisci officiis illum esse cumque iste? Vitae vel tenetur autem laboriosam voluptas in itaque possimus, exercitationem inventore nam non distinctio harum. Asperiores?
              Magni optio, doloribus aperiam eaque veniam nam natus corrupti! Beatae, cum tempore totam quos, officiis praesentium enim earum a eligendi molestias eaque deserunt illum dolor facilis, quia omnis ut maxime.</h1>
            </SettingGroup>
          </SettingSection>
        </div>
      </div>
      <Footer />
    </>
  )
}

export default Support
