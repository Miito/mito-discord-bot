import React from 'react'

interface Props {
  handleDrop: any
}

const FileDrop: React.FC<Props> = (props) => {
  const [drag, setDrag] = React.useState(false)
  const [filename, setFilename] = React.useState('')
  let dropRef = React.createRef<HTMLDivElement>()
  let dragCounter = 0

  const handleDrag = (e: DragEvent) => {
    e.preventDefault()
    e.stopPropagation()
  }

  // const handleDragIn = (e: DragEvent) => {
  //   e.preventDefault()
  //   e.stopPropagation()
  //   dragCounter++
  //   if (e?.dataTransfer.items && e?.dataTransfer.items.length > 0) setDrag(true)
  // }

  const handleDragIn = (e: any) => {
    var _a, _b;
    e.preventDefault();
    e.stopPropagation();
    dragCounter++;
    if (((_a = e) === null || _a === void 0 ? void 0 : _a.dataTransfer.items) && ((_b = e) === null || _b === void 0 ? void 0 : _b.dataTransfer.items.length) > 0)
        setDrag(true);
};


  const handleDragOut = (e: DragEvent) => {
    e.preventDefault()
    e.stopPropagation()
    dragCounter--
    if (dragCounter === 0) setDrag(false)
  }

  // const handleDrop = (e: DragEvent) => {
  //   e.preventDefault()
  //   e.stopPropagation()
  //   setDrag(false)
  //   if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
  //     props.handleDrop(e.dataTransfer.files[0])
  //     setFilename(e.dataTransfer.files[0].name)
  //     e.dataTransfer.clearData()
  //     dragCounter = 0
  //   }
  // }

  // TODO: Remove compile optional chaining
  const handleDrop = (e: any) => {
    var _a, _b, _c, _d, _e;
    e.preventDefault();
    e.stopPropagation();
    setDrag(false);
    if (((_a = e) === null || _a === void 0 ? void 0 : _a.dataTransfer.files) && ((_b = e) === null || _b === void 0 ? void 0 : _b.dataTransfer.files.length) > 0) {
        props.handleDrop((_c = e) === null || _c === void 0 ? void 0 : _c.dataTransfer.files[0]);
        setFilename((_d = e) === null || _d === void 0 ? void 0 : _d.dataTransfer.files[0].name);
        (_e = e) === null || _e === void 0 ? void 0 : _e.dataTransfer.clearData();
        dragCounter = 0;
    }
};

  // React.useEffect(() => {
  //   let div = dropRef.current
  //   div.addEventListener('dragenter', handleDragIn)
  //   div.addEventListener('dragleave', handleDragOut)
  //   div.addEventListener('dragover', handleDrag)
  //   div.addEventListener('drop', handleDrop)
  //   return function cleanup() {
  //     div.removeEventListener('dragenter', handleDragIn)
  //     div.removeEventListener('dragleave', handleDragOut)
  //     div.removeEventListener('dragover', handleDrag)
  //     div.removeEventListener('drop', handleDrop)
  //   }
  // })

  React.useEffect(() => {
    var _a, _b, _c, _d;
    let div = dropRef.current;
    (_a = div) === null || _a === void 0 ? void 0 : _a.addEventListener('dragenter', handleDragIn);
    (_b = div) === null || _b === void 0 ? void 0 : _b.addEventListener('dragleave', handleDragOut);
    (_c = div) === null || _c === void 0 ? void 0 : _c.addEventListener('dragover', handleDrag);
    (_d = div) === null || _d === void 0 ? void 0 : _d.addEventListener('drop', handleDrop);
    return function cleanup() {
        var _a, _b, _c, _d;
        (_a = div) === null || _a === void 0 ? void 0 : _a.removeEventListener('dragenter', handleDragIn);
        (_b = div) === null || _b === void 0 ? void 0 : _b.removeEventListener('dragleave', handleDragOut);
        (_c = div) === null || _c === void 0 ? void 0 : _c.removeEventListener('dragover', handleDrag);
        (_d = div) === null || _d === void 0 ? void 0 : _d.removeEventListener('drop', handleDrop);
    };
});


  return (
    <div
      ref={dropRef}
      className={drag ? 'filedrop drag' : filename ? 'filedrop ready' : 'filedrop'}
    >
      {filename && !drag ? <div>{filename}</div> : <div>Drop files here!</div>}
    </div>
  )
}

export default FileDrop
