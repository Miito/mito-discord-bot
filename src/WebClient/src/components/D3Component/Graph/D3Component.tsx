import React, { useRef, useEffect } from 'react'
import * as d3 from 'd3'

interface Props {
  data: number[]
  width: number
  height: number
}

const D3Component = (props: Props) => {
  const { width, height } = props
  const d3Container = useRef(null)
  const data = d3.csv("https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/3_TwoNumOrdered_comma.csv")
  let margin = {
    top: 0, right: 0, bottom: 0, left: 0
  },
  GraphWidth = width - margin.left - margin.right,
  GraphHeight = height - margin.top - margin.bottom

  function d (d: any) {
    return { date : d3.timeParse("%Y-%m-%d")(d.date), value : d.value }
  }

  useEffect(
    () => {
      if (props.data && d3Container.current) {
        const svg = d3.select(d3Container.current)
          .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")")

        let x = d3.scaleTime()
          // @ts-ignore
          .domain(d3.extent(data, function(d) { return d.date; }))
          .range([ 0, width ])

        svg.append("g")
          .attr("transform", `translate(0, ${height - 1})`)
          .call(d3.axisBottom(x))

        let y = d3.scaleLinear()
          // @ts-ignore
          .domain([0, d3.max(data, function(d) { return + d.value; })])
          .range([ height, 0 ]);
        svg.append("g")
          .call(d3.axisLeft(y));

        svg.append("path")
          .datum(data)
          .attr("fill", "#000000")
          .attr("stroke", "#000000")
          .attr("stroke-width", 1)
          // @ts-ignore
          .attr("d", d3.area()
            // @ts-ignore
            .x(function(d) { return x(d.date) })
            .y0(y(0))
            // @ts-ignore
            .y1(function(d) { return y(d.value) })
            )

        svg
          .exit()
          .remove()
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [props.data, d3Container.current]
  )

  return (
    <svg
      className='d3-component'
      width={ width }
      height={ height }
      ref={d3Container}
    />
  )
}

export default D3Component
