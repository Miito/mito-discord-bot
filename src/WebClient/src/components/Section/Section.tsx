import React from 'react'
import { observer } from 'mobx-react-lite'
import './Section.scss'

interface Props {
  header: string
  menu?: React.ReactNode
  children?: React.ReactNode | any
  style?: object
}

const Section: React.FC<Props> = observer((props) => {
  const { header, menu, children, style } = props

  return (
    <section className='section--card'>
      <header className='section__header'>
        <h1 className='section__h1'>{ header }</h1>
        <>
          { menu }
        </>
      </header>
      <div className='section__content' style={ style }>
        { children }
      </div>
    </section>
  )
})

export default Section
