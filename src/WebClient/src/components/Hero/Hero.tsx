import React from 'react'
import './Hero.scss'
import classNames from 'classnames'

interface Props {
  children: React.ReactNode
  className: string
}

const Hero: React.FC<Props> = (props) => {
  const {
    children,
    className
  } = props

  const classname = classNames('hero', className)

  return (
    <div className={ classname }>
      { children }
    </div>
  )
}

export default Hero
