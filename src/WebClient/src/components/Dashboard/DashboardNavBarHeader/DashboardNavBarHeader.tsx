import React from 'react'
import './DashboardNavBarHeader.scss'

interface Props {
  title: string
}

const DashboardNavBarHeader: React.FC<Props> = (props) => {
  const {
    title
  } = props
  return (
    <>
      <h1 className='dashboard-nav__header' >{title}</h1>
    </>
  )
}

export default DashboardNavBarHeader
