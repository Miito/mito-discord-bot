import React, { useContext } from 'react'
import useLocalStorage from '../../../hooks/useLocalStorage'
import { StatsContext } from '../../../models/Stats'
import { WidthProvider, Responsive } from 'react-grid-layout'
import StatsCards from '../../StatsCard/StatsCard'
import GraphCard from '../../Graph/GraphCard/GraphCard'
import './DashboardStats.scss'

interface Props {

}

const ResponsiveGridLayout = WidthProvider(Responsive)

const DashboardStats: React.FC<Props> = () => {
  const StatsModels = useContext(StatsContext)

  const DefaultLG = [
    {i: 'a', x: 0, y: 0, w: 2, h: 1},
    {i: 'b', x: 2, y: 0, w: 2, h: 1},
    {i: 'c', x: 4, y: 0, w: 2, h: 1},
    {i: 'd', x: 6, y: 0, w: 2, h: 1}
  ]

  const [layout, setLayout] = useLocalStorage('StatsSection', DefaultLG)

  const layouts = {
    lg: layout
  }

  const onLayoutChange = (currentLayout: any) => {
    setLayout(currentLayout)
  }

  return (
    <section className='dashboard-stats'>
      <h1 className='dashboard-stats__header' >Overview</h1>
      <ResponsiveGridLayout layouts={ layouts } onLayoutChange={ (currentLayout) => onLayoutChange(currentLayout) } margin={[ 18, 18 ]} width={ 880 }>
        <div key='a'>
          <StatsCards title='guilds connected' value={ StatsModels.guildsConnected }/>
        </div>
        <div key='b'>
          <StatsCards title='average commands' value={ StatsModels.averageCommand } unit='sec'/>
        </div>
        <div key='c'>
          <StatsCards title='average events ' value={ StatsModels.averageEvent } unit='sec'/>
        </div>
        <div key='d'>
          <GraphCard data={[1,2,3]} width={ 530 } height={ 170 }/>
        </div>
      </ResponsiveGridLayout>
    </section>
  )
}

export default DashboardStats
