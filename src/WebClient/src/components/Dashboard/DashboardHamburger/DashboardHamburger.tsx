import React from 'react'
import './DashboardHamburger.scss'
import { ReactComponent as Hamburger } from '../../../assets/Hamburger.svg'

interface Props {
  onClick?: any
}

const DashboardHamburger: React.FC<Props> = (props) => {
  const { onClick } = props

  return (
    <button className='dashboard-hamburger' onClick={ onClick }>
      <div className='dashboard-hamburger__container'>
        <Hamburger />
      </div>
    </button>
  )
}

export default DashboardHamburger
