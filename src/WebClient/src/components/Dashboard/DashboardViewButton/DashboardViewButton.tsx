import React, { useContext } from 'react'
import './DashboardViewButton.scss'
import BaseButton from '../../../components/Base/BaseButton/BaseButton'
import { faTv } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { FitModeContext } from '../../../models/TVMode'

interface Props {

}

const DashboardViewButton: React.FC<Props> = () => {
  const FitModeModels = useContext(FitModeContext)

  return (
    <BaseButton onClick={ () => FitModeModels.FitType } className='menu-btn text-white'>
      <FontAwesomeIcon icon={ faTv }/>
    </BaseButton>
  )
}

export default DashboardViewButton
