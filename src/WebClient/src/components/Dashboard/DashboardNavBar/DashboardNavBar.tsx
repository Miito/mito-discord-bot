import React from 'react'
import './DashboardNavBar.scss'

interface Props {
  header?: React.ReactNode | string
  content?: React.ReactNode | string
}

const DashboardNavBar: React.FC<Props> = (props) => {
  const {
    header,
    content
  } = props

  return (
    <nav className='dashboard-nav'>
      <div className="dashboard-nav__left">
        { header }
      </div>
      <div className="dashboard-nav__right">
        { content }
      </div>
    </nav>
  )
}

export default DashboardNavBar
