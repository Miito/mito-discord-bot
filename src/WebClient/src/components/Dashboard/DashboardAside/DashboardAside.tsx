import React, { useState, useContext } from 'react'
import './DashboardAside.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTv, faChartLine, faUserShield, faNetworkWired, faCog } from '@fortawesome/free-solid-svg-icons'
import DashboardHamburger from '../DashboardHamburger/DashboardHamburger'
import { Link } from 'react-router-dom'
import { FitModeContext } from '../../../models/TVMode'
import classNames from 'classnames'
import { observer } from 'mobx-react-lite'

interface Props {}

const DashboardAside: React.FC<Props> = observer(() => {
  const [toggle, setToggle] = useState(false)
  const FitModeModels = useContext(FitModeContext)

  const classnames = classNames('dashboard-aside', { hidden: FitModeModels.fitType })

  return (
    <aside className={ classnames }>
      <div className='dashboard-aside__items'>
        <DashboardHamburger onClick={ () => setToggle(!toggle) }/>
      </div>
      <Link to='/dashboard/' className='dashboard-aside__items'>
        <FontAwesomeIcon icon={ faTv } size='2x'/>
        Dashboard
      </Link>
      <Link to='/dashboard/stats' className='dashboard-aside__items'>
        <FontAwesomeIcon icon={ faChartLine } size='2x'/>
        Stats
      </Link>
      <Link to='/dashboard/admin' className='dashboard-aside__items'>
        <FontAwesomeIcon icon={ faUserShield } size='2x'/>
        Admin
      </Link>
      <Link to='/dashboard/flow' className='dashboard-aside__items'>
        <FontAwesomeIcon icon={ faNetworkWired } size='2x'/>
        Flow
      </Link>
      <Link to='/dashboard/settings' className='dashboard-aside__items'>
        <FontAwesomeIcon icon={ faCog } size='2x'/>
        Settings
      </Link>
    </aside>
  )
})

export default DashboardAside
