import React from 'react'
import classnames from 'classnames'
import './TimelineRow.scss'

interface Props {
  label: string | React.ReactNode
  description: string | React.ReactNode
  children?: React.ReactNode
  className?: string
}

function TimelineRow (props: Props) {
  const { label, description, children, className } = props

  const ClassName = () => classnames('timeline-row__container', className)

  return (
    <li className={ ClassName() }>
      <div className='timeline-row__content'>
        <div className='timeline-row__header'>
          <div className='timeline-row__title'>
            <h2 className='timeline-row__label'>{ label }</h2>
          </div>
          <p className='timeline-row__description'>{ description }</p>
        </div>
        <div className='timeline-row__body'>{ children }</div>
      </div>
    </li>
)
}

export default TimelineRow
