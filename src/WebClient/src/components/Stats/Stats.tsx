import './Stats.scss'
import { Responsive, WidthProvider } from 'react-grid-layout';
import { StatsContext } from '../../models/Stats';
import GraphCard from '../Graph/GraphCard/GraphCard';
import React, { useContext } from 'react'
import StatsCards from '../StatsCard/StatsCard';
import useLocalStorage from '../../hooks/useLocalStorage';

const ResponsiveGridLayout = WidthProvider(Responsive)

const Stats: React.FC = () => {
  const StatsModels = useContext(StatsContext)

  const DefaultLG = [
    {i: 'a', x: 0, y: 0, w: 2, h: 1},
    {i: 'b', x: 2, y: 0, w: 2, h: 1},
    {i: 'c', x: 4, y: 0, w: 2, h: 1},
    {i: 'd', x: 6, y: 0, w: 2, h: 1}
  ]

  const [layout, setLayout] = useLocalStorage('StatsSection', DefaultLG)

  const layouts = {
    lg: layout
  }

  const onLayoutChange = (currentLayout: any) => {
    setLayout(currentLayout)
  }

  return (
    <ResponsiveGridLayout verticalCompact={ true } layouts={ layouts } onLayoutChange={ (currentLayout) => onLayoutChange(currentLayout) } margin={[ 18, 18 ]} rowHeight={ 164 } width={ 880 }
      breakpoints={{lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0}}
      cols={{lg: 24, md: 22, sm: 18, xs: 4, xxs: 2}}
      >
        <div key='a'>
          <StatsCards title='guilds connected' value={ StatsModels.guildsConnected }/>
        </div>
        <div key='b'>
          <StatsCards title='average commands' value={ StatsModels.averageCommand } unit='sec'/>
        </div>
        <div key='c'>
          <StatsCards title='average events ' value={ StatsModels.averageEvent } unit='sec'/>
        </div>
        <div key='d'>
          <GraphCard data={[1,2,3]} width={ 625 } height={ 170 }/>
        </div>
    </ResponsiveGridLayout>
  )
}

export default Stats
