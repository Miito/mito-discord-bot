import React from 'react'
import './StaticAlert.scss'

function getvariant(status: number) {
  switch (status) {
    case 0:
      return 'alert alert--major'
    case 1:
    case 2:
      return 'alert alert--partial'
    case 3:
      return 'alert alert--operational'
    default:
      return 'alert alert--unknown'
  }
}

function getStatus(status: number) {
  switch (status) {
    case 0:
      return 'Major Outage'
    case 1:
      return 'Partial Outage'
    case 2:
      return 'Degraded Performance'
    case 3:
      return 'Operational'
    default:
      return 'Status Unknown'
  }
}

interface Props {
  getVariant: number
  children?: React.ReactNode
}

const StaticAlert: React.FC<Props> = (props) => {
  const { getVariant, children } = props

  return (
    <div className={ getvariant(getVariant) } role='alert'>
      <div className='alert__header'>
        <h1 className='alert__h1'>{ getStatus(getVariant) }</h1>
      </div>
      { children }
    </div>
  )
}

export default StaticAlert
