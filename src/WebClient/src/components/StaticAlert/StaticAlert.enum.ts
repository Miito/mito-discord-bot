enum status {
  StatusUnknown = -1,
  MajorOutage,
  PartialOutage,
  DegradedPerformance,
  Operational,
}

export default status
