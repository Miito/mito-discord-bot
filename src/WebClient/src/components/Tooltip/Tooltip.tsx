import React from 'react'
import ReactTooltip from 'react-tooltip'

interface Props {
}

const Tooltip: React.FC<Props> = (props) => {
  return (
    <ReactTooltip effect='solid' offset={{top: -10}} />
  )
}

export default Tooltip
