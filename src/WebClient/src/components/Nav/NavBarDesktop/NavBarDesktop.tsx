import React from 'react'
import './NavBarDesktop.scss'
import { observer } from 'mobx-react-lite'
import { Link } from 'react-router-dom'
import Logo from '../../Logo/Logo'
import NavAvatar from '../NavAvatar/NavAvatar'
import UserAuth from '../../../auth/UserAuth'

interface Props {}

const NavBarDesktop: React.FC<Props> = observer(() => {
  const isLogin = UserAuth

  return (
    <nav className='nav' role='navigation'>
      <div className='nav__outer'>
        <div className='nav__container'>
          <div className='nav__header'>
            <Link to='/'>
              <Logo alt='Logo' className='nav__logo'/>
            </Link>
          </div>
          <Link to='/' className='nav__title'>MitoBot</Link>
        </div>
        <div className='nav__container'>
          <div className='nav__hover'>
            <Link to='/' className='nav__link'>Home</Link>
            <Link to='/support' className='nav__link'>Support</Link>
            <Link to='/status' className='nav__link'>Status</Link>
          </div>
          {!isLogin ?
            <Link to='/login' className='nav__link'>login</Link>
            :
            <NavAvatar />
          }
        </div>
      </div>
    </nav>
  )
})

export default NavBarDesktop
