import React, { useState, useRef, useEffect } from 'react'
import './NavAvatar.scss'
import NavAvatarMenu from '../NavAvatarMenu/NavAvatarMenu'
import classNames from 'classnames'

interface Props {

}

const NavAvatar: React.FC<Props> = () => {
  const [toggle, setToggle] = useState(false)
  const AvatarPopoutToggle = () => {
    setToggle(!toggle)
  }
  const node = useRef(null)

  const handleClick = (e: unknown) => {
    // @ts-ignore
    if (node.current.contains(e.target)) {
      return
    }
    setToggle(false)
  }

  useEffect(() => {
    document.addEventListener('mousedown', handleClick)
    return () => {
      document.removeEventListener('mousedown', handleClick)
    }
  }, [])

  const avatarOutline = classNames('nav-avatar', { 'nav-avatar--active': toggle })

  return (
    <div className={ avatarOutline } ref={ node }>
      <div className='nav-avatar__container' onClick={ () => AvatarPopoutToggle() }>
        <span className='bg-black text-white text-center block' style={{ height: 60, width: 60 }}>
          User Avatar
        </span>
      </div>
      <NavAvatarMenu open={ toggle } />
    </div>
  )
}

export default NavAvatar
