import React, { useState } from 'react'
import './NavBarMobile.scss'
import Logo from '../../Logo/Logo'
import { Link } from 'react-router-dom'
import NavBarMobileHamburger from '../../Nav/NavBarMobileHamburger/NavBarMobileHamburger'
import Collapsible from '../../Animation/Collapsible/Collapsible'
import { Home, Support, Status, Login } from '../../../Route'

interface Props {}

const NavBarMobile: React.FC<Props> = () => {
  const [toggle, setToggle] = useState(false)

  return (
    <>
      <nav className='nav--mobile' role='navigation'>
        <div className='nav__container'>
        <Link onClick={ () => setToggle(false) } to='/'>
          <Logo alt='Logo' className='nav__logo nav__logo--mobile'/>
        </Link>
        <Link onClick={ () => setToggle(false) } to='/' className='nav__title nav__title--mobile'>MitoBot</Link>
        </div>
        <div className='nav__container'>
          <NavBarMobileHamburger ariaExpanded={ toggle } ariaControls='menu' onClick={ () => setToggle(!toggle) }/>
        </div>
        <div className='nav-menu nav-menu--mobile'>
          <Collapsible isOpen={ toggle }>
            <ul className='nav-menu__container' id='menu'>
              <li className='nav-menu__item'>
                <Link onClick={ () => setToggle(false) } onMouseOver={ () => Home.preload() } to='/' className='nav__link nav__link--mobile'>Home</Link>
              </li>
              <li className='nav-menu__item'>
                <Link onClick={ () => setToggle(false) } onMouseOver={ () => Support.preload() } to='/support' className='nav__link nav__link--mobile'>Support</Link>
              </li>
              <li className='nav-menu__item'>
                <Link onClick={ () => setToggle(false) } onMouseOver={ () => Status.preload() } to='/status' className='nav__link nav__link--mobile'>Status</Link>
              </li>
              <li className='nav-menu__item'>
                <Link onClick={ () => setToggle(false) } onMouseOver={ () => Login.preload() } to='/login' className='nav__link nav__link--mobile'>Login</Link>
              </li>
            </ul>
          </Collapsible>
        </div>
      </nav>
    </>
  )
}

export default NavBarMobile
