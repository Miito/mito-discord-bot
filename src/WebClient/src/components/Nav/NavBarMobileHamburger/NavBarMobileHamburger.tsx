import React from 'react'
import './NavBarMobileHamburger.scss'
import { ReactComponent as Hamburger } from '../../../assets/Hamburger.svg'

interface Props {
  onClick?: any
  ariaExpanded?: any
  ariaControls?: any
}

const NavBarMobileHamburger: React.FC<Props> = (props) => {
  const {
    onClick,
    ariaExpanded,
    ariaControls
  } = props

  return (
    <button className='nav-hamburger' aria-controls={ ariaControls } saria-expanded={ ariaExpanded } onClick={ onClick }>
      <div className='nav-hamburger__container'>
        <Hamburger />
      </div>
    </button>
  )
}

export default NavBarMobileHamburger
