import React from 'react'
import './NavBar.scss'
import { observer } from 'mobx-react-lite'
import { useMediaQuery } from 'react-responsive'
import NavBarDesktop from '../NavBarDesktop/NavBarDesktop'
import NavBarMobile from '../NavBarMobile/NavBarMobile'

const NavBar: React.FC = observer(() => {
  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-width: 610px)'
  })
  const isTabletOrMobile = useMediaQuery({
    query: '(max-width: 609px)'
  })

  return (
    <>
      {isDesktopOrLaptop && <NavBarDesktop />}
      {isTabletOrMobile && <NavBarMobile />}
    </>
  )
})

export default NavBar
