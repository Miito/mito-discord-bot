import React from 'react'
import { Link } from 'react-router-dom';
import './NavAvatarMenu.scss'
import classnames from 'classnames'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faCog, faMoon, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'

interface Props {
  open?: any
}

const NavAvatarMenu: React.FC<Props> = (props) => {
  const {
    open
  } = props

  const signout = () => {
    console.log('signout')
  }

  const darkmode = () => {
    console.log('darkmode')
  }

  const classNames = classnames('nav-menu', { 'nav-menu--hidden': !open })

  return (
    <div className={ classNames }>
      <div className='nav-menu__items'>
        <Link to='/profile'><FontAwesomeIcon icon={ faUser } />My Profile</Link>
      </div>
      <div className='nav-menu__items'>
        <Link to='/setting'><FontAwesomeIcon icon={ faCog } />Setting</Link>
      </div>
      <div className='nav-menu__items'>
        <button onClick={ darkmode }><FontAwesomeIcon icon={ faMoon } />Dark Theme: On</button>
      </div>
      <div className='nav-menu__items'>
        <button onClick={ signout }><FontAwesomeIcon icon={ faSignOutAlt } />Sign Out</button>
      </div>
    </div>
  )
}

export default NavAvatarMenu
