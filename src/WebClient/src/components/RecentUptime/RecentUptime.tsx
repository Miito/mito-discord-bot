import React from 'react'
import './RecentUptime.scss'
import { useMediaQuery } from 'react-responsive'

const RecentUptime: React.FC = () => {
  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-width: 610px)'
  })
  const isTabletOrMobile = useMediaQuery({
    query: '(max-width: 609px)'
  })

  return (
    <div className='recent-uptime'>
      <>
      {isDesktopOrLaptop && <div className='recent-uptime__header'>updated a minute ago</div>}
      {isTabletOrMobile && <div className='recent-uptime__header'>updated 1m</div>}
      </>
    </div>
  )
}

export default RecentUptime
