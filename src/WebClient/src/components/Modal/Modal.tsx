import React, { useEffect } from 'react'
import { createPortal } from 'react-dom'
import './Modal.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

const Portal = ({ children }: { children: React.ReactNode }) => {
  const modalRoot = document.getElementById('portal-root')
  const el = document.createElement('div')

  useEffect(() => {
    //@ts-ignore
    modalRoot.appendChild(el)
  }, [modalRoot, el])
  //@ts-ignore
  useEffect(() => {
    //@ts-ignore
    return () => modalRoot.removeChild(el)
  })

  return createPortal(children, el)
}

const Toasts = ({ children, toggle, open }: { children: React.ReactNode, toggle: any, open: any }) => {
  return (
  <Portal>
    {open && (
      <div className='modal__wrapper'>
        <div className='modal__card'>
          <button onClick={toggle} className='modal__close-button'>
            <FontAwesomeIcon icon={ faTimes }></FontAwesomeIcon>
          </button>
          {children}
        </div>
        <div onClick={toggle} className='modal__background'/>
      </div>
    )}
  </Portal>
  )
  }

export default Toasts
