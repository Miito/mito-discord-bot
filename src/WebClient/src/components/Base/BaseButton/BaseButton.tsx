import React from 'react'
import './BaseButton.scss'

interface Props {
  className?: string
  children?: React.ReactNode
  onClick?: any
}

const BaseButton: React.FC<Props> = (props) => {
  const {
    className,
    children,
    onClick
  } = props

  return (
    <button onClick={ () => onClick }className={ className }>
      { children }
    </button>
  )
}

export default BaseButton
