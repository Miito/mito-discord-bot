import React from 'react'
import classNames from 'classnames'
import './BaseCard.scss'

interface Props {
  children: React.ReactNode
  className?: string
}

const BaseCard: React.FC<Props> = (props) => {
  const { children, className } = props
  const classname = classNames('card', className)

  return (
    <div className={ classname }>
      <div className='card__outer'>
        { children }
      </div>
    </div>
  )
}

export default BaseCard
