import React from 'react'
import BaseCard from '../Base/BaseCard/BaseCard'
import './Card.scss'

interface Props {
  title: string
  children?: React.ReactNode
}

const Cards: React.FC<Props> = (props) => {
  const {
    title,
    children
  } = props

  return (
    <BaseCard>
      <header>
        <h1 className='card__title'>{ title }</h1>
      </header>
      <div className='card__inner'>
        { children }
      </div>
    </BaseCard>
  )
}

export default Cards
