import React from 'react'
import './SettingGroup.scss'

interface Props {
  children: JSX.Element | JSX.Element[]
}

const SettingGroup: React.FC<Props> = (props) => {
  const {
    children
  } = props

  return (
    <div className='setting-group'>
      { children }
    </div>
  )
}

export default SettingGroup
