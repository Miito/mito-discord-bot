import React from 'react'
import './SettingSection.scss'

interface Props {
  header: string
  children: JSX.Element | JSX.Element[]
}

const SettingSection: React.FC<Props> = (props) => {
  const {
    header,
    children
  } = props
  return (
    <section className='setting-edit' >
      <div className='setting-edit__header'>
        <h1 className='setting-edit__title'>{ header }</h1>
      </div>
      <div className='setting-edit__content'>
        { children }
      </div>
    </section>
  )
}

export default SettingSection
