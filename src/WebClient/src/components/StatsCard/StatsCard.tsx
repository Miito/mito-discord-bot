import React from 'react'
import Card from '../Card/Card'
import './StatsCard.scss'

interface Props {
  title: string
  value: number
  unit?: string
}

const StatsCards: React.FC<Props> = (props) => {
  const {
    title,
    value,
    unit,
  } = props

  return (
    <Card title={ title } >
      <p className='card__value'>{ value }{ unit && <span className='card__value card__value--subscript'>/{unit}</span> }</p>
    </Card>
  )
}

export default StatsCards
