import React from 'react'
import BaseCard from '../../Base/BaseCard/BaseCard'
import './GraphCard.scss'
import D3Component from '../../D3Component/Graph/D3Component'

interface Props {
  data: number[]
  width: number
  height: number
}

const GraphCard: React.FC<Props> = (props) => {
  const { data, width, height } = props

  return (
    <BaseCard className='graph-date-card'>
      <D3Component data={ data } width={ width } height={ height } ></D3Component>
    </BaseCard>
  )
}

export default GraphCard
