import React from 'react'
import BaseCard from '../../Base/BaseCard/BaseCard'
import './GraphDateCard.scss'

interface Props {
  header: string,
  stats: number
}

const GraphDateCard: React.FC<Props> = (props) => {
  const { header, stats } = props

  return (
    <BaseCard className='graph-date-card'>
      <div className='graph-date-card__header'>
        <h1 className='graph-date-card__h1'>{ header }</h1>
        <h1 className='graph-date-card__stats'>{ `${stats}%` }</h1>
      </div>
      <div className='graph-date-card__grid'>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
        <span className='graph-date-card__items'></span>
      </div>
    </BaseCard>
  )
}

export default GraphDateCard
