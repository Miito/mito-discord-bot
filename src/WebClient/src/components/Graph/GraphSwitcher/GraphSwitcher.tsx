import React, { useContext, useEffect } from 'react'
import GraphMenuButton from '../GraphMenuButton/GraphMenuButton'
import './GraphSwitcher.scss'
import { ReactComponent as Timeline } from '../../../assets/Timeline.svg'
import { ReactComponent as Grid } from '../../../assets/Grid.svg'
import { GraphContext } from '../../../models/GraphType'
import { observer } from 'mobx-react-lite'
import useLocalStorage from '../../../hooks/useLocalStorage'

const GraphSwitcher = observer(() => {
  const GraphStore = useContext(GraphContext)

  const activeBtn = (value: number) => graphType === value ? 'graph-switcher__button--active': 'graph-switcher__button'
  const [graphType, setGraphType] = useLocalStorage(`GraphSwitcher`, 1)

  useEffect(() => {
    GraphStore.GraphType(graphType)
  })

  return (
    <div className='graph-switcher'>
      <GraphMenuButton aria-label='Select Grid Graph' onClick={ () => setGraphType(0) } className={ activeBtn(0) }><Grid /></GraphMenuButton>
      <GraphMenuButton aria-label='Select Timeline Graph' onClick={ () => setGraphType(1) } className={ activeBtn(1) }><Timeline /></GraphMenuButton>
    </div>
  )
})

export default GraphSwitcher
