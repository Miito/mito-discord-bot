import React from 'react'
import './GraphTimeline.scss'
import GraphTimelineRow from '../GraphTimelineRow/GraphTimelineRow'
import GraphTimelineStatus from '../GraphTimelineStatus/GraphTimelineStatus'

const GraphTimeline = () => {
  return (
    <div className='graph-timeline'>
      <GraphTimelineRow title='Discord' status='Great' ></GraphTimelineRow>
      <GraphTimelineRow title='Website' status='Great' ></GraphTimelineRow>
      <GraphTimelineRow title='Database' status='Great' ></GraphTimelineRow>
      <GraphTimelineStatus />
    </div>
  )
}

export default GraphTimeline
