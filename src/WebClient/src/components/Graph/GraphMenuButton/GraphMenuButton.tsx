import React from 'react'
import classNames from 'classnames'
import './GraphMenuButton.scss'

interface Props {
  text?: string
  className?: any
  children?: React.ReactNode
  onClick?: any
}

const GraphMenuButton: React.FC<Props> = (props) => {
  const {
    text,
    className,
    children,
    onClick,
  } = props

  const btnClass = classNames('menu-btn', className)

  return (
    <button onClick={ onClick } className={ btnClass }>
      {text && <span>{ text }</span>}
      { children }
    </button>
  )
}

export default GraphMenuButton
