import React from 'react'
import './GraphTimelineStatus.scss'

interface Props {
}

const GraphTimelineStatus: React.FC<Props> = (props) => {
  return (
    <div className='graph-timeline-status'>
      <h1 className='graph-timeline-status__header'>
        90 days ago
      </h1>
      <span className='graph-timeline-status__status'>
        updated a minute ago
      </span>
    </div>
  )
}

export default GraphTimelineStatus
