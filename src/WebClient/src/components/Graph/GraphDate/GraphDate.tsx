import React from 'react'
import GraphDateCard from '../GraphDateCard/GraphDateCard'
import './GraphDate.scss'

interface Props {

}

const GraphDate: React.FC<Props> = () => {
  return (
    <div className='graph-date'>
      <GraphDateCard header='July 2019' stats={ 99.34 }></GraphDateCard>
      <GraphDateCard header='August 2019' stats={ 98.86 }></GraphDateCard>
      <GraphDateCard header='September 2019' stats={ 98.21 }></GraphDateCard>
    </div>
  )
}

export default GraphDate
