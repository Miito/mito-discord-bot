import React from 'react'
import './GraphTimelineRow.scss'

interface Props {
  title: string
  status: string
}

const GraphTimelineRow: React.FC<Props> = (props) => {
  const { title, status } = props

  return (
    <div className='graph-timeline-row'>
      <div className='graph-timeline-row__header'>
        <h2 className='graph-timeline-row__title'>{ title }</h2>
        <span className='graph-timeline-row__status'>{ status }</span>
      </div>
      <div className='graph-timeline-row__stats'>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
        <span className="graph-timeline-row__items"></span>
      </div>
    </div>
  )
}

export default GraphTimelineRow
