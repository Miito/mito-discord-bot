import React from 'react'
import './LoadingWidget.scss'

const LoadingWidget: React.FC = () => {
  return (
    <div className='loading'>
      <div className='loading-bar'></div>
      <div className='loading-bar'></div>
      <div className='loading-bar'></div>
      <div className='loading-bar'></div>
    </div>
  )
}

export default LoadingWidget
