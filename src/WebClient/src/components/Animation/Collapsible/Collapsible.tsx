import React from 'react'
import { animated, useSpring } from 'react-spring'
import ResizeObserver from 'resize-observer-polyfill'
import * as d3 from 'd3-ease'

const usePrevious = (value: any) => {
  const ref = React.useRef()

  React.useEffect(() => {
    ref.current = value
  }, [value])

  return ref.current
}

const useScrollHeight = () => {
  const ref = React.useRef<HTMLHeadingElement>(null)
  const [height, setHeight] = React.useState(0)

  React.useEffect(() => {
    const ro = new ResizeObserver(([entry]) => {
      setHeight(entry.target.scrollHeight)
    })

    if (ref.current) {
      ro.observe(ref.current)
    }

    return () => {
      ro.disconnect()
    }
  }, [])

  return [ref, height]
}

interface Props {
  children?: React.ReactChild | any
  isOpen: boolean
}

const Collapsible: React.FC<Props> = (props) => {
  const {children, isOpen = false} = props
  const [ref, height] = useScrollHeight()
  const wasOpen = usePrevious(isOpen)
  const anim = useSpring({
    to: {opacity: isOpen ? 1 : 0, height: isOpen ? height : 0},
    config: { duration: 200, easings: d3.easeCubicOut(1) },
  })
  const style = {
    ...anim,
    // @ts-ignore
    height: isOpen && wasOpen ? 'auto' : anim.height,
    width: '100%',
    overflow: 'hidden'
  }

  return (
    // @ts-ignore
    <animated.div ref={ref} style={style}>
      {children}
    </animated.div>
  )
}

export default Collapsible
