import React from 'react'
import './CollapsibleSection.scss'
import Section from '../../Section/Section'
import Collapsible from '../../Animation/Collapsible/Collapsible'
import CollapsibleButton from '../CollapsibleButton/CollapsibleButton'
import useLocalStorage from '../../../hooks/useLocalStorage'

interface Props {
  header: string
  menu?: React.ReactNode
  children?: React.ReactNode | any
  style?: object
}

const CollapsibleSection: React.FC<Props> = (props) => {
  const {
    header,
    menu,
    children,
    style,
  } = props
  const [collapsible, setCollapsible] = useLocalStorage(`${header.replace(/\s+/g, '')}CollapsibleSection`, true)

  return (
    <Section header={ header } style={ style } menu={ <CollapsibleButton aria-label='Collapse Section' menu={ menu } onClick={ () => setCollapsible(!collapsible) }/> }>
      <Collapsible isOpen={ collapsible }>
        { children }
      </Collapsible>
    </Section>
  )
}

export default CollapsibleSection
