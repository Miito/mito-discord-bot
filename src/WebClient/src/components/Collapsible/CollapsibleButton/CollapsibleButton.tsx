import React from 'react'
import './CollapsibleButton.scss'
import GraphMenuButton from '../../Graph/GraphMenuButton/GraphMenuButton'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'

interface Props {
  menu?: any
  onClick?: any
}

const CollapsibleButton: React.FC<Props> = (props) => {
  const {
    menu,
    onClick,
  } = props

  return (
      <div className='flex'>
        { menu }
        <GraphMenuButton onClick={ () => onClick() } className='graph-switcher__button--active'>
          <FontAwesomeIcon icon={ faChevronDown } size='xs'/>
        </GraphMenuButton>
      </div>
  )
}

export default CollapsibleButton
