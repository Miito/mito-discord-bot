import React from 'react'
import Card from '../../Card/Card'
import './Card.scss'

interface Props {
  title: string
  children?: JSX.Element[] | JSX.Element
}

const IncidentsCard: React.FC<Props> = (props) => {
  const {
    title,
    children
  } = props

  return (
    <Card title={ title } >
      { children }
    </Card>
  )
}

export default IncidentsCard
