import React from 'react'
import classnames from 'classnames'
import './IncidentsTimeline.scss'
import moment from 'moment-timezone'
import TimelineRow from '../../TimelineRow/TimelineRow'
import IncidentItem from '../IncidentItem/IncidentItem'
import MaintenanceItem from '../../MaintenanceItem/MaintenanceItem'

interface Props {
  children?: React.ReactNode
  className?: string
}

function Timeline (props: Props) {
  const { children, className } = props

  const ClassName = () => classnames('incidents-timeline', className)

  const dates = {}

  const getDateTime = (datetime: any) => {
    return moment.tz(datetime, moment.tz.guess())
  }

  const getFormattedDateTime = (datetime: any, fmt = 'MMM DD, YYYY - HH:mm (z)') => {
    return getDateTime(datetime).format(fmt)
  }

  // To State
  const numDisplayDates = 10
  const dateFormat = 'MMM DD, YYYY'
  const incidents = [{ incidentID: 1, name: 'xd', status: '02', createAt: new Date().toISOString(), updatedAt: new Date().toISOString() }]

  const renderDateItem = (date: any, events: any) => {
    const dateItems = events.map((event: any) => {
      if (event.hasOwnProperty('incidentID')) {
        // @ts-ignore
        // return (<IncidentItem key={event.incidentID} incidentID={event.incidentID} autoloadDetail />)
      } else if (event.hasOwnProperty('maintenanceID')) {
        // return (<MaintenanceItem key={event.maintenanceID} maintenanceID={event.maintenanceID} autoloadDetail />)
      } else {
        // throw new Error('Unknown event: ', event)
      }
    })

    let container = null
    if (dateItems.length > 0) {
      container = (
        <div className='incidents-container'>
          <ul>{dateItems}</ul>
        </div>
      )
    } else {
      container = (<span className='no-incidents'>No incidents reported.</span>)
    }

    return (
      <li key={date} className={classnames('date-item')}>
        <div className={classnames('border')}>{date}</div>
        {container}
      </li>
    )
  }

  for (let i = 0; i < numDisplayDates; i++) {
    const date = moment().subtract(i, 'days').format(dateFormat)
    // @ts-ignore
    dates[date] = []
  }

  incidents.forEach(incident => {
    const updatedAt = getFormattedDateTime(incident.updatedAt, dateFormat)
    // @ts-ignore
    if (dates.hasOwnProperty(updatedAt)) dates[updatedAt].push(incident)
  })

  // @ts-ignore
  Object.keys(dates).map(date => dates[date].sort((a, b) => {
    if (a.updatedAt < b.updatedAt) return 1
    if (a.updatedAt > b.updatedAt) return -1
    return 0
  }))

  return (
    <ol className={ ClassName() }>
      {/* {Object.keys(dates).map(date =>
        // @ts-ignore
        renderDateItem(date, dates[date])
      )} */}
      {Object.keys(dates).map(date => {
        return <TimelineRow key={ date.toString() } label={ date } description='No incidents reported.'/>
      })}
      { children }
    </ol>
  )
}

export default Timeline
