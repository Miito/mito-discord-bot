import React from 'react'
import BaseButton from '../Base/BaseButton/BaseButton'
import './LoginButton.scss'

interface Props {
  children: React.ReactNode
}

const LoginButton: React.FC<Props> = (props) => {
  const {
    children
  } = props

  return (
    <BaseButton className='login-button' >
      { children }
    </BaseButton>
  )
}

export default LoginButton
