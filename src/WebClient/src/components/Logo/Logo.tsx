import React from 'react'
import avatar from '../../assets/avatar.jpg'
import classNames from 'classnames'

interface Props {
  alt?: string
  crossOrigin?: 'anonymous' | 'use-credentials' | ''
  decoding?: 'async' | 'auto' | 'sync'
  height?: number | string
  sizes?: string
  src?: string
  srcSet?: string
  useMap?: string
  width?: number | string
  className?: string
}

const Logo: React.FC<Props> = (props) => {
  const {
    alt,
    className
  } = props

  const classnames = classNames('logo', className)

  return (
    <img src={avatar} alt={ alt } className={ classnames }/>
  )
}

export default Logo
