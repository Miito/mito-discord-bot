import React from 'react'
import './NoResult.scss'

const NoResult: React.FC = () => {
  return (
    <div className='no-result'>updated a minute ago</div>
  )
}

export default NoResult
