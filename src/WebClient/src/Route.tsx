import React from 'react'
import { Route, Redirect } from 'react-router'
import UserAuth from './auth/UserAuth'

import Loadable from 'react-loadable'
import LoadingPage from './pages/LoadingPage'

const L = (props: any) => {
  return Loadable({
    loading: LoadingPage,
    ...props
  })
}

export const Home = L({loader: () => import('./pages/Home')})
export const Contact = L({loader: () => import('./pages/Contact')})
export const Support = L({loader: () => import('./pages/Support')})
export const Status = L({loader: () => import('./pages/Status')})
export const NotFoundPage = L({loader: () => import('./pages/404')})
export const Commands = L({loader: () => import('./pages/Commands')})
export const Profile = L({loader: () => import('./pages/Profile')})
export const Setting = L({loader: () => import('./pages/Setting')})
export const Test = L({loader: () => import('./pages/Test')})
export const Login = L({loader: () => import('./pages/Login')})
export const Dashboard = L({loader: () => import('./pages/Dashboard')})
export const DashboardHome = L({loader: () => import('./components/Dashboard/DashboardHome/DashboardHome')})
export const DashboardStats = L({loader: () => import('./components/Dashboard/DashboardStats/DashboardStats')})
export const DashboardAdmin = L({loader: () => import('./components/Dashboard/DashboardAdmin/DashboardAdmin')})
export const DashboardFlow = L({loader: () => import('./components/Dashboard/DashboardFlow/DashboardFlow')})
export const DashboardSettings = L({loader: () => import('./components/Dashboard/DashboardSettings/DashboardSettings')})

const PrivateRoute = ({ component, ...rest }: any) => {
  const routeComponent = (props: any) => (
    UserAuth
      ? React.createElement(component, props)
      : <Redirect to={{ pathname: '/login' }}/>
  );
  return <Route {...rest} render={routeComponent}/>;
};

export default PrivateRoute
