import { observable } from 'mobx'
import { createContext } from 'react'

class ServerStatusModels {
  @observable status: number = -1
}

export const ServerStatusContext = createContext(new ServerStatusModels())
