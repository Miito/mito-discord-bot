import { observable } from 'mobx'
import { createContext } from 'react'

const minmax = (min: number, max: number): number => Math.floor(Math.random() * (max - min + 1) + min)

class StatsModels {
  @observable editMode = false
  @observable guildsConnected = minmax(0, 7999)
  @observable averageCommand = minmax(0, 3999)
  @observable averageEvent = minmax(0, 5999)
}

export const StatsContext = createContext(new StatsModels())
