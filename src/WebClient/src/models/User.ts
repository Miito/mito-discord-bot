import { observable, action } from 'mobx'
import { createContext } from 'react'

class UserModels {
  @observable avatarURL: string = ''
  @observable username: string = ''

}

export const UserContext = createContext(new UserModels())
