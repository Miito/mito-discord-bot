import { observable, action } from 'mobx'
import { createContext } from 'react'

class MenuModels {
  @observable toggle = false

  @action
  Toggle = (current = !this.toggle) => {
    this.toggle = current
  }
}

export const MenuContext = createContext(new MenuModels())
