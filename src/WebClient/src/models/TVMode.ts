import { observable, action } from 'mobx'
import { createContext } from 'react'

class FitModeModels {
  @observable fitType = false

  @action FitType = () => {
    this.fitType = !this.fitType
  }
}

export const FitModeContext = createContext(new FitModeModels())
