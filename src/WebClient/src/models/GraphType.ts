import { observable, action } from 'mobx'
import { createContext } from 'react'

enum Graph {
  Date,
  Timeline
}

class GraphModels {
  @observable graphType: Graph = 1

  @action GraphType = (type: Graph) => {
    this.graphType = type
  }
}

export const GraphContext = createContext(new GraphModels())
