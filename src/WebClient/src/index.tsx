import React from 'react'
import ReactDOM from 'react-dom'
import './styles/tailwind.css'
import './assets/Inter/inter.css'
import './styles/global.css'
import App from './App'
import * as serviceWorker from './serviceWorker'

if (process.env.NODE_ENV !== 'production') {
  const axe = require('react-axe')
  axe(React, ReactDOM, 1000);
}

ReactDOM.render(<App />, document.getElementById('root'))

serviceWorker.register()
