import { Message, MessageEmbed } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class PlayCommand extends Command {
  public queue: Map<number, string>

  public constructor (client: CommandoClient) {
    super(client, {
      name: 'play',
      aliases: ['play', 'p'],
      group: 'music',
      memberName: 'play',
      description: 'play music',
      examples: ['!play undertale music'],
      guildOnly: true,
      clientPermissions: ['CONNECT', 'SPEAK'],
      throttling: {
        usages: 2,
        duration: 5
      },
      args: [
        {
          key: 'args',
          prompt: 'What song would you like to listen to?',
          type: 'string',
          validate: async (url: string): Promise<boolean> => url.length > 0 && url.length < 200
        }
      ]
    })

    this.queue = this.client.queue
  }

  public async run (message: CommandoMessage, { args }: { args: string }): Promise<Message | Message[]> {
    if (!message.member.voice.channelID) {
      const Embed = new MessageEmbed()
      
      Embed
        .setTitle(':x: Error!')
        .setDescription('You must be in a voice channel to play a song.')
        .setColor('#e74c3c')

      return message.channel.send(Embed)
    }

  }
}

export default PlayCommand
