import * as osu from '../../API/osu'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'
import Nodesu, { Beatmap } from 'nodesu'
import moment = require('moment');

interface BeatmapLocal {
  title: string
  total_length: number
  bpm: number
  diff_size: number
  diff_approach: number
  diff_overall: number
  diff_drain: number
  beatmapset_id: number
}

class BeatmapCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'beatmap',
      group: 'osu',
      memberName: 'beatmap',
      description: 'Gets the beatmap info',
      examples: [`${client.commandPrefix}beatmap beatmap_ID`],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3
      },
      args: [
        {
          key: 'beatmapId',
          prompt: 'What is the beatmap ID?',
          type: 'string'
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { beatmapId }: { beatmapId: number }): Promise<Message | Message[]> {
    const NodesuClient = new Nodesu.Client(process.env.OSU_TOKEN as string)
    const Embed = new MessageEmbed()
    const getBeatmap: Beatmap[] & BeatmapLocal[] = await NodesuClient.beatmaps.getByBeatmapId(Number(beatmapId)) as Beatmap[] & BeatmapLocal[]

    if (getBeatmap[0]) {
      Embed
        .setAuthor('Beatmap Found!', 'https://media.discordapp.net/attachments/582807734211313664/582807756973801496/CheckMark.png')
        .setColor(osu.Colours.pink)
        .setTitle(getBeatmap[0].title)
        .setURL(`https://osu.ppy.sh/b/${getBeatmap[0].beatmap_id}`)
        .setDescription(`Download: [osu](https://osu.ppy.sh/beatmapsets/${getBeatmap[0].beatmapset_id}/download) ([no video](https://osu.ppy.sh/beatmapsets/${getBeatmap[0].beatmapset_id}/download?noVideo=1)) - [Bloodcat](https://bloodcat.com/osu/_data/beatmaps/${getBeatmap[0].beatmapset_id}.osz) - [sayobot](https://osu.sayobot.cn/osu.php?s=${getBeatmap[0].beatmapset_id})`)
        .setThumbnail(`https://b.ppy.sh/thumb/${getBeatmap[0].beatmapset_id}.jpg`)
        .addField('Length', `${moment.utc(moment.duration(getBeatmap[0].total_length, 'm').asMilliseconds()).format('h:mm')} (${getBeatmap[0].total_length})`, true)
        .addField('BPM', `${getBeatmap[0].bpm}`, true)
        .addField('Info', `CS: ${getBeatmap[0].diff_size} AR: ${getBeatmap[0].diff_approach} OD:${getBeatmap[0].diff_overall} HD:${getBeatmap[0].diff_drain}`, true)
        .setFooter('Mito Bot', this.client.user.avatarURL)
        .setTimestamp()
    } else {
      Embed
        .setAuthor('No Beatmap Found!', 'http://media.discordapp.net/attachments/582807734211313664/582860068014522377/XMark.png')
        .setColor(osu.Colours.pink)
    }
    return message.embed(Embed)
  }
}

export default BeatmapCommand
