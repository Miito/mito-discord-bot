import { flag } from 'country-emoji'
import { AccuracyMethods, Beatmaps, Colours, ModChecker } from '../../API/osu'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'
import Nodesu, { Beatmap, User, UserScore } from 'nodesu'

class RecentCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'recent',
      group: 'osu',
      memberName: 'recent',
      description: 'Gets the osu! recent status',
      examples: [`${client.commandPrefix}recent USERNAME`, `${client.commandPrefix}recent ID`],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3
      },
      args: [
        {
          key: 'user',
          prompt: 'What is the username or user ID?',
          type: 'string'
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { user }: { user: any }): Promise<Message | Message[]> {
    const NodesuClient = new Nodesu.Client(process.env.OSU_TOKEN as string)
    const Embed = new MessageEmbed()
    const userInfo: object | User = await NodesuClient.user.get(user)
    const userRecent: object[] | UserScore[] = await NodesuClient.user.getRecent(user)
    const beatmapInfo: object[] | Beatmap[] = await NodesuClient.beatmaps.getByBeatmapId(userRecent[0].beatmap_id)

    Embed
      .setAuthor(`Most Recent osu! ${Beatmaps.mode[0]} Play by ${flag(userInfo.country)} ${user}`, `https://a.ppy.sh/${userInfo.user_id}`)
      .setURL(`https://osu.ppy.sh/beatmapsets/${beatmapInfo[0].beatmapset_id}#osu/${userRecent[0].beatmap_id}`)
      .setColor(Colours.pink)
      .setTitle(`${beatmapInfo[0].artist}-${beatmapInfo[0].title} [${beatmapInfo[0].version}] ${ModChecker(userRecent[0].enabled_mods)} [${Number.parseFloat(beatmapInfo[0].difficultyrating).toFixed(2)}★]`)
      .setThumbnail(`https://b.ppy.sh/thumb/${beatmapInfo[0].beatmapset_id}.jpg`)
      .addField('Score', parseInt(userRecent[0].score).toLocaleString(), true)
      .addField('Mod', ModChecker(userRecent[0].enabled_mods), true)
      .addField('Combo', `${userRecent[0].maxcombo}x`, true)
      .addField('Accuracy', AccuracyMethods.Standard({ 300: userRecent[0].count300, 100: userRecent[0].count100, 50: userRecent[0].count50, miss: userRecent[0].countmiss }), true)
      .addField('Time', userRecent[0].date, true)
      .setTimestamp()

    return message.embed(Embed)
  }
}

export default RecentCommand
