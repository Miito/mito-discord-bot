import * as osu from '../../API/osu'
import { flag } from 'country-emoji'
import { parseBitFlags } from '../../Utils/DataStructure'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'
import Nodesu, { Beatmap, User } from 'nodesu'

class OsuCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'osu',
      group: 'osu',
      memberName: 'osu',
      description: 'Gets the osu! user status',
      examples: [`${client.commandPrefix}osu USERNAME`, `${client.commandPrefix}osu ID`],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3
      },
      args: [
        {
          key: 'user',
          prompt: 'What is the username or user ID?',
          type: 'string'
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { user }: { user: any }): Promise<Message | Message[]> {
    const NodesuClient = new Nodesu.Client(process.env.OSU_TOKEN as string)
    const Embed = new MessageEmbed()
    const userInfo: unknown = await NodesuClient.user.get(user)
    const userBest: unknown = await NodesuClient.user.getBest(user)
    const getBestBeatmapInfo: unknown = await NodesuClient.beatmaps.getByBeatmapId(userBest[0].beatmap_id) as Beatmap[] & BeatmapLocal[]

    Embed
      .setAuthor(message.author.username, `https://osu.ppy.sh/images/flags/${userInfo.country}.png`)
      .setColor(osu.Colours.pink)
      .setTitle(userInfo.username)
      .setThumbnail(`https://a.ppy.sh/${userInfo.user_id}`)
      .addField('Ranked Score', parseInt(userInfo.ranked_score).toLocaleString(), true)
      .addField('Total Score', parseInt(userInfo.total_score).toLocaleString(), true)
      .addField('PP', Math.floor(userInfo.pp_raw), true)
      .addField('Rank', `${parseInt(userInfo.pp_country_rank).toLocaleString()} ${flag(userInfo.country)} (${parseInt(userInfo.pp_rank).toLocaleString()}) 🌎`, true)
      .addField('Accuracy', `${Math.round(userInfo.accuracy)}%`, true)
      .addField('Level', Math.floor(userInfo.level), true)
      .addField('Best', `**${getBestBeatmapInfo[0].title}** by **${getBestBeatmapInfo[0].artist}** - **${getBestBeatmapInfo[0].creator}**
      (${userBest[0].rank} | ${userBest[0].pp}pp | ${String(parseBitFlags(userBest[0].enabled_mods, Nodesu.Mods).join(', '))})`, true)
      .setFooter('Mito Bot', this.client.user.avatarURL)
      .setTimestamp()

    return message.embed(Embed)
  }
}

export default OsuCommand
