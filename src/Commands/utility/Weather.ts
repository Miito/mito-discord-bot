import axios from 'axios'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'
import Canvas from 'canvas'

class RottenTomatoesCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'weather',
      group: 'utility',
      memberName: 'weather',
      description: 'Weather status',
      args: [
        {
          key: 'query',
          prompt: 'What place should I look up weather status.',
          type: 'string'
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { query }: { query: string }): Promise<Message | Message[]> {
    const canvas = Canvas.createCanvas(width, height)
    const context = canvas.getContext('2d')

    this.rain(context)
  }

  protected async rain (context): Canvas {
    const height = 365
    const width = 465
    const number = 200
    const array: any = []

    for (let i = 0; i < number; i++) {
      array.push({
        x: Math.random() * width,
        y: Math.random() * height,
        w: 2,
        h: Math.random() * 30,
        s: Math.random() * 10 + 2
      })
    }

    /**
     *
     */
    async function raindrops (): Promise<$TsFixMe> {
      context.clearRect(0, 0, width, height)
      for (let i = 0; i < number; i++) {
        context.fillStyle = 'rgba(0, 51, 102, 0.1)'
        context.fillRect(array[i].x, array[i].y, array[i].w, array[i].h)
      }
      makeItRain()
    }

    /**
     *
     */
    async function makeItRain (): Promise<$TsFixMe> {
      for (let i = 0; i < number; i++) {
        array[i].y += array[i].s
        if (array[i].y > h) {
          array[i].y = -array[i].h
        }
      }
    }
    setInterval(raindrops, 15)
  }
}

export default RottenTomatoesCommand
