import Canvas from 'canvas'
import { Colour } from '../../Utils/Random'
import { Message } from 'discord.js'
import RGBToHex from '../../Utils/Colours/RGBToHex'
import colorally from 'colorally'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class ColorCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'color',
      description: 'Show a random color or a preview of the given color!',
      aliases: ['hex', 'colour'],
      group: 'utility',
      memberName: 'color',
      args: [
        {
          key: 'text',
          prompt: 'What color should I display?',
          type: 'string',
          default: ''
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { text }: { text: string }): Promise<Message | Message[]> {
    const rgbRegex = /rgb\((\d{1,2}|1\d{1,2}|2[0-4]\d|25[0-5]), ?(\d{1,2}|1\d{1,2}|2[0-4]\d|25[0-5]), ?(\d{1,2}|1\d{1,2}|2[0-4]\d|25[0-5])\)/
    const rgbGroup = text.match(rgbRegex)
    let color

    if (text === '') {
      color = Colour()
    } else {
      if (rgbGroup) {
        color = RGBToHex([Number(rgbGroup[1]), Number(rgbGroup[2]), Number(rgbGroup[3])])
      } else {
        color = text
      }
    }

    const canvas = Canvas.createCanvas(250, 300)
    const context = canvas.getContext('2d')

    context.fillStyle = color
    context.fillRect(0, 0, canvas.width, 200)

    context.fillStyle = '#fff'
    context.fillRect(0, 200, canvas.width, 150)

    context.imageSmoothingQuality = 'high'
    context.font = 'bold 20px sans-serif'
    context.textAlign = 'center'
    context.textBaseline = 'middle'
    context.fillStyle = '#000'
    context.filter = 'url(#remove-alpha)'
    context.fillText(`${colorally(color).name}`, canvas.width / 2, 240)

    context.font = '16px sans-serif'
    context.textAlign = 'center'
    context.textBaseline = 'middle'
    context.fillStyle = '#000'
    context.filter = 'url(#remove-alpha)'
    context.fillText(`${color}`, canvas.width / 2, 264)

    return message.say({ files: [{ attachment: canvas.toBuffer(), name: 'color.png' }] })
  }
}

export default ColorCommand
