import math from 'mathjs'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'

class MathCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'math',
      aliases: ['solve', 'calc'],
      group: 'utility',
      memberName: 'math',
      description: 'I\'ll do your math homework!',
      examples: ['~math [equation]'],
      throttling: {
        usages: 1,
        duration: 5
      },
      args: [{
        key: 'equation',
        prompt: 'Please provide me with an equation to solve!',
        type: 'string'
      }]
    })
  }

  public async run (message: CommandoMessage, { text }: { text: string }): Promise<Message | Message[]> {
    const Embed = new MessageEmbed()
    var equation = text

    try {
      var solution = math.eval(equation)
    } catch (error) {
      return message.channel.send(`❎ | I couldn't solve that equation! \`${error}\``)
    }

    Embed
      .setColor('#767CC1')
      .addField('**📥 Expression**', `\`\`\`${equation}\`\`\``)
      .addField('**📤 Result**', `\`\`\`${solution}\`\`\``)

    return message.embed(Embed)
  }
}

export default MathCommand
