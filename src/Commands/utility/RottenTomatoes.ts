import axios from 'axios'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'

class RottenTomatoesCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'rotten-tomatoes',
      aliases: ['tomato-meter', 'r-tomatoes'],
      group: 'utility',
      memberName: 'rotten-tomatoes',
      description: 'Searches Rotten Tomatoes',
      args: [
        {
          key: 'query',
          prompt: 'What movie would you like to search for?',
          type: 'string'
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { query }: { query: string }): Promise<Message | Message[]> {
    try {
      const id = await this.search(query)
      if (!id) return message.say('Could not find any results.')

      const data = await this.fetchMovie(id)
      const criticScore = data.ratingSummary.allCritics
      const audienceScore = data.ratingSummary.audience
      const Embed = new MessageEmbed()

      Embed
        .setColor('#FFEC02')
        .setTitle(`${data.title} (${data.year})`)
        .setURL(`https://www.rottentomatoes.com${data.url}`)
        .setAuthor('Rotten Tomatoes', 'https://i.imgur.com/Sru8mZ3.jpg', 'https://www.rottentomatoes.com/')
        .setDescription(data.ratingSummary.consensus)
        .setThumbnail(data.posters.original)
        .addField('❯ Critic Score', criticScore.meterValue ? `${criticScore.meterValue}%` : '???', true)
        .addField('❯ Audience Score', audienceScore.meterScore ? `${audienceScore.meterScore}%` : '???', true)
      return message.embed(Embed)
    } catch (error) {
      return message.reply(`Oh no, an error occurred: \`${error.message}\`. Try again later!`)
    }
  }

  protected async search (query: string): Promise<string | null> {
    const { body } = await axios({
      method: 'GET',
      url: 'https://www.rottentomatoes.com/api/private/v2.0/search/',
      params: {
        limit: 10,
        q: query
      }
    })

    if (!body.movies.length) return null

    const find = body.movies.find((m: any): boolean => m.name.toLowerCase() === query.toLowerCase()) || body.movies[0]
    return find.url.replace('/m/', '')
  }

  protected async fetchMovie (id: string): Promise<any> {
    const text = await axios({
      method: 'GET',
      url: `https://www.rottentomatoes.com/api/private/v1.0/movies/${id}`
    })

    return text
  }
}

export default RottenTomatoesCommand
