import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { atob, btoa } from '../../Utils/Encoding/Base64'

export class EncodeCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'encode',
      group: 'misc',
      memberName: 'encode',
      description: 'Base64 encoder.',
      examples: [
        'encode your face'
      ],
      args: [
        {
          key: 'argument',
          prompt: 'please specify the input to encode.',
          type: 'string'
        }
      ]
    })
  }

  public async run (message: CommandoMessage, argument: Message): Promise<Message | Message[]> {
    return message.channel.send(`\`\`\`${btoa(argument.input)}\`\`\``)
  }
}

export class DecodeCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'decode',
      group: 'misc',
      memberName: 'decode',
      description: 'Base64 decoder.',
      examples: [
        'decode eW91ciBmYWNl'
      ],
      args: [
        {
          key: 'input',
          prompt: 'please specify the input to decode.',
          type: 'string'
        }
      ]
    })
  }

  public async run (message, arguments_): Promise<Message | Message[]> {
    return message.channel.send(`\`\`\`${atob(arguments_.input)}\`\`\``)
  }
}
