import { Trim } from '../../Utils/Array'
import axios from 'axios'
import moment from 'moment'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'

class NPMCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'npm',
      group: 'utility',
      memberName: 'npm',
      description: 'Responds with information on an NPM package.',
      args: [
        {
          key: 'pkg',
          label: 'package',
          prompt: 'What package would you like to get information on?',
          type: 'string',
          parse: (dependencie: string): string => encodeURIComponent(dependencie.replace(/ /g, '-'))
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { dependencie }: { dependencie: string }): Promise<Message | Message[]> {
    try {
      const { body } = await axios.get(`https://registry.npmjs.com/${dependencie}`)
      if (body.time.unpublished) return message.say('This package no longer exists.')
      const version = body.versions[body['dist-tags'].latest]
      const maintainers = Trim(body.maintainers.map(async (user: any): Promise<any> => user.name))
      const dependencies = version.dependencies ? Trim(Object.keys(version.dependencies)) : null
      const embed = new MessageEmbed()
        .setColor('#CB0000')
        .setAuthor('NPM', 'https://i.imgur.com/ErKf5Y0.png', 'https://www.npmjs.com/')
        .setTitle(body.name)
        .setURL(`https://www.npmjs.com/package/${dependencie}`)
        .setDescription(body.description || 'No description.')
        .addField('Version', body['dist-tags'].latest, true)
        .addField('License', body.license || 'None', true)
        .addField('Author', body.author ? body.author.name : '???', true)
        .addField('Creation Date', moment.utc(body.time.created).format('MM/DD/YYYY h:mm A'), true)
        .addField('Modification Date', moment.utc(body.time.modified).format('MM/DD/YYYY h:mm A'), true)
        .addField('Main File', version.main || 'index.js', true)
        .addField('Dependencies', dependencies && dependencies.length ? dependencies.join(', ') : 'None')
        .addField('Maintainers', maintainers.join(', '))
      return message.embed(embed)
    } catch (error) {
      if (error.status === 404) return message.say('Could not find any results.')
      return message.reply(`Oh no, an error occurred: \`${error.message}\`. Try again later!`)
    }
  }
};

export default NPMCommand
