import { Message } from 'discord.js'
import axios from 'axios'
import { stripIndents } from 'common-tags'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class HumbleBundleCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'humblebundle',
      aliases: ['humble'],
      group: 'utility',
      memberName: 'humblebundle',
      description: 'Responds with the current Humble Bundle.'
    })
  }

  public async run (message: CommandoMessage): Promise<Message | Message[]> {
    try {
      const { text } = await axios({
        method: 'get',
        url: 'https://www.humblebundle.com/androidapp/v2/service_check'
      })
      const body = JSON.parse(text)
      if (!body.length) return message.say('There is no bundle right now...')
      if (body.length > 1) {
        return message.say(stripIndents`
          There are **${body.length}** bundles on right now!
          ${body.map((bundle: any): string => `**${bundle.bundle_name}:** <${bundle.url}>`).join('\n')}
        `)
      }
      const data = body[0]
      return message.say(stripIndents`
        The current bundle is **${data.bundle_name}**!
        ${data.url}
      `)
    } catch (error) {
      return message.reply(`Oh no, an error occurred: \`${error.message}\`. Try again later!`)
    }
  }
};

export default HumbleBundleCommand
