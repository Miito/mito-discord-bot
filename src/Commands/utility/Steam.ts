import axios from 'axios'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'

interface Search {
  data: {
    items: {
      [key: string]: {
        id: number
        tiny_image: string
      }
    }
  }
}

class SteamCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'steam',
      group: 'utility',
      aliases: ['game'],
      memberName: 'steam',
      description: 'Searches Steam for games!',
      examples: ['~steam [game search]'],
      args: [{
        key: 'text',
        prompt: 'Please provide me a game to search for!',
        type: 'string',
        default: 'Doki Doki Literature Club'
      }]
    })
  }

  public async run (message: CommandoMessage, { text }: { text: string }): Promise<Message | Message[]> {
    const Embed = new MessageEmbed()
    const query = text
    const search: Search = await axios({
      method: 'get',
      url: 'https://store.steampowered.com/api/storesearch',
      params: {
        cc: 'us',
        l: 'en',
        term: query
      }
    })

    if (!search.data.items.length) return message.say(`No results found for **${query}**!`)

    /* eslint-disable @typescript-eslint/camelcase */
    const { id, tiny_image } = search.data.items[0]

    const body = await axios({
      method: 'get',
      url: 'https://store.steampowered.com/api/appdetails',
      params: {
        appids: id
      }
    })

    const { data } = body.data[id.toString()]
    const current = data.price_overview ? `$${data.price_overview.final / 100}` : 'Free'
    const original = data.price_overview ? `$${data.price_overview.initial / 100}` : 'Free'
    const price = current === original ? current : `~~${original}~~ ${current} (${(((data.price_overview.final - data.price_overview.initial) / data.price_overview.initial) * 100).toFixed(0)}%)`
    const platforms = []
    if (data.platforms) {
      if (data.platforms.windows) platforms.push('Windows')
      if (data.platforms.mac) platforms.push('Mac')
      if (data.platforms.linux) platforms.push('Linux')
    }

    Embed
      .setColor('#101D2F')
      .setAuthor('Steam', 'https://cdn.discordapp.com/attachments/582807734211313664/592466201972703244/steam.png', 'http://store.steampowered.com/')
      .setTitle(data.name)
      .setURL(`http://store.steampowered.com/app/${data.steam_appid}`)
      .setImage(tiny_image)
      .setDescription(data.short_description)
      .addField('Price', `${price}`, true)
      .addField('Metascore', `${data.metacritic ? data.metacritic.score : '???'}`, true)
      .addField('Recommendations', `${data.recommendations ? data.recommendations.total : '???'}`, true)
      .addField('Platforms', `${platforms.join(', ') || 'None'}`, true)
      .addField('Release Date', `${data.release_date ? data.release_date.date : '???'}`, true)
      .addField('DLC Count', `${data.dlc ? data.dlc.length : 0}`, true)
      .addField('Developers', `${data.developers ? data.developers.join(', ') || '???' : '???'}`, true)
      .addField('Publishers', `${data.publishers ? data.publishers.join(', ') || '???' : '???'}`, true)
    return message.embed(Embed)
  }
}

export default SteamCommand
