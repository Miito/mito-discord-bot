import { search as booru } from 'booru'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'

class GelbooruCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'gelbooru',
      aliases: ['gel'],
      group: 'nsfw',
      memberName: 'gelbooru',
      description: 'nsfw content from gelbooru',
      examples: [`${client.commandPrefix}gelbooru sans from undertale`],
      nsfw: true,
      args: [
        {
          key: 'text',
          prompt: 'What should I search for?',
          type: 'string'
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { text }: { text: string }): Promise<Message | Message[]> {
    const booruSearch = await booru('gelbooru', text, {
      limit: 1,
      random: true
    })
    const hit = booruSearch.first
    const Embed = new MessageEmbed()
    const imageTags: string[] = []

    hit.tags.forEach((tag: string): number => imageTags.push(`[#${tag}](${hit.fileUrl})`))

    Embed
      .setTitle(`gelbooru image for ${text}`)
      .setURL(hit.fileUrl)
      .setColor('#FFB6C1')
      .setDescription(`${imageTags.slice(0, 5).join(' ')}
      Score: ${hit.score}`
      )
      .setImage(hit.fileUrl)

    return message.embed(Embed)
  }
}

export default GelbooruCommand
