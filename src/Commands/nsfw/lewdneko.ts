import Neko from 'nekos.life'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'

class LewdNekoNSFWCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'lewdneko',
      group: 'nsfw',
      memberName: 'lewdneko',
      description: 'nsfw content from http://nekos.life',
      examples: [`${client.commandPrefix}lewdneko`],
      nsfw: true
    })
  }

  public async run (message: CommandoMessage): Promise<Message | Message[]> {
    const neko = new Neko()
    const image = await neko.nsfw.neko()

    const Embed = new MessageEmbed()

    Embed
      .setTitle(`neko.life nsfw image`)
      .setURL(image.url)
      .setColor('#c245a9')
      .setImage(image.url)

    return message.embed(Embed)
  }
}

export default LewdNekoNSFWCommand
