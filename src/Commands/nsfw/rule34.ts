import { search as booru } from 'booru'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'

class Rule34Command extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'rule34',
      aliases: ['r34'],
      group: 'nsfw',
      memberName: 'rule34',
      description: 'nsfw content from rule34',
      examples: [`${client.commandPrefix}rule34 sans from undertale`],
      args: [
        {
          key: 'text',
          prompt: 'What should I search for?',
          type: 'string'
        }
      ],
      nsfw: true
    })
  }

  public async run (message: CommandoMessage, { text }: { text: string }): Promise<Message | Message[]> {
    const booruSearch = await booru('r34', text, {
      limit: 1,
      random: true
    })
    const hit = booruSearch.first
    const Embed = new MessageEmbed()
    const imageTags: string[] = []

    hit.tags.forEach((tag: string): number => imageTags.push(`[#${tag}](${hit.fileUrl})`))

    Embed
      .setTitle(`Rule34 image for ${text}`)
      .setURL(hit.fileUrl)
      .setColor('#FFB6C1')
      .setDescription(`${imageTags.slice(0, 5).join(' ')}
      Score: ${hit.score}`
      )
      .setImage(hit.fileUrl)

    return message.embed(Embed)
  }
}

export default Rule34Command
