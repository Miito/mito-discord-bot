import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { GuildMember, MessageMentions, Message } from 'discord.js'

class RemoveRole extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'removerole',
      group: 'roles',
      memberName: 'removerole',
      description: 'Remove a specified role from a specified user',
      args: [
        {
          key: 'member',
          label: 'member',
          prompt: 'Please provide a user to add the role to (as a mention)',
          type: 'member'
        },
        {
          key: 'role',
          label: 'role',
          prompt:
            'Please provide a role to add the user to (as a mention or by typing the role name)',
          type: 'role'
        }
      ]
    })
  }

  public async run (message: CommandoMessage, argument: Message): Promise<Message | Message[]> {
    const mentions: MessageMentions = message.mentions

    if (mentions.members.size === 0) {
      return message.reply(
        'You must provide a user in a mention to use this command'
      )
    }

    const member: GuildMember = mentions.members.first()
    let role
    if (mentions.roles.size === 0) {
      role = message.guild.roles.find('name', argument.role)

      if (!role) {
        return message.reply('The role provided was not found')
      } else {
        role = mentions.roles.first()
      }

      member.removeRole(role)

      return message.say(
        `${member.user.username} was removed from the ${role.name} role`
      )
    }
  }
}

export default RemoveRole
