import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class ShutdownCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'shutdown',
      aliases: ['st', 'die'],
      group: 'bot',
      memberName: 'shutdown',
      ownerOnly: true,
      description: 'Shut down the bot',
      examples: [`${client.commandPrefix}st`, `${client.commandPrefix}die`]
    })
  }

  public async run (message: CommandoMessage): Promise<Message | Message[]> {
    message.channel.send('Shutting Down...')
    return process.exit()
  }
}

export default ShutdownCommand
