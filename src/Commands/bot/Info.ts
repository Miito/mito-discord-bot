import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'
import moment = require('moment')

class InfoCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'info',
      description: 'Bot information',
      memberName: 'info',
      guildOnly: false,
      group: 'bot'
    })
  }

  public async run (message: CommandoMessage): Promise<Message | Message[]> {
    const Embed = new MessageEmbed()

    Embed
      .setColor(0x00AE86)
      .setThumbnail(this.client.user.avatarURL)
      .addField('Servers', this.client.guilds.size, true)
      .addField('Commands', (this.client.registry.commands.size, true))
      .addField('Memory Usage', `${Math.round(process.memoryUsage().heapUsed / 1024 / 1024)}MB`, true)
      .addField('Uptime', moment.duration(this.client.uptime as number, 'milliseconds').humanize(), true)

    return message.embed(Embed)
  }
}

export default InfoCommand
