import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class RestartCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'restart',
      group: 'bot',
      memberName: 'restart',
      ownerOnly: true,
      description: 'Restart the bot',
      examples: [`${client.commandPrefix}restart`]
    })
  }

  public async run (): Promise<void> {
    this.client.destroy()
    setTimeout(() => {
      this.client.login(process.env.DISCORD_TOKEN || '')
    }, 3000);
  }
}

export default RestartCommand
