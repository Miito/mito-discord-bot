import { Message } from 'discord.js'
import moment from 'moment'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class UptimeCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'uptime',
      description: 'Show bot uptime',
      memberName: 'uptime',
      group: 'bot'
    })
  }

  public async run (message: CommandoMessage): Promise<Message | Message[]> {
    return message.reply(moment.duration(this.client.uptime as number, 'milliseconds').humanize())
  }
}

export default UptimeCommand
