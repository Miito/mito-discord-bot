import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class MemCalcCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'memcalc',
      group: 'bot',
      memberName: 'memcalc',
      ownerOnly: true,
      description: 'Calculates Memory usage.'
    })
  }

  public async run (message: CommandoMessage): Promise<Message | Message[]> {
    return message.reply((process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2))
  }
}

export default MemCalcCommand
