import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class ChangeNameCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'changename',
      aliases: ['setname'],
      group: 'bot',
      memberName: 'changename',
      ownerOnly: true,
      description: 'Changes bot\'s name to something different',
      examples: [`${client.commandPrefix}setname name`],
      throttling: {
        usages: 2,
        duration: 3
      },
      args: [
        {
          key: 'input',
          prompt: 'What picture to change profile picture',
          type: 'string'
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { input }: { input: string }): Promise<Message | Message[]> {
    const bot = this.client.user
    const Name = input

    bot ? bot.setUsername(Name) : false

    message.channel.send('Bot Name Changed!')
    return message.delete(5000)
  }
}

export default ChangeNameCommand
