import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class ChangePictureCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'changepicture',
      aliases: ['setpfp', 'setavatar'],
      group: 'bot',
      memberName: 'changepicture',
      ownerOnly: true,
      description: 'Changes bot\'s avatar to something different',
      examples: [`${client.commandPrefix}setpfp Url`, `${client.commandPrefix}setavatar Url`],
      throttling: {
        usages: 2,
        duration: 3
      },
      args: [
        {
          key: 'input',
          prompt: 'What picture to change profile picture',
          type: 'string'
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { input }: { input: string }): Promise<Message | Message[]> {
    const bot = this.client.user
    const Url = input

    bot ? bot.setAvatar(Url) : false

    message.channel.send('Profile Photo Changed!')
    return message.delete(5000)
  }
}

export default ChangePictureCommand
