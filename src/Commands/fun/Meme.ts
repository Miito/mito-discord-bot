import Canvas from 'canvas'
import Hash from '../../Utils/Encoding/Hash'
import ShortenText from '../../Utils/Canvas/ShortenText'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageAttachment } from 'discord.js'
import { urlRegex } from '../../Utils/Regex'
import probe from 'probe-image-size'

class MemeCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'meme',
      aliases: ['meme'],
      group: 'fun',
      memberName: 'meme',
      description: 'meme top text and bottom text',
      examples: [`${client.commandPrefix}meme <link> <top text> <bottom text>`],
      args: [
        {
          key: 'text',
          prompt: '<top text> <bottom text>',
          type: 'string'
        },
        {
          key: 'new',
          prompt: 'Should I use the new engine ]to crate this meme.',
          type: 'string'
        }
      ]
    })
  }

  // public async MemeCreatorV2 (message: CommandoMessage, { text }: { text: string }): Promise<Message | Message[]> {
  //   const args = text.split(/ +/)
  //   const lastMessage: MessageAttachment = (await message.channel.messages.fetch({ limit: 2 })).array()
  //   const attachments = lastMessage[1].attachments.last()
  //   let backgroundImage
  //   let backgroundSize

  //   if (args[0].match(urlRegex)) {
  //     const imageSize = (await probe(args[0]))
  //     backgroundImage = args[0]
  //     backgroundSize = {
  //       width: imageSize.width,
  //       height: imageSize.height
  //     }
  //   } else {
  //     backgroundImage = attachments.proxyURL
  //     backgroundSize = {
  //       width: attachments.width,
  //       height: attachments.height
  //     }
  //   }

  //   // Create Stage
  //   const Stage = new Konva.Stage({
  //     width: attachments.width,
  //     height: attachments.height
  //   })

  //   const layer = new Konva.Layer()

  //   const Image = new Konva.window.Image()

  //   Image.onload = (): void => {
  //     const Img = new Konva.Image({
  //       image: backgroundImage,
  //       x: 0,
  //       y: 0
  //     })

  //     layer.add(Img)
  //     layer.draw()
  //   }
  // }

  /* eslint-disable @typescript-eslint/no-unused-vars */
  public async run (message: CommandoMessage, { text }: { text: string }): Promise<Message | Message[]> {
    const argument = text.split(/ +/)
    const lastMessage: Message[] = (await message.channel.messages.fetch({ limit: 2 })).array()
    const attachments: MessageAttachment = lastMessage[1].attachments.last()
    let backgroundImage
    let backgroundSize

    if (argument[0].match(urlRegex)) {
      const imageSize = (await probe(argument[0]))

      backgroundImage = argument[0]
      backgroundSize = {
        width: imageSize.width,
        height: imageSize.height
      }
    } else {
      backgroundImage = attachments.proxyURL
      backgroundSize = {
        width: attachments.width,
        height: attachments.height
      }
    }

    const canvas = Canvas.createCanvas(backgroundSize.width, backgroundSize.height)
    const context = canvas.getContext('2d')

    const contextBackground = await Canvas.loadImage(backgroundImage)

    context.drawImage(contextBackground, 0, 0, backgroundSize.width, backgroundSize.height)

    let topText
    let bottomText

    if (argument.length === 3) {
      topText = ShortenText(context, argument[1] || '', backgroundImage)
      bottomText = ShortenText(context, argument[2] || '', backgroundImage)
    } else {
      topText = ShortenText(context, argument[0] || '', backgroundImage)
      bottomText = ShortenText(context, argument[1] || '', backgroundImage)
    }

    context.font = '60px Impact'
    context.textAlign = 'center'
    context.miterLimit = 2
    context.lineJoin = 'round'
    context.strokeStyle = 'black'
    context.lineWidth = 8
    context.strokeText(topText, canvas.width / 2, 100)
    context.fillStyle = 'white'
    context.fillText(topText, canvas.width / 2, 100)

    context.font = '60px Impact'
    context.textAlign = 'center'
    context.miterLimit = 2
    context.lineJoin = 'round'
    context.strokeStyle = 'black'
    context.lineWidth = 8
    context.strokeText(bottomText, canvas.width / 2, canvas.height - 50)
    context.fillStyle = 'white'
    context.fillText(bottomText, canvas.width / 2, canvas.height - 50)

    const fileName = (await new Hash().HashCreator('md5', backgroundImage, 'hex'))

    return message.say({ files: [{ attachment: canvas.toBuffer(), name: `meme-${fileName}.png` }] })
  }
}

export default MemeCommand
