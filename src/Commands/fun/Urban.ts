import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'
const request = require('superagent-promise')(require('superagent'), Promise)

const urbanApi = 'http://api.urbandictionary.com/v0/define?term='

class UrbanCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'urban',
      aliases: ['urban'],
      group: 'fun',
      memberName: 'urban',
      description:
        'Searches urban dictionary for phrase and returns search results',
      args: [
        {
          key: 'searchTerm',
          label: 'searchTerm',
          prompt: 'What would you like to search Urban Dictionary for?',
          type: 'string',
          infinite: false
        }
      ]
    })
  }

  public async run (message: CommandoMessage, argument: $TsFixMe): Promise<Message | Message[]> {
    const json = request('GET', `${urbanApi}${argument.searchTerm}`)

    const result = JSON.parse(json.res.text).list[0]

    if (result === null) {
      return message.channel.send(
        `\`\`\`\n${argument.searchTerm} has no definition!\n\`\`\``
      )
    }

    const Embed = new MessageEmbed()

    Embed
      .setColor('#00ff00')
      .addField(argument.searchTerm, result.definition)
      .addField('Example', result.example)
      .addField('Link', `<${result.permalink}>`)

    return message.embed(Embed)
  }
}

export default UrbanCommand
