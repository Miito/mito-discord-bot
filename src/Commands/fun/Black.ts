import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class BlackCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'black',
      description: 'Replies with Omar is black',
      memberName: 'black',
      group: 'fun'
    })
  }

  public async run (message: CommandoMessage): Promise<Message | Message[]> {
    const randomValue = Math.random() < 0.5

    if (randomValue) {
      return message.channel.send('omar is nigga')
    } else {
      return message.channel.send('omar is black')
    }
  }
}

export default BlackCommand
