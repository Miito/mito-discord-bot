import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'

class CoinFlipCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'flipcoin',
      memberName: 'flipcoin',
      aliases: ['fc'],
      examples: [`${client.commandPrefix}coin`, `${client.commandPrefix}flipcoin`],
      description: 'Flips a coin',
      group: 'fun'
    })
  }

  public async run (message: CommandoMessage): Promise<any> {
    const replies = ['Heads', 'Tails']
    const result = Math.floor((Math.random() * replies.length))

    const Embed = new MessageEmbed()

    Embed
      .setColor('#FF000')
      .setDescription('Flipping Coin')

    message.channel.send(Embed).then(async (message: any): Promise<Message | Message[]> => {
      Embed.setColor('#00FF00')
      Embed.setDescription(`You Flipped \`${replies[result]}\``)
      return message.edit(Embed)
    })
  }
}

export default CoinFlipCommand
