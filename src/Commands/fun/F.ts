import { MessageEmbed, Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class FCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'f',
      aliases: ['respect', 'respects', 'rip'],
      group: 'fun',
      memberName: 'f',
      guildOnly: true,
      description: 'Press F to pay respects',
      examples: ['~f <something you want to respect>'],
      throttling: {
        usages: 1,
        duration: 5
      },
      args: [{
        key: 'text',
        prompt: 'Please provide me something to respect!',
        type: 'string',
        default: ''
      }]
    })
  }

  public async run (message: CommandoMessage, { text }: { text: string }): Promise<Message | Message[]> {
    const Embed = new MessageEmbed()

    if (text === '') {
      Embed
        .setAuthor(`${message.author.username} has paid their respects.`, message.author.displayAvatarURL())
        .setColor('#4E373B')
        .setFooter(`Press F to pay your respects.`)
    } else {
      Embed
        .setAuthor(`\u2000`, message.author.displayAvatarURL())
        .setColor('#4E373B')
        .setDescription(`${message.author} has paid their respects to ${text}`)
        .setFooter(`Press F to pay your respects.`)
    }

    return message
      .embed(Embed)
      .then((context: any): any => context.react('🇫'))
      .catch((error) => {
        throw new Error(error)
      })
  }
}

export default FCommand
