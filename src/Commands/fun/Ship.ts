import Canvas from 'canvas'
import path from 'path'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, User } from 'discord.js'

class EightBallCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'ship',
      memberName: 'ship',
      aliases: ['match'],
      examples: [`${client.commandPrefix}ship @user1 @user2', 'ship @user2`],
      description: 'Tells you how compatible two users are',
      group: 'fun',
      throttling: {
        usages: 2,
        duration: 10
      },
      args: [
        {
          key: 'user1',
          label: 'lover',
          prompt: 'Which user would you like to ship with?',
          type: 'user'
        },
        {
          key: 'user2',
          label: 'second lover',
          prompt: 'Which user would you like to ship the first with?',
          type: 'user',
          default: async (message: $TsFixMe): Promise<$TsFixMe> => message.author
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { user1, user2 }: { user1: User, user2: User }): Promise<Message | Message[]> {
    const ids = [user1.id, user2.id].sort(undefined).join(String(Date.now()).substring(0, 5))
    let shipPercentage = parseInt(require('crypto').createHash('sha256').update(ids).digest('base64').toLowerCase().substring(0, 4), 32) % 101

    let reaction = 'Yikes.'
    if (shipPercentage >= 40) reaction = 'You can make it work!'
    if (shipPercentage >= 65) reaction = 'So cute together!'
    if (shipPercentage >= 85) reaction = 'You two are such a power couple!'
    if (shipPercentage === 100) reaction = 'It was meant to be!'

    // If you ship with yourself
    if (user1 === user2) {
      shipPercentage = 150
      reaction = 'You should always love yourself :)'
    }

    // If you ship with Cinnamon
    if (user1 === this.client.user || user2 === this.client.user) {
      shipPercentage = 100
      reaction = "I'll always love you <3"
    }

    // Set a new canvas to the dimensions of 700x300 pixels
    const canvas = Canvas.createCanvas(700, 300)
    const context = canvas.getContext('2d')

    // Load avatars
    const avatar1 = await Canvas.loadImage(user1.avatarURL({ format: 'png' }))
    const avatar2 = await Canvas.loadImage(user2.avatarURL({ format: 'png' }))

    // Draw avatars
    context.drawImage(avatar1, 20, 20, 300, 300)
    context.drawImage(avatar2, 420, 20, 300, 300)

    // Draw heart
    const heart = await Canvas.loadImage(path.join(process.cwd(), 'Assets', 'images', 'ship-heart.png'))
    // Change opacity based on ship num
    let heartOpacity = shipPercentage / 100
    // Define lower limit to opacity
    if (heartOpacity <= 0.4) {
      heartOpacity = 0.4
    }
    context.globalAlpha = heartOpacity
    // Draw heart and reset opacity
    context.drawImage(heart, 270, 80, 200, 200)
    context.globalAlpha = 1

    // If match > 75%, add overlay
    if (shipPercentage >= 75) {
      const overlay = await Canvas.loadImage(path.join(process.cwd(), 'Assets', 'images', 'ship-overlay.png'))
      context.drawImage(overlay, 0, 0, 320, 110)
      context.drawImage(overlay, 420, 0, 320, 110)
    }

    // Draw percentage
    context.font = 'bold 60px sans-serif'
    context.textAlign = 'center'
    context.fillStyle = '#ffffff'
    context.fillText(`${shipPercentage}%`, canvas.width / 2, 180)

    // Display
    return message.say(reaction, { files: [{ attachment: canvas.toBuffer(), name: 'ship.png' }] })
  }
}

export default EightBallCommand
