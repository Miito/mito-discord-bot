import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'

class EightBallCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: '8ball',
      memberName: '8ball',
      aliases: ['8b'],
      examples: [`${client.commandPrefix}8ball Is this true or false?`],
      description: 'Ask a question',
      group: 'fun',
      args: [
        {
          key: 'content',
          prompt: 'What question do you want to ask?',
          type: 'string'
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { content }: { content: string }): Promise<Message | Message[]> {
    const affirmative = ['It is certain.', 'It is decidedly so.', 'Without a doubt.', 'Yes - definitely.', 'You may rely on it.', 'As I see it, yes.', 'Most likely.', 'Outlook good.', 'Yes.', 'Signs point to yes.']
    const noncommittal = ['Reply hazy, try again.', 'Ask again later.', 'Better not tell you now.', 'Cannot predict now.', 'Concentrate and ask again.']
    const negative = ['Don\'t count on it.', 'My reply is no.', 'My sources say no.', 'Outlook not so good.', 'Very doubtful.']
    const replies = [].concat(affirmative, noncommittal, negative)
    const result = Math.floor((Math.random() * replies.length))
    const question = content

    const Embed = new MessageEmbed()

    Embed
      .setAuthor('Magic 8 Ball', 'https://media.discordapp.net/attachments/582807734211313664/587154140371877888/f73a8294c3ac519665af224d98bf411e.png')
      .addField('Question', question)
      .addField('Answer', replies[result])

    return message.channel.send(Embed)
  }
}

export default EightBallCommand
