import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class DiceRollCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'dice',
      memberName: 'dice',
      aliases: ['diceroll', 'roll', 'rolldice'],
      examples: [`${client.commandPrefix}dice`, `${client.commandPrefix}diceroll`, `${client.commandPrefix}roll`, `${client.commandPrefix}rolldice`],
      description: 'Rolls dice',
      group: 'fun'
    })
  }

  public async run (message: CommandoMessage): Promise<Message | Message[]> {
    return message.say(`:game_die: **${message.author.username}**, you rolled a **${Math.floor(Math.random() * 6) + 1}**!`)
  }
}

export default DiceRollCommand
