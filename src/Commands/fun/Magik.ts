import Hash from '../../Utils/Encoding/Hash'
import { Message } from 'discord.js'
import axios from 'axios'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import sharp, { Sharp } from 'sharp'

class DeepFryCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'magik',
      memberName: 'magik',
      description: 'magik stuff',
      group: 'fun',
      throttling: {
        usages: 1,
        duration: 3
      },
      args: [
        {
          key: 'text',
          prompt: '<url?>',
          type: 'string',
          default: async (message: $TsFixMe): Promise<$TsFixMe> => (((await message.channel.messages.fetch({ limit: 2 })).last()).attachments.last()).proxyURL
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { text }: { text: $TsFixMe }): Promise<Message | Message[]> {
    const ImageURL = text
    const fileName = (await new Hash().HashCreator('md5', ImageURL, 'hex'))

    const Image = await (axios.get(ImageURL, { responseType: 'arraybuffer' }))

    const EditedImage: Sharp = sharp(Image.data)
    EditedImage
      .jpeg({
        quality: 40,
        chromaSubsampling: '4:4:4'
      })
      .modulate({
        brightness: 1.2,
        saturation: 4,
        hue: 2
      })
      .sharpen(3, 1.1, 4)
      .toBuffer({ resolveWithObject: true })

    return message.say(`magik-${fileName}.png`, { files: [{ attachment: EditedImage, name: `magik-${fileName}.png` }] })
  }
}

export default DeepFryCommand
