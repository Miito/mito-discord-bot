import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class MeowCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'meow',
      description: 'Replies with a meow, kitty cat.',
      memberName: 'meow',
      group: 'fun'
    })
  }

  public async run (message: CommandoMessage): Promise<Message | Message[]> {
    return message.channel.send('Meow!')
  }
}

export default MeowCommand
