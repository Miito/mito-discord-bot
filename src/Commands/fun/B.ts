import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class BCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'b',
      guildOnly: true,
      aliases: ['bmoji'],
      group: 'fun',
      memberName: 'b',
      description: 'Converts your text to 🅱 text!',
      throttling: {
        usages: 1,
        duration: 3
      },
      args: [{
        key: 'text',
        prompt: 'What text do you want to do B on?\n',
        type: 'string',
        default: 'traps are not gay',
        parse: (text: string): string => text.toLowerCase()
      }]
    })
  }

  protected B (text: string): string {
    return text.replace(new RegExp(/([bdghpqt])/gi), '🅱')
  }

  public async run (message: CommandoMessage, { text }: { text: string }): Promise<Message | Message[]> {
    return message.channel.send(this.B(text))
  }
}

export default BCommand
