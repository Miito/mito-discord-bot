import axios from 'axios'
import { Message, MessageAttachment } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class PokedexCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'pokedex',
      aliases: ['pokédex'],
      group: 'pokemon',
      memberName: 'pokedex',
      description: 'Retrieve Pokédex info.',
      examples: [`${client.commandPrefix}pokedex 448`, `${client.commandPrefix}pokedex lucario`],
      args: [
        {
          key: 'pokemon',
          label: 'Pokémon',
          prompt: 'Enter Pokémon name or number.',
          type: 'string',
          infinite: false
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { pokemon }: { pokemon: string }): Promise<Message | Message[]> {
    try {
      const info = await this.info(pokemon)
      const spriteUrl = await this.sprite(pokemon)
      const sprite = new MessageAttachment(spriteUrl)
      return message.channel.send(info, sprite)
    } catch (error) {
      return message.channel.send(error.message)
    }
  }

  /**
   * Formats a Pokémon info object into a string
   *
   * @param info Object
   * @returns {string}
   */
  protected formatInfo (info: string): string {
    return `#${info.id}: ${info.name.toCapitalized()}\nType: ${info.type.toCapitalized()}`
  }

  protected async info (pokemon: string): Promise<string> {
    try {
      const response = await axios({
        method: 'GET',
        url: `https://pokeapi.co/api/v2/pokemon/${pokemon.trim().toLowerCase()}`
      })

      const data = response.data
      const info: $TsFixMe = {}
      info.id = data.id
      info.name = data.species.name
      info.type = ''
      data.types.forEach(async (type: $TsFixMe): Promise<$TsFixMe> => {
        info.type = `${info.type} ${type.type.name}`
      })

      return this.formatInfo(info)
    } catch (error) {
      throw new Error('Sorry, I\'ve never heard of that Pokémon!')
    }
  }

  protected async sprite (pokemon: string): Promise<string> {
    try {
      const response = await axios({
        method: 'GET',
        url: `https://pokeapi.co/api/v2/pokemon/${pokemon.trim().toLowerCase()}`
      })

      const data = response.data
      return data.sprites.front_default
    } catch (error) {
      throw new Error('Sorry, I\'ve never heard of that Pokémon!')
    }
  }
}

export default PokedexCommand
