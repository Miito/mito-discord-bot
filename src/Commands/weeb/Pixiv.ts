import PixivAppApi from 'pixiv-app-api'
import fs from 'fs'
import pixivImg from 'pixiv-img'
import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

const pixiv = new PixivAppApi(process.env.PIXIV_USERNAME, process.env.PIXIV_PASSWORD)

class PixivCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'pixiv',
      group: 'weeb',
      memberName: 'pixiv',
      description: 'Sends a random image with tags, NO NSFW',
      examples: ['pixiv tags'],
      guildOnly: true,
      throttling: {
        usages: 20,
        duration: 40
      },
      args: [{
        key: 'tags',
        prompt: 'Tags to search for',
        type: 'string'
      }]
    })
  }

  public async run (message: CommandoMessage, argument: $TsFixMe): Promise<Message | Message[]> {
    const { tags } = argument

    return pixiv.searchIllust(tags)
      .then(async (json: $TsFixMe): Promise<$TsFixMe> => {
        console.log(json)
        var file = fs.createWriteStream('../../teste.png')
        var r = Math.floor(Math.random() * json.illusts.length)
        console.log(`downloading ${json.illusts[r].title}`)
        return pixivImg(json.illusts[r].imageUrls.large).then(async (output: $TsFixMe): Promise<$TsFixMe> => file.write(output, async (error: $TsFixMe): Promise<$TsFixMe> => {
          if (error) { console.error(error) }

          await message.channel.send({ files: [{ attachment: `./${output}` }] })
          fs.unlink(`./${output}`, async (error: $TsFixMe): Promise<$TsFixMe> => {
            if (error) { console.error(error) }
          })
        }))
      }).catch((error: unknown) => {
        throw new Error(`Pixiv error ${error}`)
      })
  }
}

export default PixivCommand
