import Kitsu from 'kitsu'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'

class AnimeCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'anime',
      group: 'weeb',
      memberName: 'anime',
      guildOnly: true,
      description: 'Searches for an anime on Kitsu.io!',
      examples: [`${client.commandPrefix}anime <anime name>`],
      throttling: {
        usages: 3,
        duration: 10
      },
      args: [
        {
          key: 'text',
          prompt: 'Please specify the anime to search for.',
          type: 'string',
          default: ''
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { text }: { text: string }): Promise<Message | Message[]| any> {
    const kitsu = new Kitsu()
    const Embed = new MessageEmbed()
    const { data }: { data: any[] } = await kitsu.get('anime', { filter: { text: text } })

    Embed
      .setColor('#FF9D6E')
      .setAuthor(`${data[0].titles.en_us} | ${data[0].showType}`, data[0].posterImage.original)
      .setDescription(data[0].synopsis.replace(/<[^>]*>/g, '').split('\n')[0])
      .addField('Information', `Japanese Name: ${data[0].titles.ja_jp}\nAge Rating: ${data[0].ageRatingGuide}\nNSFW: ${data[0].nsfw ? 'Yes' : 'No'}`, true)
      .addField('Stats', `Average Rating: ${data[0].averageRating}\nRating Rank: ${data[0].ratingRank}\nPopularity Rank: ${data[0].popularityRank}`, true)
      .addField('Status', `Episodes: ${data[0].episodeCount ? data[0].episodeCount : 'N/A'}\nStart Date: ${data[0].startDate}\nEnd Date: ${data[0].endDate ? data[0].endDate : 'Still airing'}`, true)
      .setImage(data[0].posterImage.original)

    return message.embed(Embed)
  }
}

module.exports = AnimeCommand
