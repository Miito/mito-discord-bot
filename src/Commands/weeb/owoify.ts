import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class OwOifyCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'owoify',
      description: 'OwOify text',
      memberName: 'owoify',
      group: 'weeb',
      args: [
        {
          key: 'text',
          prompt: 'What should I OwOify?',
          type: 'string',
          default: ''
        }
      ]
    })
  }

  public owo (string: string): string {
    const face = ['owo', 'UwU', '>w<', '^w^', '(*^ω^)', '(つ✧ω✧)つ', '(/ =ω=)/', '(*￣з￣)']

    const owoify = string
      .replace(/[lr]/g, 'w')
      .replace(/[LR]/g, 'W')
      .replace(/(ove)/g, 'uv')
      .replace(/n([aeiou])/g, 'ny')
      .replace(/N([aeiou])/g, 'NY')
      .replace(/N([AEIOU])/g, 'Ny')
      .replace(/(has)/g, 'haz')
      .replace(/(en)/g, 'wen')
      .replace(/(have)/g, 'haz')
      .replace(/(you)/g, 'uu')
      .replace(/!+/g, ` ${face[Math.floor(Math.random() * face.length)]} `)

    return owoify
  }

  public async run (message: CommandoMessage, { text }: { text: string }): Promise<Message | Message[]> {
    if (text === '') {
      const lastMessage = (await message.channel.messages.fetch({ limit: 2 })).last().content

      return message.channel.send(this.owo(lastMessage))
    }

    return message.channel.send(this.owo(text))
  }
}

export default OwOifyCommand
