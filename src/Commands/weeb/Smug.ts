import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class SmugCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'smug',
      description: 'Sends a smug face.',
      memberName: 'smug',
      group: 'weeb'
    })
  }

  public async run (message: CommandoMessage): Promise<Message | Message[]> {
    const random = Math.floor(Math.random() * 58) + 1

    return message.channel.send(`https://smug.moe/smg/${random}.png`)
  }
}

export default SmugCommand
