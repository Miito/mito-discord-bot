import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Emoji, Message, MessageEmbed } from 'discord.js'
import format from 'date-fns/format'
import moment = require('moment')

class ServerInfoCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'serverinfo',
      group: 'misc',
      memberName: 'serverinfo',
      description: 'Give informations about the server which command is invoked',
      examples: [`${client.commandPrefix}serverinfo`],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3
      }
    })
  }

  public async run (message: CommandoMessage): Promise<Message | Message[]> {
    const Embed = new MessageEmbed()

    Embed
      .setAuthor('Server Info')
      .setTitle(message.guild.name)
      .addField('ID', message.guild.id, true)
      .addField('Owner', message.guild.owner ? message.guild.owner.user.tag : 'Unknown', true)
      .addField('Members', message.guild.memberCount, true)
      .addField('Text Channels', message.guild.channels.filter((channel): boolean => channel.type === 'text').keyArray().length, true)
      .addField('Voice Channels', message.guild.channels.filter((channel): boolean => channel.type === 'voice').keyArray().length, true)
      .addField('Created at', format(message.guild.createdAt, 'DD-MM-YYYY HH:mm:ss'), true)
      .addField('Region', message.guild.region, true)
      .addField('Roles', `${message.guild.roles.array().length - 1}`, true)
      .addField('Features', `${message.guild.features.join('\n') || '-'}`, true)
      .addField(`Emojis(${message.guild.emojis.array().length})`, message.guild.emojis.map((emoji: Emoji): string[] => [`${emoji.name}<:${emoji.name}:${emoji.id}>`]).splice(0, 20).join(' '), true)

    return message.say({ Embed })
  }
}

export default ServerInfoCommand
