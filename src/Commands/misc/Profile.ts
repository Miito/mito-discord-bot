import { stripIndents } from 'common-tags'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { GuildMember, Message, User } from 'discord.js'

class ProfileCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'profile',
      group: 'misc',
      memberName: 'profile',
      aliases: ['profile', 'user'],
      description: 'Gets the avatar from a user.',
      examples: [`${client.commandPrefix}user @Mito#2675`, `${client.commandPrefix}profile @Mito#2675`],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3
      },
      args: [
        {
          key: 'user',
          prompt: 'What user would you like to snoop on?',
          type: 'user'
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { user }: { user: User & GuildMember }): Promise<Message | Message[]> {
    return message.reply(stripIndents`
    Info on **${user.username}#${user.discriminator}** (ID: ${user.id})
    **❯ Member Details**
    ${user.nickname !== null ? ` • Nickname: ${user.nickname}` : ' • No nickname'}
      • Roles: ${user.roles.map((roles: any): string => `\`${roles.name}\``).join(', ')}
      • Joined at: ${user.joinedAt}
    **❯ User Details**
      • Created at: ${user.createdAt}${user.bot ? '\n • Is a bot account' : ''}
      • Status: ${user.presence.status}
      • Game: ${user.presence.activity ? user.presence.activity.name : 'None'}
    `)
  }
}

export default ProfileCommand
