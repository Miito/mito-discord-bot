import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class HelloCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'hello',
      aliases: ['hi', 'ello'],
      description: 'Hi!',
      memberName: 'hello',
      group: 'misc'
    })
  }

  public async run (message: CommandoMessage): Promise<Message | Message[]> {
    return message.channel.send('Hello!')
  }
}

export default HelloCommand
