import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed, User } from 'discord.js'

class AvatarCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'avatar',
      group: 'misc',
      memberName: 'avatar',
      description: 'Gets the avatar from a user.',
      examples: [`${client.commandPrefix}avatar @Mito#2675`],
      guildOnly: true,
      throttling: {
        usages: 2,
        duration: 3
      },
      args: [
        {
          key: 'user',
          prompt: 'What user would you like to get the avatar from?',
          type: 'user',
          default: async (message: CommandoMessage): Promise<User> => message.author
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { user }: { user: User }): Promise<Message | Message[]> {
    const avatarURL = user.displayAvatarURL()
    const Embed = new MessageEmbed()

    Embed
      .setColor('#FFB6C1')
      .setImage(avatarURL)
      .setTitle(user.username)
      .setURL(avatarURL)
      .setDescription(`[Link](${avatarURL})`)

    return message.embed(Embed)
  }
}

export default AvatarCommand
