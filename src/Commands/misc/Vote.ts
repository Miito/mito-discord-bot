import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class VoteCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'vote',
      description: 'say',
      memberName: 'vote',
      group: 'misc',
      argsType: 'single'
    })
  }

  public async run (message: CommandoMessage, { text }: { text: string }): Promise<Message | Message[] | any> {
    if (text === '') {
      return (await message.reply('You didn\'t give anything to vote over!') as Message).delete({timeout: 5000})
    }
    const voteMessage: Message = await message.channel.send(text)
    await voteMessage.react('✅')
    await voteMessage.react('❎')
  }
}

export default VoteCommand
