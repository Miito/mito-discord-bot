// import { Command } from 'discord.js-commando'
// import { RichEmbed } from 'discord.js'

// class Ping extends Command {
//   public constructor (client: any) {
//     super(client, {
//       name: 'ping',
//       group: 'util',
//       memberName: 'ping',
//       description: 'Checks the bot\'s ping to the Discord server.',
//       throttling: {
//         usages: 5,
//         duration: 10
//       }
//     })
//   }

//   public async run (message) {
//     if(!message.editable) {
//       const pingMsg = await message.channel.send('Pinging...')

//       return pingMsg.edit(`${message.channel.type !== 'dm' ? `${message.author},` : ''}
//         Pong! took me ${pingMsg.createdTimestamp - message.createdTimestamp}ms.
//         ${this.client.ping ? `The heartbeat ping is ${Math.round(this.client.ping)}ms.` : ''}
//       `);
//     }

//     await message.edit('Pinging...');
//     return message.edit(`
//       Pong! took me ${message.editedTimestamp - message.createdTimestamp}ms.
//       ${this.client.ping ? `The heartbeat ping is ${Math.round(this.client.ping)}ms.` : ''}
//     `);
//   }
// }

// export default Ping
