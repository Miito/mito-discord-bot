import { Message } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class SayCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'say',
      description: 'say',
      memberName: 'say',
      group: 'misc',
      args: [
        {
          key: 'text',
          prompt: 'What should I say?',
          type: 'string'
        }
      ]
    })
  }

  public async run (message: CommandoMessage, { text }: { text: string }): Promise<Message | Message[]> {
    return message.channel.send(text)
  }
}

export default SayCommand
