import Neko from 'nekos.life'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'
import { Message, MessageEmbed } from 'discord.js'

class NekoCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'neko',
      group: 'sfw',
      memberName: 'neko',
      description: 'sfw content from http://nekos.life',
      examples: [`${client.commandPrefix}neko`]
    })
  }

  public async run (message: CommandoMessage): Promise<Message | Message[]> {
    const neko = new Neko()
    const image = await neko.sfw.neko()

    const Embed = new MessageEmbed()

    Embed
      .setTitle(`neko.life sfw image`)
      .setURL(image.url)
      .setColor('#c245a9')
      .setImage(image.url)

    return message.embed(Embed)
  }
}

export default NekoCommand
