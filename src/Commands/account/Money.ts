import mongoose from 'mongoose'
import Money from '../../Models/money'
import Terminal from '../../Utils/Terminal'
import { Message, MessageEmbed } from 'discord.js'
import { Command, CommandoClient, CommandoMessage } from 'discord.js-commando'

class MoneyCommand extends Command {
  public constructor (client: CommandoClient) {
    super(client, {
      name: 'money',
      group: 'account',
      memberName: 'money',
      description: 'Shows the amount of money you have in a server.',
      examples: [`${client.commandPrefix}coin`],
      throttling: {
        usages: 2,
        duration: 3
      }
    })
  }

  public async run (message: CommandoMessage): Promise<$TsFixMe> {
    Money.findOne({
      userID: message.author.id,
      serverID: message.guild.id
    }, async (error, money): Promise<Message | Message[]> => {
      if (error) { Terminal.error(error) }

      const Embed = new MessageEmbed()

      Embed
        .setTitle(message.author.tag)
        .setColor('green')
        .setThumbnail(message.author.displayAvatarURL())

      if (!money) {
        Embed.addField('Money', '0', true)
        return message.channel.send(Embed)
      } else {
        Embed.addField('Money', money.money, true)
        return message.channel.send(Embed)
      }
    })
  }
}

export default MoneyCommand
