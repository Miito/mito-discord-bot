import mongoose, { Schema, Document } from 'mongoose'

interface IGuild extends Document {
  _id: string
  name: string
  prefix: string
}

const GuildSchema = new Schema({
  _id: String,
  name: String,
  prefix: String
})

export default mongoose.model<IGuild>('Guild', GuildSchema)
