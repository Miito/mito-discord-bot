import mongoose, { Schema, Document } from 'mongoose'

interface IMoneySchema extends Document {
  userID: string
  serverID: string
  money: number
}

const MoneySchema = new Schema({
  userID: String,
  serverID: String,
  money: Number
})

export default mongoose.model<IMoneySchema>('money', MoneySchema)
