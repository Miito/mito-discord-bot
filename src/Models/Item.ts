import mongoose, { Schema, Document } from 'mongoose'

export interface IItem extends Document {
  name: string
  date?: Date
}

const ItemSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
})

export default mongoose.model<IItem>('item', ItemSchema)
