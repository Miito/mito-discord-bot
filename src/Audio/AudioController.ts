import { CommandoClient } from 'discord.js-commando'
import { Shoukaku } from 'shoukaku'
import Terminal from '../Utils/Terminal'

/**
 * A Basic AudioController class
 * This controller is used to help control discord audio
 *
 * @class AudioController
 */
class AudioController {
  public client: CommandoClient
  public dispatchers: Map<number, string>
  public Shoukaku: Shoukaku

  public constructor (client: CommandoClient) {
    this.client = client
    this.dispatchers = new Map()
    this.Shoukaku = new Shoukaku(this.client, {
      resumable: true,
      resumableTimeout: 30,
      reconnectTries: 2,
      restTimeout: 10000
    })
    this.Shoukaku
      .on('ready', (name, resumed) => Terminal.log(`Lavalink Node: ${name} is now connected. This connection is ${resumed ? 'resumed' : 'a new connection'}`))
      .on('error', (name, error) => Terminal.log(`Lavalink Node: ${name} emitted an error.`, error))
      .on('closed', (name, code, reason) => Terminal.log(`Lavalink Node: ${name} closed with code ${code}. Reason: ${typeof reason !== 'undefined' ? reason : 'No reason'}`))
      .on('disconnected', (name, reason) => Terminal.log(`Lavalink Node: ${name} disconnected. Reason: ${typeof reason !== 'undefined' ? reason : 'No reason'}`))
  }

  public Play () {

  }
}

export default AudioController
