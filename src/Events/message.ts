import Terminal from '../Utils/Terminal'
import { CommandoClient, CommandoMessage } from 'discord.js-commando'
import Money from '../Models/money'

/**
 *
 *
 * @export
 * @param {CommandoClient} client
 * @param {Message} message
 */
export default function (client: CommandoClient, message: CommandoMessage): void {
  if (message.author.bot || !message.guild || message.channel.type === 'dm' || message.command !== null ? message.command.groupID === 'account' : false) {
    return
  }

  const coinsToAdd = Math.ceil(Math.random() * 10)

  Money.findOne({
    userID: message.author.id,
    serverID: message.guild.id
  }, (error, money) => {
    if (error) { Terminal.error(error) }

    if (!money) {
      const newMoney = new Money({
        userID: message.author.id,
        serverID: message.guild.id,
        money: coinsToAdd
      })

      newMoney.save().catch(error => Terminal.error(error))
    } else {
      money.money = money.money + coinsToAdd
      money.save().catch(error => Terminal.error(error))
    }
  }).catch((error) => Terminal.error(error.message))
}
