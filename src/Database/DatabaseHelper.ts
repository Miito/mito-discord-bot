import mongoose, { ConnectionOptions, Mongoose } from 'mongoose'

export const DatabaseOptions: ConnectionOptions = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  autoIndex: true,
  connectTimeoutMS: 10000
}

/**
 *
 *
 * @export
 * @returns {Promise<void>}
 */
export async function ConnectToDatabase (): Promise<Mongoose> {
  const BaseURL = typeof process.env.MONGODB_URL !== 'undefined' ? process.env.MONGODB_URL : 'mongodb://localhost:27017/Database'

  return mongoose.connect(BaseURL, DatabaseOptions)
}

/**
 *
 *
 * @export
 * @returns {Promise<void>}
 */
export async function DisconectFromDatabase (): Promise<void> {
  return mongoose.disconnect()
}
