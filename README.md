# Mito Discord Bot

## Dependencies & Requirements
* node.js 10 LTS | Latest
* IDE with IntelliSense

## Running the bot

### Downloading the source code
Clone the repository:
```sh
git clone https://gitlab.com/Miito/mito-discord-bot.git
cd mito-discord-bot
```

### Configuring
Add token or account detail to ```.env``` file
```
DISCORD_TOKEN=TOKEN
OSU_TOKEN=TOKEN
YOUTUBE_TOKEN=TOKEN
PIXIV_USERNAME=USERNAME
PIXIV_PASSWORD=PASSWORD
PORT=LOCALPORT
```

Configuring the bot ```bot.config.json```
```json
{
  "discordOwners": "OWNER_ID",
  "defaultPrefix": "COMMAND_PREFIX",
  "databaseUrl": "mongodb://localhost:27017/URL_TO_MONGODB"
}

```



### Building
Install dependencies
```sh
npm i
```

Build typescript
```sh
npm run build-ts
```

Build typescript production
```sh
npm run build-prod
```


## Contributing
I welcome all contribution. Before you start working, please make sure you are familiar with testing.

Please see the list of open issues that I am accepting. Before submitting a merge request, please ensure that your code has test and that they passes testing.

### Run tests
```sh
npn run test
```

If you have found any bug in the bot, or have any suggestion, feel free to submit an issue.
